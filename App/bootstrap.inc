<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.10.2017
 * Time: 17:52
 */

define('ROOT_DIR', dirname(dirname(__FILE__)));
define('APP_DIR', ROOT_DIR . '/App');
define('TMP_DIR', ROOT_DIR . '/temp');
define('PUBLIC_DIR', ROOT_DIR . '/public_html');

define('TPL_DIR', APP_DIR . '/Common/Templates');


require_once APP_DIR . '/autoload.php';
