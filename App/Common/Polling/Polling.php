<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.11.2017
 * Time: 13:13
 */

namespace App\Common\Polling;


/**
 * Class Polling
 *
 * @package App\Common\Polling
 */
class Polling {
	private const CHANNEL = 'channel';

	/**
	 * @param $data
	 * @param string $channel
	 *
	 * @return mixed
	 */
	public function send($data, string $channel = '') {
		if (empty($channel)) {
			$channel = self::CHANNEL;
		}

		$string_data = is_string($data) ? $data : json_encode($data);

		$url = HOST . "/pub?id=$channel";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string_data);
		curl_setopt($ch, CURLOPT_POST, 1);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}
}
