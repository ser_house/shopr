<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.11.2017
 * Time: 01:42
 */

namespace App\Common;

use App\Sections\Main\Controller\NotFound;

/**
 * Class ControllerFactory.
 *
 * Фабрика разруливает корневые разделы сайта.
 *
 * @package App\Common
 */
class ControllerFactory {
	/** @var Request */
	private $request = null;

	/**
	 * @param Request $request
	 *
	 * @return Controller|null
	 */
	public function getController(Request $request): ?\App\Common\Controller {
		$this->request = $request;

		$root_uri = $this->request->getRootUri();
		$action = $this->getAction();

		switch ($root_uri) {
			case 'product':
				return \App\Sections\Product\Controller\Factory::getController($action, $this->request);

			case 'site':
				return \App\Sections\Site\Controller\Factory::getController($action, $this->request);

			case 'main':
				return \App\Sections\Main\Controller\Factory::getController($action, $this->request);

			case 'admin':
				return \App\Sections\Admin\Controller\Factory::getController($action, $this->request);

			case '404':
				return new NotFound($this->request);

			default:
				return null;
		}
	}

	/**
	 * Возвращает строку действия для текущего адреса.
	 * Например, для product/add это будет 'add', а для product/1/edit - 'edit'.
	 *
	 * @return string
	 */
	private function getAction(): string {
		$args = $this->request->getArgs();

		if (!empty($args)) {
			$action = array_shift($args);
		}
		else {
			$action = 'index';
		}

		// root из args уже убран, так что action может быть 'add' или id сущности.
		// Если это id сущности, то action будет уже из следующей части адреса
		// (product/1/edit, например).
		if (is_numeric($action)) {
			$id = (int)$action;
			if (empty($args)) {
				$args[] = $id;
				$action = 'view';
			}
			else {
				$action = $args[0];
				$args[0] = $id;
			}
		}

		return $action;
	}
}
