<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.09.2017
 * Time: 20:12
 */

namespace App\Common;


use App\Common\Html\Menu;


/**
 * Class Controller.
 *
 * Базовый класс для всех контроллеров приложения.
 * Абстрактный, поскольку требует обязательной реализации метода __invoke()
 * Это неполная реализация концепции
 * {@link https://jenssegers.com/85/goodbye-controllers-hello-request-handlers Request handlers}
 *
 * @package App\Common
 */
class Controller {
	/** @var string возможный заменитель id сущности в url */
	protected $placeholder = '';

	/** @var Request */
	protected $request = null;


	/**
	 * Controller constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * Возвращает объект главного меню.
	 *
	 * @return Menu
	 * @throws \Exception
	 */
	public function getMainMenu(): Menu {
		// Пункты из конфига влияют только на вывод меню,
		// сами страницы доступны в любом случае - пока не стал городить полноценную обработку.
		$main_menu_items = App::getInstance()->getConfigValue('app')['main_menu'];
		$items = [];
		foreach($main_menu_items as $href => $title) {
			$items[] = [
				'href' => $href,
				'title' => $title,
			];
		}
		$attr = [
			'class' => [
				'main-menu',
			],
		];

		return new Menu($items, $attr);
	}

	/**
	 * В базовых классах это просто заглушка.
	 *
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @throws \Exception
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		return '';
	}

	/**
	 * Возвращает данные для построения хлебных крошек.
	 *
	 * @return array список описывающих каждую крошку ассоциативных массивов, слева-направо.
	 *   Каждая крошка описывается так:
	 * <pre>
	 *  - 'href' => '', // url ссылки, необязательный (для вывода просто текста без ссылки)
	 *  - 'title' => '', // текст ссылки
	 * </pre>
	 */
	public function getBreadcrumbs(): array {
		$items = [];

		$app = App::getInstance();

		$request_uri = $this->request->getRequestUri();

		if (empty($request_uri)) {
			return $items;
		}
		$uri_parts = explode('/', $request_uri);
		if (empty($uri_parts)) {
			return $items;
		}

		// Сделаем копию и будем из нее убирать часть за частью так,
		// что в итоге крошки будут построены только на ту длину,
		// на которую хватит определений.
		$this_items = $this->getUriItems();
		if (empty($this_items)) {
			return $items;
		}

		$current_href = '';

		$items[] = [
			'href' => $current_href,
			'title' => 'Главная',
		];

		try {
			foreach ($uri_parts as $uri_part) {
				// Его может потребоваться получить отдельными телодвижениями,
				// которые могут и не дать результата.
				// И если получить текст ссылки не получится, то закончим на этом построение крошек.
				$title = '';

				// Число может быть id сущности и текст для ссылки должен вернуть контроллер сущности.
				// Но, поскольку крошки строятся по текущему $uri слева направо, то для сущности id может быть
				// только первым числом в этой цепочке.
				// То есть product/1/edit/2/3 - 1 это id сущности, все остальные числа игнорируются.
				if (is_numeric($uri_part)) {
					$placeholder = $this->placeholder;
					if (isset($this_items[$placeholder])) {
						$title_method = $this_items[$placeholder]['title'];
						if (!method_exists($this, $title_method)) {
							$class = get_called_class();
							throw new \Exception("Метод $title_method не определен в классе {$class}, uri: $request_uri");
						}
						$title = call_user_func_array([$this, $title_method], [$uri_part]);
						unset($this_items[$placeholder]);
					}
				}
				else {
					$title = $this_items[$uri_part]['title'];
					unset($this_items[$uri_part]);
				}

				// Как только не удалось получить текст для очередной ссылки в крошках - всё, заканчиваем.
				if (empty($title)) {
					// Исключением, чтобы проскочить удаление последней нормальной ссылки.
					throw new \Exception("Не удалось получить текст ссылки, uri: $request_uri");
				}

				if (empty($current_href)) {
					$current_href = $uri_part;
				}
				else {
					$current_href .= "/$uri_part";
				}

				$items[] = [
					'href' => "/$current_href",
					'title' => $title,
				];

				// Если все определенные части обработаны, то всё.
				// Оставшуюся часть uri не обрабатываем.
				if (empty($this_items)) {
					break;
				}
			}

			// Последнюю часть (это текущая страница) делаем просто текстом без ссылки.
			$last_index = count($items) - 1;
			unset($items[$last_index]['href']);
		}
		catch (\Exception $e) {
			$app->setMessage($e->getMessage(), 'error');
		}
		finally {
			return $items;
		}
	}

	/**
	 * Метод должен возвращать ассоциативный массив, описывающий элементы пути для хлебных крошек.
	 * Каждому элементу пути должен присваиваться короткий текст.
	 *
	 * @return array
	 * <pre>
	 * 'элемент пути' => [
	 *   'title' => 'Название страницы для хлебных крошек',
	 * ],
	 * </pre>
	 * Например:
	 * <pre>
	 * 'product' => [
	 *   'title' => 'Товары',
	 * ],
	 * 'add' => [
	 *   'title' => 'Новый товар',
	 * ],
	 * </pre>
	 */
	public function getUriItems(): array {
		return [];
	}

	/**
	 * @todo: а вот это надо куда-нибудь в типа Route::url().
	 *
	 * @param int $id
	 * @param string $op
	 *
	 * @return string
	 */
	public function getUrl(int $id, string $op = ''): string {
		return '';
	}
}
