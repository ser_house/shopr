<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 07:08
 */

namespace App\Common;

use App\Common\Logger\Logger;


/**
 * Class Route
 *
 * @package App\Common
 */
class Route {
	static public function process() {
		$controller = null;

		// Корневая фабрика контроллеров возвращает фабрику же контроллеров же для каждого
		// базового раздела сайта (product, site и проч.).
		// На двух уровнях фабрик можно и остановиться, потому как более глубокие слои можно разрулировать
		// уже внутри конкретного контроллера.
		try {
			$request = new Request();
			$controllerFactory = new ControllerFactory();
			$controller = $controllerFactory->getController($request);
			if ($controller && $html = $controller()) {
				$response = new Response($request);
				$response->print($html);
			}
			else {
				$_SESSION['404_source'] = $request->getRequestUri();
				Route::errorPage404();
			}
		}
		catch (\Exception $e) {
			$logger = new Logger('exception', true);
			$logger->write($e->getMessage());
			throw $e;
		}
	}

	static public function errorPage404() {
		header('Refresh: 0; url=/404');
	}

	/**
	 * @param string $uri
	 */
	static public function redirect(string $uri) {
		header('Location:' . HOST . "/$uri");
		exit();
	}
}
