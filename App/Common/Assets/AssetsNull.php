<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 10:29
 */

namespace App\Common\Assets;


/**
 * Class AssetsNull
 *
 * @package App\Common\Assets
 */
class AssetsNull implements IAssets {

	/**
	 * @inheritDoc
	 */
	public function addItem(string $asset_item, bool $absolute = false): void {

	}

	/**
	 * @inheritDoc
	 */
	public function getItems(): array {
		return [];
	}
}
