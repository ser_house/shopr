<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 10:27
 */

namespace App\Common\Assets;


/**
 * Interface IAssets
 *
 * @package App\Common\Assets
 */
interface IAssets {

	/**
	 * @param string $asset_item
	 * @param bool $absolute
	 */
	public function addItem(string $asset_item, bool $absolute = false): void;

	/**
	 * @return array
	 */
	public function getItems(): array;
}
