<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.10.2017
 * Time: 18:23
 */

namespace App\Common\Assets;



class AssetsFiles implements IAssets {
	protected $assetsPath = 'assets';
	protected $files = [];

	/**
	 * AssetsFiles constructor.
	 *
	 * @param string $assetsPath
	 */
	public function __construct($assetsPath = '') {
		if (!empty($assetsPath)) {
			$this->assetsPath = $assetsPath;
		}
	}

	/**
	 * @inheritDoc
	 */
	public function addItem(string $asset_item, bool $absolute = false): void {
		if (!$absolute) {
			$filePath = "/$this->assetsPath/$asset_item";
		}
		else {
			$filePath = $asset_item;
		}
		$this->files[$filePath] = $filePath;
	}

	/**
	 * @inheritDoc
	 */
	public function getItems(): array {
		return $this->files;
	}
}
