<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.11.2017
 * Time: 18:17
 */

namespace App\Common\Logger;


/**
 * Class Logger
 *
 * @package App\Common\Logger
 */
class Logger implements ILogger {
	protected const LOG_DIR = ROOT_DIR . '/log';
	private const DEFAULT_LOG_FILE = 'log';
	private $log_file = '';

	/**
	 * Logger constructor.
	 *
	 * @param string $log_file название файла без расширения.
	 * @param bool $clear
	 */
	public function __construct(string $log_file = '', bool $clear = false) {
		if (!is_dir(self::LOG_DIR)) {
			mkdir(self::LOG_DIR);
			chmod(self::LOG_DIR, 0775);
		}

		if (empty($log_file)) {
			$log_file = self::DEFAULT_LOG_FILE;
		}
		$this->log_file = self::LOG_DIR . "/$log_file.log";

		if ($clear && file_exists($this->log_file)) {
			unlink($this->log_file);
		}
	}

	/**
	 * @param $data
	 * @param string $label
	 * @param bool $append
	 *
	 * @return bool
	 */
	public function write($data, string $label = '', bool $append = true): bool {
		$output = date('d.m.Y H:i:s');
		if ($label) {
			$output .= " $label";
		}
		$output .= ': ';

		if (is_bool($data)) {
			$output .= ($data ? 'true' : 'false');
		}
		elseif (is_scalar($data)) {
			$output .= $data;
		}
		else {
			$output .= print_r($data, true);
		}
		$output .= PHP_EOL;

		return file_put_contents($this->log_file, $output, $append ? FILE_APPEND : 0);
	}

	/**
	 * Читает указанный файл лога и возвращает список строк.
	 *
	 * @param string $log_file название файла лога без пути и расширения (расширение подразумевается '.log').
	 *
	 * @return string[]
	 */
	public function read(string $log_file): array {
		$file_path = self::LOG_DIR . "/$log_file.log";
		if (!file_exists($file_path)) {
			return [];
		}

		return file($file_path, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	}
}
