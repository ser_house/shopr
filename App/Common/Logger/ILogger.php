<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 18:40
 */

namespace App\Common\Logger;


interface ILogger {
	/**
	 * @param $data
	 * @param string $label
	 * @param bool $append
	 *
	 * @return bool
	 */
	public function write($data, string $label = '', bool $append = true): bool;
}
