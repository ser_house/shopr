<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 18:53
 */

namespace App\Common\Logger;


/**
 * Class LoggerNull
 *
 * @package App\Common\Logger
 */
class LoggerNull implements ILogger {

	/**
	 * @inheritDoc
	 */
	public function write($data, string $label = '', bool $append = true): bool {
		return true;
	}
}
