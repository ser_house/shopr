<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.10.2017
 * Time: 11:49
 */

namespace App\Common;


/**
 * Trait Messages
 *
 * @package App\Common
 */
trait Messages {
	/**
	 * Добавляет сообщение указанного типа.
	 * Сообщения группируются по типам.
	 *
	 * @param string $message текст сообщения.
	 * @param string $type обычно бывает 'status', 'error', 'warning', но ничем не ограничен.
	 */
	public function setMessage(string $message, string $type = 'status') {
		$_SESSION['messages'][$type][] = $message;
	}

	/**
	 * Возвращает массив сообщений либо конкретного типа, либо всех (сгруппированных по типу).
	 *
	 * @param string $type обычно бывает 'status', 'error', 'warning', но ничем не ограничен.
	 *
	 * @return array
	 */
	public function getMessages(string $type = ''): array {
		if (!empty($type) && isset($_SESSION['messages'][$type])) {
			return $_SESSION['messages'][$type];
		}

		return $_SESSION['messages'] ?? [];
	}

	/**
	 * Стирает все имеющиеся сообщения.
	 */
	public function clearMessages(): void {
		unset($_SESSION['messages']);
	}

	/**
	 * Проверяет, что есть сообщения.
	 *
	 * @return bool
	 */
	public function hasMessages(): bool {
		return !empty($_SESSION['messages']);
	}
}
