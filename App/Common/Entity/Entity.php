<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 12:49
 */

namespace App\Common\Entity;


class Entity implements IEntity, \JsonSerializable {
	/**
	 * @var int
	 */
	public $id = 0;

	/**
	 * Entity constructor.
	 *
	 * @param array $values
	 */
	public function __construct(array $values = []) {
		$this->setData($values);
	}

	/**
	 * @param array $values
	 */
	public function setData(array $values) {
		if (empty($values)) {
			return;
		}

		foreach($this as $key => &$value) {
			if (isset($values[$key])) {
				$value = $values[$key];
			}
		}
		unset($value);
	}

	public function id() {
		return $this->id;
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		$array = [];
		foreach($this as $key => $value) {
			$array[$key] = $value;
		}
		return $array;
	}
}
