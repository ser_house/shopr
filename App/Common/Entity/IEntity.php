<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.12.2017
 * Time: 09:26
 */

namespace App\Common\Entity;

/**
 * Interface IEntity.
 *
 * Определяет универсальные методы для всех сущностей.
 *
 * @package App\Common\Entity
 */
interface IEntity {

	/**
	 * Возвращаемый тип не определен, чтобы иметь возможность использовать строковые идентификаторы (uuid, например).
	 *
	 * @return mixed
	 */
	public function id();
}
