<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.12.2017
 * Time: 06:53
 */

namespace App\Common;


use App\Common\Html\Breadcrumbs;
use App\Common\Html\Html;
use App\Common\Html\Messages;
use App\Common\Html\Page;

/**
 * Class View.
 *
 * Базовый для всех представлений класс.
 *
 * @package App\Common
 */
class View {
	/** @var Controller */
	protected $controller = null;
	/** @var Html */
	protected $html = null;

	/**
	 * View constructor.
	 *
	 * @param Controller $controller
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller) {
		$this->controller = $controller;

		$app = App::getInstance();

		$title = $app->getConfigValue('app')['title'];

		$this->html = new Html($title, new Page($title));

		$mainMenu = $this->controller->getMainMenu();
		$this->html->addContent($mainMenu, 'main_menu');

		$page = $this->html->getPage();

		if ($app->hasMessages()) {
			$msgs = new Messages($app->getMessages());
			$app->clearMessages();
			$page->setMessages($msgs);
		}
		$breadcrumbs = new Breadcrumbs($this->controller->getBreadcrumbs());
		$page->setBreadcrumbs($breadcrumbs);
	}

	/**
	 * Возвращает объект Html текущего представления.
	 *
	 * @return Html
	 */
	public function getHtml(): Html {
		return $this->html;
	}
}
