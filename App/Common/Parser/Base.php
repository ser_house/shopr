<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 00:01
 */

namespace App\Common\Parser;


use App\Common\LoaderHtml\ILoaderHtml;


/**
 * Class Base
 *
 * @package App\Common\Parser
 */
class Base {
	/** @var ILoaderHtml */
	protected $loaderHtml = null;

	/**
	 * Base constructor.
	 *
	 * @param ILoaderHtml $loaderHtml
	 */
	public function __construct(ILoaderHtml $loaderHtml) {
		$this->loaderHtml = $loaderHtml;
	}
}
