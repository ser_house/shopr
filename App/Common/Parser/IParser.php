<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 09:47
 */

namespace App\Common\Parser;


/**
 * Interface IParser
 *
 * @package App\Common\Parser
 */
interface IParser {
	/**
	 * @return int
	 */
	public function getPrice(): int;
}
