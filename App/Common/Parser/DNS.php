<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 19:30
 */

namespace App\Common\Parser;

use DiDom\Document;

/**
 * Class DNS
 *
 * @package App\Common\Parser
 */
class DNS extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);

		$node = $document->first('.current-price-value');
		if ($node) {
			return $node->attr('data-price-value');
		}

		return 0;
	}
}
