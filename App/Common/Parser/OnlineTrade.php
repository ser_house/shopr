<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 00:01
 */

namespace App\Common\Parser;


use DiDom\Document;

/**
 * Class OnlineTrade
 *
 * @package App\Common\Parser
 */
class OnlineTrade extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);
//		<span itemprop="price" content="122990">122 990</span>
		$node = $document->first('.catalog__displayedItem__actualPrice');
		if ($node) {
			$span = $node->first('span[itemprop=price]');
			if ($span) {
				return $span->attr('content');
			}
			else {
				return preg_replace('/[^0-9]/', '', $node->text());
			}
		}

		return 0;
	}
}
