<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.12.2017
 * Time: 10:59
 */

namespace App\Common\Parser;

use DiDom\Document;

/**
 * Class Mvideo
 *
 * @package App\Common\Parser
 */
class Mvideo extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);

		$node = $document->first('.c-pdp-price__current');
		if ($node) {
			return preg_replace('/[^0-9]/', '', $node->text());
		}

		return 0;
	}
}
