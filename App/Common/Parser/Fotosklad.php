<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.12.2017
 * Time: 08:15
 */

namespace App\Common\Parser;

use DiDom\Document;

class Fotosklad extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);
		$node = $document->first('.price_line');
		if ($node) {
			$span = $node->first('meta[itemprop=price]');
			if ($span) {
				return $span->attr('content');
			}
			else {
				return preg_replace('/[^0-9]/', '', $node->text());
			}
		}

		return 0;
	}
}
