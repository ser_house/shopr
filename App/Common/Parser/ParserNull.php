<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 09:45
 */

namespace App\Common\Parser;


/**
 * Class ParserNull
 *
 * @package App\Common\Parser
 */
class ParserNull extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		return 0;
	}
}
