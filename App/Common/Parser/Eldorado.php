<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.12.2017
 * Time: 11:08
 */

namespace App\Common\Parser;

use DiDom\Document;

/**
 * Class Eldorado
 *
 * @package App\Common\Parser
 */
class Eldorado extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);

		$node = $document->first('.addToCartBig');
		if ($node) {
			return $node->attr('data-price');
		}

		return 0;
	}
}
