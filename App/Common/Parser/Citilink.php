<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.12.2017
 * Time: 08:47
 */

namespace App\Common\Parser;

use DiDom\Document;

class Citilink extends Base implements IParser {

	/**
	 * @inheritDoc
	 */
	public function getPrice(): int {
		$html = $this->loaderHtml->getHtml();

		$document = new Document($html);

		$priceNode = $document->first('.price_break');

		if ($priceNode) {
			$el = $priceNode->first('.num');
			if (!$el) {
				$el = $priceNode;
			}

			return preg_replace('/[^0-9]/', '', $el->text());
		}
		else {
			$missingNode = $document->first('.out-of-stock_js');
			if ($missingNode) {
				// @todo: и? что теперь делать с нет в наличии товаром?
				// воткнуть флаг в страницу товара, например.
				// А если хочется посмотреть период, в который товара не было в магазине,
				// то добавить отдельно хранение граничных дат:
				// при первой проверке добавляем дату и "в наличии"
				// при когда нет в наличии, добавляем дату и "нет в наличии"
				// ну и следующую дату, в которую товар опять есть в наличии.
			}
		}

		return 0;
	}
}
