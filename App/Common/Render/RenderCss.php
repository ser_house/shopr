<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.11.2017
 * Time: 10:43
 */

namespace App\Common\Render;


/**
 * Class RenderCss
 *
 * @package App\Common\Render
 */
class RenderCss extends RenderAssets {

	/**
	 * @return string
	 */
	public function render(): string {
		$output = '';
		foreach ($this->files as $file) {
			$output .= "<link rel='stylesheet' media='all' href='$file'>";
		}

		return $output;
	}
}
