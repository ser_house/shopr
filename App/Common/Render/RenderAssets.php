<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.11.2017
 * Time: 10:49
 */

namespace App\Common\Render;


/**
 * Class RenderAssets
 *
 * @package App\Common\Render
 */
class RenderAssets {
	/** @var array */
	protected $files = [];

	/**
	 * RenderAssets constructor.
	 *
	 * @param array $files
	 */
	public function __construct(array $files) {
		$this->files = $files;
	}
}
