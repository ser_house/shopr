<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.11.2017
 * Time: 08:45
 */

namespace App\Common\Render;

use App\Common\Formatter\Formatter;
use App\Common\Html\Html;
use App\Common\Html\IComponent;
use App\Common\Html\Table;
use App\Common\Logger\Logger;


/**
 * Class Render.
 *
 * @package App\Common\Render
 */
class Render {
	// Содержимое полей формы должно выводиться в том же виде, в котором его вводил пользователь,
	// без санитизации, фильтрации и форматирования.
	protected const FORM_ELEMENT_TAGS = [
		'input',
		'button',
		'checkbox',
		'radio',
		'textarea',
		'select',
		'option',
	];


	protected function isSanitize(string $tag): bool {
		return !in_array($tag, self::FORM_ELEMENT_TAGS);
	}

	protected function renderData($component, bool $sanitize = true) {
		// @todo: тег и шаблон взаимоисключающие вещи.
		// @todo: пожалуй, удобнее будет подготавливать всегда $vars,
		// а вызывающий код уж пускай сам решает что с ними делать.

		// Должна возвращаться строка:
		//   если это примитивный тип
		//   если это элемент с тегом
		// Должен возвращаться массив для $vars:
		//   если это элемент и у него установлен шаблон
		//   если это элемент без шаблона и без тега

		// Получили на вход примитивный тип.
		if (is_scalar($component)) {
			return $component;
		}

		// Получили на вход список элементов.
		// @todo: здесь самая главная проблема - если мы сюда зашли из рендера шаблона,
		// то элементы из этого списка должны вернуться в ключах $vars для этого шаблона.
		if (is_array($component)) {
			$output = '';
			foreach($component as $item) {
				$output .= $this->renderData($item, $sanitize);
			}
			return $output;
		}

		// Таблица особый случай, поскольку у неё есть отдельно столбцы и отдельно строки с ячейками.
		if ($component instanceof Table) {
			return $this->renderTable($component);
		}

		// Элемент хочет использовать файл шаблона, в этом случае тег,
		// даже если он и указан, не имеет значения.
		// Сам же элемент уходит наружу (например, в ключ $vars для родительского шаблона)
		// уже отрендеренный.
		$element_tpl_file = $component->getTplFile();
		if (!empty($element_tpl_file)) {
			$vars = [
				'attributes' => $this->renderElementAttributes($component),
			];

			$content = $component->getContent();

			foreach ($content as $key => $item) {
				$vars[$key] = $this->renderData($item);
			}
			return $this->renderTpl($element_tpl_file, $vars);
		}

		$tag = $component->getTag();

		if (!empty($tag)) {
			return $this->renderTagged($component);
		}
		else {
			$output = '';
			$content = $component->getContent();
			foreach($content as $item) {
				$output .= $this->renderData($item, $sanitize);
			}
			return $output;
		}
	}

	/**
	 * Рендер элемента с тегом в строку html.
	 *
	 * @param IComponent $component
	 *
	 * @return string
	 */
	protected function renderTagged(IComponent $component): string {
		$tag = $component->getTag();

		$output = '';
		$attributes = $component->getAttributes();
		$output .= $this->renderOpenTag($tag, $attributes);
		$sanitize = $this->isSanitize($tag);

		$content = $component->getContent();
		foreach($content as $item) {
			$output .= $this->renderData($item, $sanitize);
		}

		$output .= $this->renderCloseTag($tag);

		return $output;
	}

	/**
	 * Рендер открывающего тега вместе с атрибутами.
	 *
	 * @param string $tag
	 * @param array $attributes
	 *
	 * @return string
	 */
	protected function renderOpenTag(string $tag, array $attributes): string {
		$output = '';

		$attr = '';
		$id = '';
		$label = '';

		if (!empty($attributes)) {
			$elAttr = new TagAttributes($attributes);
			$id = $elAttr->getAttribute('id');

			// @todo: вынести из атрибутов - какое оно к ним имеет отношение?
			$label = $elAttr->getAttribute('label');
			if (!empty($label)) {
				$elAttr->removeAttribute('label');
			}

			$prepared_attrs = $elAttr->getPreparedAttributes();
			$attr = " $prepared_attrs";
		}

		if (isset($attributes['type']) && 'checkbox' == $attributes['type']) {
			if (!empty($label)) {
				$output .= "<label class='checkbox-label'>";
			}

			$output .= "<$tag{$attr}>";

			if (!empty($label)) {
				$output .= "$label</label>";
			}
		}
		else {
			if (!empty($label)) {
				$for = '';

				if (!empty($label)) {
					if (!empty($id)) {
						$for = " for='$id'";
					}
				}

				$output .= "<label$for>$label</label>";
			}

			$output .= "<$tag{$attr}>";
		}

		return $output;
	}

	/**
	 * Рендер закрывающего тега.
	 *
	 * @param string $tag
	 *
	 * @return string
	 */
	protected function renderCloseTag(string $tag): string {
		$output = '';

		if ('input' != $tag) {
			$output .= "</$tag>";
		}

		return $output;
	}

	protected function renderTable(Table $table): string {
		$tpl_file = $table->getTplFile();
		if (!empty($tpl_file)) {
			return $this->renderTableTpl($table);
		}

		// Просто как вариант рендерить без шаблона.
		// Не используется, поскольку таблица в базовом классе ставит себе шаблон,
		// но покрывает случай, когда производный класс решил, что самый умный, и избавил себя от шаблона.
		$attributes = $this->renderElementAttributes($table);
		$columns = $table->getColumns();
		$rows = $table->getRows();

		$items = $this->prerenderTableItems($columns, $rows);

		$output = "<table$attributes>";
		$output .= "<thead><tr>";

		foreach ($items['columns'] as $column) {
			$output .= "<th{$column['attributes']}>{$column['content']}</th>";
		}
		unset($column);

		$output .= "</tr></thead>";
		$output .= "<tbody>";
		foreach ($items['rows'] as $row) {
			$output .= "<tr{$row['attributes']}>";
			foreach ($row['cells'] as $cell) {
				$output .= "<td{$cell['attributes']}>{$cell['content']}</td>";
			}
			$output .= '</tr>';
		}

		$output .= '</tbody></table>';

		return $output;
	}

	protected function renderTableTpl(Table $table): string {
		$tpl_file = $table->getTplFile();

		$columns = $table->getColumns();
		$rows = $table->getRows();

		$vars = $this->prerenderTableItems($columns, $rows);
		$vars['attributes'] = $this->renderElementAttributes($table);

		ob_start();

		foreach ($vars as $key => $value) {
			${$key} = $value;
		}
		unset($value);

		include $tpl_file;

		return ob_get_clean();
	}

	/**
	 * Подготавливает столбцы и ряды с ячейками таблицы к передаче в файл шаблона.
	 *
	 * @param array $columns
	 * @param array $rows
	 *
	 * @return array
	 */
	protected function prerenderTableItems(array $columns, array $rows): array {
		$prepared_columns = [];
		foreach($columns as $column) {
			$prepared_columns[] = $this->renderTableCell($column);
		}

		$prepared_rows = [];
		foreach($rows as $row) {
			$prepared_row = [
				'attributes' => $this->renderElementAttributes($row),
				'content' => [],
			];

			$cells = $row->getContent();
			foreach($cells as $cell) {
				$prepared_row['content'][] = $this->renderTableCell($cell);
			}

			$prepared_rows[] = $prepared_row;
		}

		return [
			'columns' => $prepared_columns,
			'rows' => $prepared_rows,
		];
	}

	/**
	 * Подготавливает переданный компонент и его содержимое к передаче в файл шаблона.
	 *
	 * @param IComponent $component
	 *
	 * @return array
	 *  - 'attributes' (string) => готовая к выводу строка с атрибутами компонента
	 *  - 'content' (array) => ассоциативн
	 */
	protected function prerenderTplVar(IComponent $component): array {
		$var = [
			'attributes' => $this->renderElementAttributes($component),
			'content' => [],
		];

		$content = $component->getContent();
		foreach($content as $key => $item) {
			$var['content'][$key] = $this->renderData($item);
		}

		return $var;
	}

	protected function renderTableCell($cell): array {
		$rendered_cell = [
			'attributes' => '',
			'content' => '',
		];

		if (is_scalar($cell) || is_array($cell)) {
			$rendered_cell['content'] = $this->renderData($cell);
		}
		elseif ($cell instanceof IComponent) {
			$tag = $cell->getTag();
			if (empty($tag)) {
				$prerendered = $this->prerenderTplVar($cell);
				$rendered_cell['attributes'] = $prerendered['attributes'];
				$rendered_cell['content'] = implode('', $prerendered['content']);
			}
			else {
				$rendered_cell['content'] = $this->renderData($cell);
			}
		}
		else {
			throw new \InvalidArgumentException('Недопустимый тип ячейки таблицы: ' . PHP_EOL . print_r($cell, true) . PHP_EOL);
		}

		return $rendered_cell;
	}

	protected function renderElementAttributes(IComponent $element): string {
		$attributes = $element->getAttributes();
		return $this->renderAttributes($attributes);
	}

	protected function renderAttributes(array $attributes): string {
		$rendered_attrs = '';

		if (!empty($attributes)) {
			$elAttr = new TagAttributes($attributes);
			$prepared_attrs = $elAttr->getPreparedAttributes();
			$rendered_attrs = " $prepared_attrs";
		}

		return $rendered_attrs;
	}

	public function renderHtml(Html $html): string {
		$tpl_file = $html->getTplFile();

		$vars = [
			'attributes' => $this->renderElementAttributes($html),
			'styles' => '',
			'scripts' => '',
		];

		$content = $html->getContent();

		foreach ($content as $key => $item) {
			$vars[$key] = $this->renderData($item);
		}

		$assets_css = $html->getAssetsCss()->getItems();
		if (!empty($assets_css)) {
			$cssRender = new RenderCss($assets_css);
			$vars['styles'] = $cssRender->render();
		}

		$assets_js = $html->getAssetsJs()->getItems();
		if (!empty($assets_js)) {
			$jsRender = new RenderJs($assets_js);
			$vars['scripts'] = $jsRender->render();
		}

		return $this->renderTpl($tpl_file, $vars);
	}

	protected function renderTpl(string $tpl_file, array $vars): string {
		ob_start();

		foreach ($vars as $key => $value) {
			${$key} = $value;
		}
		include $tpl_file;

		return ob_get_clean();
	}

	/**
	 * Метод пытается перевести полученный элемент вместе со всеми дочерними в код html.
	 * @todo: документация.
	 *
	 * @param \App\Common\Html\IComponent $component
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function render(IComponent $component): string {

		try {
			$class = new \ReflectionClass($component);
			$element_type = $class->getShortName();

			switch($element_type) {
				// Html нарушает принцип Лисков, поскольку у него ещё стили со скриптами. todo?
				case 'Html':
					return $this->renderHtml($component);

				case 'ComponentScalar':
					$content = $component->getContent();
					$output = '';
					foreach($content as $item) {
						$output .= $this->renderData($item);
					}
					return $output;

				default:
					break;
			}

			return $this->renderData($component);
		}
		catch (\Exception $e) {
			$logger = new Logger('exception', true);
			$logger->write($e->getMessage());

			throw $e;
		}
	}

	/**
	 * Рендер панельки с показателями производительности.
	 *
	 * @return string
	 */
	public function renderDebugPanel(): string {
		$output = '';

		$output .= '<div id="db-panel">';
		if (function_exists('xdebug_peak_memory_usage')) {
			$memory_peak = xdebug_peak_memory_usage();
		}
		else {
			$memory_peak = memory_get_peak_usage(true);
		}

		$memory_peak_formatted = Formatter::formatBytes($memory_peak);

		$output .= "<span>Memory peak: $memory_peak_formatted</span>";

		$timeRun = \App\Common\App::getInstance()->getTimeRun();
		$output .= "<span>Time run: {$timeRun}ms</span>";

		return $output;
	}
}
