<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.10.2017
 * Time: 13:14
 */

namespace App\Common\Render;


/**
 * Class TagAttributes
 *
 * @package App\Common\Render
 */
class TagAttributes {
	protected $attributes = [];

	protected $preparedAttributes = '';

	/**
	 * TagAttributes constructor.
	 *
	 * @param array $attributes
	 */
	public function __construct(array $attributes = []) {
		if (!empty($attributes['class'])) {
			$attributes['class'] = array_unique($attributes['class']);
		}

		$this->attributes = $attributes;
	}

	/**
	 * @param array $classes
	 */
	public function setClasses(array $classes) {
		foreach ($classes as $class) {
			$this->addClass($class);
		}
	}

	/**
	 * @param string $class
	 */
	public function addClass(string $class) {
		$this->attributes['class'][$class] = $class;
	}

	/**
	 * @param string $key
	 * @param $value
	 */
	public function setAttribute(string $key, $value) {
		$this->attributes[$key] = $value;
	}

	/**
	 * @param string $key
	 *
	 * @return mixed|string
	 */
	public function getAttribute(string $key) {
		return isset($this->attributes[$key]) ? $this->attributes[$key] : '';
	}

	/**
	 * @param string $key
	 */
	public function removeAttribute(string $key) {
		unset($this->attributes[$key]);
	}

	protected function buildAttributes() {
		$attributes = [];
		foreach ($this->attributes as $key => $value) {
			switch ($key) {
				case 'required':
				case 'checked':
				case 'selected':
					if ($value) {
						$attributes[] = $key;
					}
					break;

				case 'class':
					$classes = implode(' ', $value);
					$attributes[] = "class='$classes'";
					break;

				default:
					$attributes[] = "$key='$value'";
			}
		}

		$this->preparedAttributes = implode(' ', $attributes);
	}

	/**
	 * @return string
	 */
	public function getPreparedAttributes(): string {
		$this->buildAttributes();

		return $this->preparedAttributes;
	}
}
