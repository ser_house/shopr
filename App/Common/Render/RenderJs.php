<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.11.2017
 * Time: 10:44
 */

namespace App\Common\Render;


/**
 * Class RenderJs
 *
 * @package App\Common\Render
 */
class RenderJs extends RenderAssets {

	/**
	 * @return string
	 */
	public function render(): string {
		$output = '';
		foreach ($this->files as $file) {
			$output .= "<script src='$file' type='text/javascript'></script>";
		}

		return $output;
	}
}
