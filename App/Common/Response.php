<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.12.2017
 * Time: 21:02
 */

namespace App\Common;


use App\Common\Html\Messages;
use App\Common\Render\Render;


/**
 * Class Response.
 *
 * Набросок класса как единая точка выброса ответа в вывод.
 *
 * @package App\Common
 */
class Response {
	/** @var Request */
	private $request = null;

	/**
	 * Response constructor.
	 *
	 * @param Request $request
	 */
	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * Тот самый главный метод, который и выбрасывает ответ.
	 * Для ajax это будет json, для не-ajax - html.
	 *
	 * @param View|array|string $result
	 *
	 * @throws \Exception
	 */
	public function print($result) {
		if ($this->request->isAjax() && is_array($result)) {
			if (isset($result['html'])) {
				$render = new Render();
				$result['html'] = $render->render($result['html']);
			}
			print json_encode($result);
			exit;
		}

		$app = App::getInstance();

		$html = $result->getHtml();

		$messages = $app->getMessages();
		if (!empty($messages)) {
			$app->clearMessages();
			$msgsView = new Messages($messages);

			$html->getPage()->addContent($msgsView, 'messages');
		}

		$has_dbg = true;
		if ($has_dbg) {
			$html->addClassCss('has-dbg');
		}

		$render = new Render();
		print $render->render($html);

		if ($has_dbg) {
			print $render->renderDebugPanel();
		}
	}
}
