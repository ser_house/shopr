<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.09.2017
 * Time: 19:36
 */

namespace App\Common;

use App\Common\Config\Config;
use App\Common\Html\Breadcrumbs;
use App\Common\Html\Messages;
use App\Common\Html\Html;
use App\Common\Logger\Logger;
use App\Common\Render\Render;


/**
 * Class App
 *
 * @package App\Common
 */
class App {
	use \App\Common\Messages;

	/** @var \App\Common\Controller */
	private $controller = null;

	/** @var Config */
	private $config = null;

	/**
	 * @var bool
	 */
	private $hasException = false;

	/**
	 * @var float
	 */
	private $timeStart = 0.00;
	/**
	 * @var float
	 */
	private $timeEnd = 0.00;

	/**
	 * @var App
	 */
	static private $instance = null;

	/**
	 * @return null|static
	 */
	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new static();

			if (function_exists('xdebug_time_index')) {
				self::$instance->timeStart = xdebug_time_index();
			}
			else {
				self::$instance->timeStart = microtime(true);
			}

			self::$instance->config = new Config();

			set_error_handler([self::$instance, 'errorHandler']);
			set_exception_handler([self::$instance, 'exceptionHandler']);
			register_shutdown_function([self::$instance, 'shutdownHandler']);
		}

		return self::$instance;
	}

	/**
	 *
	 */
	public function process() {
		if (!is_dir(TMP_DIR)) {
			mkdir(TMP_DIR);
			chmod(TMP_DIR, 0775);
		}

		Route::process();
	}

	/**
	 * @param string $name
	 *
	 * @return \ArrayObject|mixed
	 * @throws \Exception
	 */
	public function getConfigValue(string $name = '') {
		return self::$instance->config->get($name);
	}

	/**
	 * @return float
	 */
	public function getTimeRun() {
		if (function_exists('xdebug_time_index')) {
			$this->timeEnd = xdebug_time_index();
			return ceil(1000 * $this->timeEnd);
		}
		else {
			$this->timeEnd = microtime(true);
			return ceil(1000 * ($this->timeEnd - $this->timeStart));
		}
	}

	/**
	 * @param $errno
	 * @param $errstr
	 * @param $errfile
	 * @param $errline
	 *
	 * @return bool
	 */
	function errorHandler($errno, $errstr, $errfile, $errline) {
		if (!(error_reporting() & $errno)) {
			return false;
		}
		$error = "$errfile ($errline): $errstr";

		$logger = new Logger('error');
		$logger->write($error);

		$this->setMessage($error, 'error');

		return true;
	}

	/**
	 * @param \Throwable $e
	 */
	public function exceptionHandler(\Throwable $e) {
		$this->hasException = true;

		$errfile = $e->getFile();
		$errline = $e->getLine();
		$errstr = $e->getMessage();

//		if (!empty($e->xdebug_message)) {
//			$error = $e->xdebug_message;
//		}
//		else {
			$error = "$errfile ($errline): $errstr";
//		}


		if (class_exists('\Kint')) {
			\Kint::dump($error);
			\Kint::trace($e->getTrace());
		}
		$logger = new Logger('exception');
		$logger->write($error);

		$this->setMessage($error, 'error');
	}

	/**
	 *
	 */
	public function shutdownHandler() {
		if ($this->hasException) {
			$messages = $this->getMessages();
			$this->clearMessages();

			$title = 'Произошла ошибка';

			$html = new Html($title);

			$has_dbg = true;
			if ($has_dbg) {
				$html->addClassCss('has-dbg');
			}

			$page = $html->getPage();

			$msgsView = new Messages($messages);
			$page->setMessages($msgsView);

			if ($this->controller) {
				$breadcrumbs = new Breadcrumbs($this->controller->getBreadcrumbs());
				$page->setBreadcrumbs($breadcrumbs);
			}

			$render = new Render();
			$output = $render->render($html);
			if ($has_dbg) {
				$output .= $render->renderDebugPanel();
			}
			print $output;
		}
	}
}
