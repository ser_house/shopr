<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 00:58
 */

namespace App\Common\Config;


/**
 * Class Config
 *
 * @package App\Common\Config
 */
class Config {
	/** @var \ArrayObject */
	protected $data = null;

	/**
	 * Config constructor.
	 */
	public function __construct() {
		$conf_path = APP_DIR . '/config';
		// Определяет как файлы с конфигурациями, так и их порядок:
		// сначала файл общей конфигурации
		// затем файл локальной конфигурации
		// Это даст простое наследование, когда частная конфигурация ("наследник")
		// перекрывает настройки родителя (если они там определены) либо их расширяет.
		$mask = "$conf_path/{conf,{,*.}local}.inc";

		$files = glob($mask, GLOB_BRACE);

		$config = [];
		foreach ($files as $file) {
			$config = array_replace_recursive($config, include $file);
		}

		$this->data = new \ArrayObject($config, \ArrayObject::ARRAY_AS_PROPS);
	}

	/**
	 * @param string $name
	 *
	 * @return \ArrayObject|mixed
	 * @throws \Exception
	 */
	public function get(string $name = '') {
		if (empty($name)) {
			return $this->data;
		}

		if (!isset($this->data[$name])) {
			throw new \Exception("Параметр {$name} не существует.");
		}

		return $this->data[$name];
	}
}
