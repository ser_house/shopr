<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.11.2017
 * Time: 01:12
 */

namespace App\Common;


/**
 * Class Request.
 *
 * Набросок объекта запроса к системе, очень простой.
 * Его назначение такое же, как и обычно - инкапсулировать в себе некоторые данные реального запроса.
 *
 * @package App\Common
 */
class Request {
	// @todo: мда...
	// Вообще с этим никаких проблем нет до тех пор, пока адреса сущностей остаются простыми.
	// Вида /product/[id]. id - всегда на одной и той же позиции.
	// Но вот если появятся вложенные сущности со своими адресами
	// (вида /product/[product_id]/sites/[site_id]) - надо будет уже декларировать их uri с плейсхолдерами.
	private const URI_ENTITY_ID_POS = 1;

	/** @var string */
	private $request_uri = '';
	/** @var array */
	private $args = [];
	/** @var string */
	private $root_uri = '';

	/**
	 * Request constructor.
	 */
	public function __construct() {
		$this->request_uri = ltrim($_SERVER['REQUEST_URI'], '/');

		if (empty($this->request_uri)) {
			$this->root_uri = 'main';
		}
		else {
			$this->args = explode('/', $this->request_uri);
			$this->root_uri = array_shift($this->args);
		}
	}

	/**
	 * Это POST-запрос.
	 *
	 * @return bool
	 */
	public function isPost(): bool {
		return !empty($_POST);
	}

	/**
	 * Это Ajax-запрос.
	 *
	 * @return bool
	 */
	public function isAjax(): bool {
		// @todo: конечно, это самый распространенный способ,
		// но он не точен - клиент может и не отослать 'XMLHttpRequest',
		// а может отослать и что-нибудь другое
		// (https://stackoverflow.com/questions/18260537/how-to-check-if-the-request-is-an-ajax-request-with-php#comment59759830_21411882)
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';

		// Если сделать примерно так:
		// return in_array('ajax', $this->args);
		// с добавлением к ajax-элементам хэша (и его же в сессию) и проверкой здесь.
	}

	/**
	 * Возвращает данные POST как ассоциативный массив.
	 *
	 * @return array
	 */
	public function getPostData(): array {
		return $_POST;
	}

	/**
	 * Uri текущего запроса.
	 *
	 * @return string
	 */
	public function getRequestUri(): string {
		return $this->request_uri;
	}

	/**
	 * Возвращает список аргументов текущего запроса.
	 * Аргументами считаются все части uri, которые следуют за корневыми.
	 * Например, для uri product/1/edit аргументами будут 1 и 'edit'.
	 *
	 * @return array
	 */
	public function getArgs(): array {
		return $this->args;
	}

	/**
	 * Корневой uri запроса.
	 *
	 * @return string
	 */
	public function getRootUri(): string {
		return $this->root_uri;
	}

	/**
	 * Возвращает id сущности из текущего адреса.
	 * Если id определить не удалось, то делает перенаправление на 404.
	 *
	 * @return int
	 */
	public function getEntityId(): int {
		$parts = explode('/', $this->request_uri);

		if (empty($parts)) {
			Route::errorPage404();
		}

		if (!isset($parts[self::URI_ENTITY_ID_POS])) {
			Route::errorPage404();
		}

		return $parts[self::URI_ENTITY_ID_POS];
	}
}
