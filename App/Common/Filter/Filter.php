<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.12.2017
 * Time: 10:37
 */

namespace App\Common\Filter;


class Filter {
	static public function sanitizeString(string $var): string {
		return filter_var($var, FILTER_SANITIZE_STRING);
	}

	static public function sanitizeUrl(string $url): string {
		return filter_var($url, FILTER_SANITIZE_URL);
	}

	static public function sanitize(string $text): string {
		return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
	}

	static public function stripHtml(string $html, string $allowed_tags = '<div><span><p><br>'): string {
		return strip_tags($html, $allowed_tags);
	}
}
