<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.11.2017
 * Time: 20:19
 */

namespace App\Common\Proxy;


/**
 * Class Proxy
 *
 * @package App\Sections\Proxy
 */
class Proxy implements IProxy {
	protected $items = [];
	protected $current_index = 0;

	/**
	 * Proxy constructor.
	 *
	 * @param array $items
	 */
	public function __construct(array $items) {
		$this->items = $items;
	}

	/**
	 *
	 * @return array
	 *   список прокси вида ip:port
	 */
	public function getItems(): array {
		return $this->items;
	}

	/**
	 * @return int
	 */
	public function getItemsCount(): int {
		return count($this->items);
	}

	/**
	 * @return string
	 */
	public function getCurrentProxy(): string {
		return $this->getByIndex($this->current_index);
	}

	public function removeCurrentProxy(): void {
		array_splice($this->items, $this->current_index, 1);
	}

	/**
	 * @param int $index
	 *
	 * @return string
	 */
	public function getByIndex(int $index): string {
		return $this->items[$index] ?? '';
	}

	/**
	 * @return bool
	 */
	public function next(): bool {
		$this->current_index++;

		if ($this->current_index >= $this->getItemsCount()) {
			$this->current_index = 0;

			return false;
		}

		return true;
	}
}
