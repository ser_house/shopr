<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 18:19
 */

namespace App\Common\Proxy;

// @todo: это, собственно, должен быть репозиторий.

interface IProxy {
	/**
	 *
	 * @return array
	 *   список прокси вида ip:port
	 */
	public function getItems(): array;

	/**
	 * @return int
	 */
	public function getItemsCount(): int;

	/**
	 * @return string
	 */
	public function getCurrentProxy(): string;

	public function removeCurrentProxy(): void;

	/**
	 * @param int $index
	 *
	 * @return string
	 */
	public function getByIndex(int $index): string;

	/**
	 * @return bool
	 */
	public function next(): bool;
}
