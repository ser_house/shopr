<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.11.2017
 * Time: 11:01
 */

namespace App\Common\Proxy;


/**
 * Class ProxyFileCache
 *
 * @package App\Common\Proxy
 */
class ProxyFileCache {
	protected const CACHE_EXPIRE_MINUTES = 30;

	protected $cache_file_name = '';
	protected $cache_file = '';

	/**
	 * ProxyFileCache constructor.
	 *
	 * @param string $cache_file_name
	 * @param bool $process_expire
	 */
	public function __construct(string $cache_file_name, bool $process_expire = true) {
		$this->cache_file_name = $cache_file_name;
		$this->cache_file = TMP_DIR . "/$this->cache_file_name.cache";

		if ($process_expire) {
			$this->processCacheExpire();
		}
	}

	/**
	 * @return string
	 */
	public function getCacheFilePath(): string {
		return $this->cache_file;
	}

	/**
	 * @return string
	 */
	public function getCacheFileName(): string {
		return "$this->cache_file_name.cache";
	}

	protected function processCacheExpire() {
		if (!file_exists($this->cache_file)) {
			return;
		}

		$last_allowed_filetime = time() - self::CACHE_EXPIRE_MINUTES * 60;
		$modified = filemtime($this->cache_file);
		if ($last_allowed_filetime > $modified) {
			unlink($this->cache_file);
		}
	}

	/**
	 * @return bool
	 */
	public function cacheExists(): bool {
		return file_exists($this->cache_file);
	}

	/**
	 * @param array $items
	 */
	public function saveItems(array $items) {
		file_put_contents($this->cache_file, implode(PHP_EOL, $items));
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getItems(): array {
		if (!$this->cacheExists()) {
			throw new \Exception(__METHOD__ . ": cache doesn't exits.");
		}

		$loaderFile = new LoaderFile($this->cache_file);

		return $loaderFile->getItems();
	}
}
