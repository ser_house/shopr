<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.11.2017
 * Time: 09:35
 */

namespace App\Common\Proxy;


/**
 * Class LoaderFile
 *
 * @package App\Common\Proxy
 */
class LoaderFile {
	private const DEFAULT_FILE = TMP_DIR . '/proxy.list';

	/** @var string полный путь к файлу со списком прокси. */
	private $file = '';

	/**
	 * LoaderFile constructor.
	 *
	 * @param string $file
	 */
	public function __construct(string $file = self::DEFAULT_FILE) {
		$this->file = $file;
	}

	/**
	 * @inheritDoc
	 */
	public function getItems(): array {
		if (!file_exists($this->file)) {
			throw new \Exception("File $this->file doesn't exists.");
		}

		$items = file($this->file, FILE_IGNORE_NEW_LINES);

		if (empty($items)) {
			throw new \Exception("File $this->file is empty.");
		}

		return $items;
	}
}
