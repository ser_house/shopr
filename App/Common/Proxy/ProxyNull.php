<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 18:28
 */

namespace App\Common\Proxy;


class ProxyNull implements IProxy {

	/**
	 * @inheritDoc
	 */
	public function getItems(): array {
		return [];
	}

	/**
	 * @inheritDoc
	 */
	public function getItemsCount(): int {
		return 0;
	}

	/**
	 * @inheritDoc
	 */
	public function getCurrentProxy(): string {
		return '';
	}

	public function removeCurrentProxy(): void {

	}

	/**
	 * @inheritDoc
	 */
	public function getByIndex(int $index): string {
		return '';
	}

	/**
	 * @inheritDoc
	 */
	public function next(): bool {
		return false;
	}
}
