<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.12.2017
 * Time: 09:55
 */

namespace App\Common\Proxy;


use App\Common\Logger\ILogger;


/**
 * Class ProxyChecker
 *
 * @package App\Sections\Proxy
 */
class ProxyChecker {
	/** @var string */
	private $test_url = '';

	/**
	 * ProxyChecker constructor.
	 *
	 * @param string $test_url
	 */
	public function __construct(string $test_url = 'https://google.com') {
		$this->test_url = $test_url;
	}

	/**
	 * @param string $proxy_str
	 *
	 * @param ILogger|null $logger
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function isResponsible(string $proxy_str, ILogger $logger = null): bool {
		$user_agent = UserAgent::getUserAgentString();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->test_url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_PROXY, $proxy_str);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($ch);
		$info = curl_getinfo($ch);

		if (200 == $info['http_code']) {
			$result = true;
		}
		else {
			$result = false;
			if (!empty($logger)) {
				$err = curl_error($ch);
				$logger->write($err, 'err');
				$logger->write($info, $proxy_str);
			}
		}
		curl_close($ch);

		return $result;
	}

	/**
	 * @return string
	 */
	public function getTestUrl(): string {
		return $this->test_url;
	}

	/**
	 * @param string $test_url
	 */
	public function setTestUrl(string $test_url): void {
		$this->test_url = $test_url;
	}
}
