<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $title ?></title>
	<?= $styles ?>
</head>
<body<?= $attributes ?>>
<header>
	<?php if (!empty($main_menu)) { print $main_menu; } ?>
</header>
<?= $page ?>
<footer></footer>
<?= $scripts ?>
</body>
</html>
