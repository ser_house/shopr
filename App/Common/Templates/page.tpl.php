<?php if (!empty($messages)) {
	print $messages;
} ?>
<section<?= $attributes ?>>
	<?php if (!empty($breadcrumbs)) {
		print $breadcrumbs;
	} ?>
	<h1><?= $title ?></h1>
	<?php if (!empty($local_actions)) {
		print $local_actions;
	} ?>
	<div class="content-wrapper">
		<?php if (!empty($content)): ?>
			<?= $content ?>
		<?php endif; ?>
	</div>
</section>
