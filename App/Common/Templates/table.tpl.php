<table<?= $attributes ?>>
	<thead>
	<tr>
		<?php foreach ($columns as $column): ?>
			<th<?= $column['attributes'] ?>><?= $column['content'] ?></th>
		<?php endforeach; ?>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($rows as $row): ?>
		<tr<?= $row['attributes'] ?>>
			<?php foreach ($row['content'] as $cell): ?>
				<td<?= $cell['attributes'] ?>><?= $cell['content'] ?></td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
