<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.12.2017
 * Time: 20:40
 */

namespace App\Common\LoaderHtml;

use App\Common\Logger\ILogger;
use App\Common\Logger\LoggerNull;
use App\Common\Proxy\IProxy;
use App\Common\Proxy\UserAgent;


/**
 * Class LoaderHtml
 *
 * @package App\Common\LoaderHtml
 */
class LoaderHtml implements ILoaderHtml {
	protected const CONNECT_TIMEOUT = 5;
	protected const TIMEOUT = 90;

	/** @var string */
	protected $url = '';

	/** @var IProxy */
	protected $proxy = null;

	/** @var ILogger */
	protected $logger = null;


	/**
	 * LoaderHtml constructor.
	 *
	 * @param string $url
	 * @param ILogger|null $logger
	 * @param IProxy|null $proxy
	 */
	public function __construct(string $url, ILogger $logger = null,  IProxy $proxy = null) {
		$this->url = $url;
		// Прокси удобно проверять на null.
		$this->proxy = $proxy;
		// А вот вызов записи в лог удобнее делать без проверок на null объекта-логгера.
		$this->logger = $logger ?? new LoggerNull();
	}

	/**
	 * @inheritDoc
	 */
	public function getHtml() {
		$this->logger->write($this->url, 'url');

		$ch = curl_init();

		$user_agent = UserAgent::getUserAgentString();

		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECT_TIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		if ($this->proxy) {
			$proxy_str = $this->proxy->getCurrentProxy();
			if (!empty($proxy_str)) {
				curl_setopt($ch, CURLOPT_PROXY, $proxy_str);
			}
		}

		$data = curl_exec($ch);
		$errno = curl_errno($ch);

		if (empty($errno)) {
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->logger->write($http_code, 'http code');

			if (200 != $http_code) {
				$info = curl_getinfo($ch);
				$this->logger->write($info, 'info');
			}
		}
		else {
			$error = curl_error($ch);
			$this->logger->write($error, 'curl error');
		}

		curl_close($ch);

		if (empty($data)) {
			throw new \Exception("Не удалось загрузить страницу $this->url");
		}

		return $data;
	}
}
