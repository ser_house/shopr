<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.12.2017
 * Time: 20:46
 */

namespace App\Common\LoaderHtml;


/**
 * Interface ILoaderHtml
 *
 * @package App\Common\LoaderHtml
 */
interface ILoaderHtml {
	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getHtml();
}
