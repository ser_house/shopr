<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 04:11
 */

namespace App\Common;

/**
 * Class Cache
 */
class Cache {

	/**
	 * Содранный откуда-то из процедурного статический кэш.
	 *
	 * @param $name
	 * @param null $default_value
	 * @param bool $reset
	 *
	 * @return array|mixed
	 */
	static public function &static($name, $default_value = null, $reset = false) {
		static $data = [], $default = [];
		// First check if dealing with a previously defined static variable.
		if (isset($data[$name]) || array_key_exists($name, $data)) {
			// Non-NULL $name and both $data[$name] and $default[$name] statics exist.
			if ($reset) {
				// Reset pre-existing static variable to its default value.
				$data[$name] = $default[$name];
			}

			return $data[$name];
		}
		// Neither $data[$name] nor $default[$name] static variables exist.
		if (isset($name)) {
			if ($reset) {
				// Reset was called before a default is set and yet a variable must be
				// returned.
				return $data;
			}
			// First call with new non-NULL $name. Initialize a new static variable.
			$default[$name] = $data[$name] = $default_value;

			return $data[$name];
		}
		// Reset all: ($name == NULL). This needs to be done one at a time so that
		// references returned by earlier invocations of drupal_static() also get
		// reset.
		foreach ($default as $name => $value) {
			$data[$name] = $value;
		}
		// As the function returns a reference, the return should always be a
		// variable.
		return $data;
	}
}
