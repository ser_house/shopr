<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 11:21
 */

namespace App\Common\Event;


/**
 * Class Event.
 *
 * @package App\Common\Event
 */
class Event {
	public const PRICE_CHECKED = 1;
}
