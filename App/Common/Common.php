<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 19:29
 */

namespace App\Common;


/**
 * Class Common.
 *
 * Конечно, в идеале это должны быть просто функции в своём пространстве имен.
 * Организация кода была бы более правильная, да.
 * Но пришлось бы прикручивать автозагрузку этих функций (либо вручную отслеживать),
 * а со статическими методами класса она у нас уже есть.
 *
 * @package App\Common
 */
class Common {

	/**
	 * Собирает значения указанного поля (для объектов) или ключа (для массивов)
	 * в ассоциативный массив.
	 *
	 * @param array $items список объектов или массивов
	 * @param string $field поле, значения которого надо свести в массив.
	 *  Если передан список объектов, то это поле у их типа должно быть публичным.
	 *
	 * @return array ассоциативный массив значений:
	 *   $value => $value
	 */
	static public function getFieldValuesList(array $items, string $field = 'id'): array {
		$values = [];

		foreach ($items as $item) {
			if (is_array($item) && isset($item[$field])) {
				$value = $item[$field];
				$values[$value] = $value;
			}
			elseif (is_object($item) && isset($item->$field)) {
				$value = $item->$field;
				$values[$value] = $value;
			}
		}

		return $values;
	}

	/**
	 * Собирает переданные объекты/массивы из списка в ассоциативный массив
	 * с индексацией по значению указанного поля.
	 * Если значения свойства одинаковые у нескольких объектов/массивов,
	 * то в массиве останется последний из них.
	 *
	 * @param array $items
	 *
	 * @return array
	 */
	static public function mapToIndexed(array $items, string $field = 'id'): array {
		$indexed = [];

		foreach ($items as $item) {
			if (is_array($item) && isset($item[$field])) {
				$key = $item[$field];
				$indexed[$key] = $item;
			}
			elseif (is_object($item) && isset($item->$field)) {
				$key = $item->$field;
				$indexed[$key] = $item;
			}
		}

		return $indexed;
	}

	/**
	 * Помимо обычного trim убирает еще и слэши по краям строки.
	 *
	 * @param string $url
	 *
	 * @return string
	 */
	static public function trimUrl(string $url): string {
		$url = trim($url);
		return trim($url, '/');
	}

	/**
	 * Пытается получить и вернуть хост из переданного адреса.
	 *
	 * @param string $url
	 *
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	static public function getHost(string $url): string {
		if (empty($url)) {
			throw new \InvalidArgumentException("Отсутствует аргумент url: $url");
		}

		$host = parse_url($url, PHP_URL_HOST);
		if (empty($host)) {
			throw new \InvalidArgumentException("Не удалось получить хост из url: $url");
		}

		return $host;
	}
}
