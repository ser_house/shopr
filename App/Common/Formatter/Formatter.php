<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.12.2017
 * Time: 18:33
 */

namespace App\Common\Formatter;


use App\Common\Html\Component;


class Formatter {
	static public function formatDateFromTimestamp(int $timestamp, string $format = 'd.m.Y H:i'): string {
		return date($format, $timestamp);
	}

	static public function formatDateFromString(string $datetime, string $format = 'd.m.Y H:i'): string {
		return self::formatDateFromTimestamp(strtotime($datetime), $format);
	}

	static public function formatMoney(int $amount): string {
		return number_format($amount / 100, 0, ',', ' ') . 'р.';
	}

	static public function formatTrackedValue(bool $tracked, string $tracked_value, string $not_tracked_value): string {
		return $tracked ? $tracked_value : $not_tracked_value;
	}

	static public function formatBytes(int $bytes, int $precision = 2): string {
		$base = log($bytes, 1024);
		$suffixes = ['', 'K', 'M', 'G', 'T'];
		$suffix = (int)floor($base);

		return round(pow(1024, $base - floor($base)), $precision) . $suffixes[$suffix];
	}

	static public function formatTrackedHtml(bool $tracked, string $tracked_value = 'да', string $not_tracked_value = 'нет'): Component {
		$class = $tracked ? 'tracked' : 'not-tracked';
		$value = self::formatTrackedValue($tracked, $tracked_value, $not_tracked_value);
		$attr = [
			'class' => [$class],
		];
		return new Component($attr, 'span', $value);
	}

}
