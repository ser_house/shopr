<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.10.2017
 * Time: 16:53
 */

namespace App\Common\Database;


/**
 * Class DbConnection
 *
 * @package App\Common\Database
 */
class DbConnection {
	/**
	 * @var DbConfiguration
	 */
	private $configuration;

	/**
	 * @param DbConfiguration $config
	 */
	public function __construct(DbConfiguration $config) {
		$this->configuration = $config;
	}

	/**
	 * @return string
	 */
	public function getDsn(): string {
		$config = $this->configuration;

		$host = $config->getHost();
		$dbname = $config->getDbname();
		$charset = $config->getCharset();

		return "mysql:host=$host;dbname=$dbname;charset=$charset";
	}
}
