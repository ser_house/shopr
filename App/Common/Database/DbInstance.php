<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.10.2017
 * Time: 17:34
 */

namespace App\Common\Database;

use PDO;

/**
 * Class DbInstance
 *
 * @package App\Common\Database
 */
class DbInstance {
	static private $instance = null;
	/** @var Db */
	static private $db = null;

	private function __construct() {
		$dbConf = new DbConfiguration();
		$dbConnection = new DbConnection($dbConf);
		$dsn = $dbConnection->getDsn();

		// В базе время хранится в UTC (TIMESTAMP),
		// чтобы база сама учитывала при запросах зону текущего пользователя - надо её указать базе.
		// Для текущего клиента это делает "SET time_zone".
		// И в базу предварительно должны быть загружены актуальные временные зоны системы (Debian 9):
		// mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u[user] -p[password] --force mysql
		// А если бы был пользователь, то было бы что-нибудь вроде $user->timezone
		$tz = date_default_timezone_get();

		$opt = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = '$tz'",
		];
		self::$db = new Db($dsn, $dbConf->getUsername(), $dbConf->getPassword(), $opt);
	}

	private function __clone() {
	}

	/** @noinspection PhpUnusedPrivateMethodInspection */
	private function __wakeup() {
	}

	/**
	 * @return Db
	 */
	static public function getInstance() {
		if (self::$instance == null) {
			self::$instance = new static();
		}

		return self::$db;
	}
}
