<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.10.2017
 * Time: 16:52
 */

namespace App\Common\Database;

use App\Common\App;


/**
 * Class DbConfiguration
 *
 * @package App\Common\Database
 */
class DbConfiguration {
	/**
	 * @var string
	 */
	private $host;

	/**
	 * @var int
	 */
	private $port;

	/**
	 * @var string
	 */
	private $dbname;

	/**
	 * @var string
	 */
	private $charset;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $password;

	public function __construct() {
		$db_conf = App::getInstance()->getConfigValue('db');

		$this->host = $db_conf['host'];
		$this->dbname = $db_conf['dbname'];
		$this->charset = $db_conf['charset'];
		$this->port = isset($db_conf['port']) ? $db_conf['port'] : 3306;
		$this->username = $db_conf['user'];
		$this->password = $db_conf['pass'];
	}

	/**
	 * @return string
	 */
	public function getHost(): string {
		return $this->host;
	}

	/**
	 * @return int
	 */
	public function getPort(): int {
		return $this->port;
	}

	/**
	 * @return string
	 */
	public function getDbname(): string {
		return $this->dbname;
	}

	/**
	 * @return string
	 */
	public function getCharset(): string {
		return $this->charset;
	}

	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}
}
