<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.12.2017
 * Time: 11:23
 */

namespace App\Common\Database;


/**
 * Class Select
 *
 * @package App\Common\Database
 */
class Select {
	/**
	 * @var string
	 */
	protected $table = '';
	/**
	 * @var array
	 */
	protected $fields = [];
	/**
	 * @var array
	 */
	protected $joins = [];
	/**
	 * @var array
	 */
	protected $where = [];
	/**
	 * @var array
	 */
	protected $orderBy = [];
	/**
	 * @var int
	 */
	protected $limit = 0;

	/**
	 * Select constructor.
	 *
	 * @param string $table
	 */
	public function __construct(string $table) {
		$this->table = $table;
	}

	/**
	 * @param string $table
	 */
	public function setTable(string $table) {
		$this->table = $table;
	}

	/**
	 * @param string $fields
	 */
	public function addFields(string $fields) {
		$this->fields[] = $fields;
	}

	/**
	 * @param string $join
	 */
	public function addJoin(string $join) {
		$this->joins[] = $join;
	}

	/**
	 * @param string $where
	 */
	public function addWhere(string $where) {
		$this->where[] = $where;
	}

	/**
	 * @param string $order_by
	 */
	public function addOrderBy(string $order_by) {
		$this->orderBy[] = $order_by;
	}

	/**
	 * @param int $limit
	 */
	public function setLimit(int $limit) {
		$this->limit = $limit;
	}

	/**
	 *
	 * @return string
	 */
	public function getSql(): string {
		$sql = "SELECT " . implode(',', $this->fields);
		$sql .= " FROM $this->table";

		if (!empty($this->joins)) {
			$sql .= ' ' . implode(' ', $this->joins);
		}

		if (!empty($this->where)) {
			$sql .= " WHERE " . implode(' AND ', $this->where);
		}

		if (!empty($this->orderBy)) {
			$sql .= " ORDER BY " . implode(',', $this->orderBy);
		}

		if (!empty($this->limit)) {
			$sql .= " LIMIT $this->limit";
		}

		return $sql;
	}
}
