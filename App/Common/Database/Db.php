<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.11.2017
 * Time: 08:47
 */

namespace App\Common\Database;

use PDO;

/**
 * Class Db
 *
 * @package App\Common\Database
 */
class Db extends PDO {
	/**
	 * @var int для вложенных транзакций,
	 * см. {@link http://php.net/manual/ru/pdo.begintransaction.php }
	 */
	protected $transactionCount = 0;

	/**
	 * @return bool
	 */
	public function beginTransaction() {
		if (!$this->transactionCount++) {
			return parent::beginTransaction();
		}
		$this->exec('SAVEPOINT trans' . $this->transactionCount);

		return $this->transactionCount >= 0;
	}

	/**
	 * @return bool
	 */
	public function commit() {
		if (!--$this->transactionCount) {
			return parent::commit();
		}

		return $this->transactionCount >= 0;
	}

	/**
	 * @return bool
	 */
	public function rollback() {
		if (--$this->transactionCount) {
			$this->exec('ROLLBACK TO trans' . ($this->transactionCount + 1));

			return true;
		}

		return parent::rollback();
	}

	/**
	 * Готовит из списка значений параметры для условия IN() запроса.
	 *
	 * После вызова метода и получения массива $params использовать:
	 *
	 * для вставки в строку запроса
	 * <pre>
	 * 	$params_str = implode(',', array_keys($params));
	 * </pre>
	 *
	 * для связывания:
	 * <pre>
	 * $stmt = $db->prepare($sql);
	 * foreach ($params as $key => &$value) {
	 * 	$stmt->bindParam($key, $value, \PDO::PARAM_INT);
	 * }
	 * </pre>
	 *
	 * @param array $values
	 *
	 * @return array
	 */
	public function prepareIN(array $values): array {
		$values = array_values($values);

		$params = [];
		foreach($values as $i => $id) {
			$params[":var_$i"] = $id;
		}

		return $params;
	}
}
