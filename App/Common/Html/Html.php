<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.10.2017
 * Time: 18:12
 */

namespace App\Common\Html;

use App\Common\Assets\IAssets;
use App\Common\Assets\AssetsFiles;

/**
 * Class Html
 *
 * @package App\Common\Html
 */
class Html extends Component {
	/** @var string */
	protected $title = '';

	/** @var Page */
	protected $page = null;

	/** @var AssetsFiles */
	protected $assetsCss = null;
	/** @var AssetsFiles */
	protected $assetsJs = null;

	/**
	 * Html constructor.
	 *
	 * @param string $title
	 * @param Page|null $page
	 */
	public function __construct(string $title, Page $page = null) {
		parent::__construct();

		$this->assetsCss = new AssetsFiles();
		$this->assetsJs = new AssetsFiles();

		$this->tpl_file = TPL_DIR . '/html.tpl.php';

		$this->setTitle($title);

		$this->addCss('css/main.css');

		if (empty($page)) {
			$page = new Page($title);
		}

		$this->page = $page;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->addContent($title, 'title');
	}

	/**
	 * @param Page $page
	 */
	public function setPage(Page $page) {
		$this->page = $page;
	}

	/**
	 * @return Page
	 */
	public function getPage(): Page {
		return $this->page;
	}

	/**
	 * @return Component[]
	 */
	public function getContent(): array {
		$elements = $this->content;
		$elements['page'] = $this->page;

		return $elements;
	}

	/**
	 * Добавляет файл стилей.
	 *
	 * @param string $css_file если $absolute == false, то путь к файлу стилей
	 * 	относительно директории public_html/assets, например: 'css/main.css'.
	 * 	Если $absolute == true, то абсолютный адрес файла стилей.
	 * @param bool $absolute абсолютный или относительный путь к файлу стилей?
	 */
	public function addCss(string $css_file, bool $absolute = false): void {
		$this->assetsCss->addItem($css_file, $absolute);
	}

	/**
	 * Добавляет файл скрипта js.
	 *
	 * @param string $js_file путь к файлу js относительно директории public_html/assets.
	 *   Например: 'js/product/form/form.js'.
	 */
	public function addJs(string $js_file): void {
		$this->assetsJs->addItem($js_file);
	}

	/**
	 * Возвращает набор файлов стилей.
	 *
	 * @return IAssets
	 */
	public function getAssetsCss(): IAssets {
		return $this->assetsCss;
	}

	/**
	 * Возвращает набор файлов скриптов js.
	 *
	 * @return IAssets
	 */
	public function getAssetsJs(): IAssets {
		return $this->assetsJs;
	}
}
