<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.11.2017
 * Time: 12:02
 */

namespace App\Common\Html;


/**
 * Class Label
 *
 * @package App\Common\Html
 */
class Label extends Component {
	/**
	 * Label constructor.
	 *
	 * @param string $text
	 * @param array $attributes
	 */
	public function __construct(string $text, array $attributes = []) {
		parent::__construct($attributes, 'label');

		$this->addContent($text, 'text');
	}
}
