<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.10.2017
 * Time: 11:02
 */

namespace App\Common\Html;


/**
 * Class Messages
 *
 * @package App\Common\Html
 */
class Messages extends Component {

	/**
	 * Messages constructor.
	 *
	 * @param array $messages
	 * @param array $attributes
	 */
	public function __construct(array $messages, array $attributes = []) {
		$attributes['class'][] = 'messages';

		parent::__construct($attributes, 'div');

		foreach ($messages as $type => $type_messages) {
			$attr = [
				'class' => [
					$type,
				],
			];
			$msgElement = new Component($attr, 'ul');

			foreach ($type_messages as $message) {
				$item = new Component([], 'li', $message);
				$msgElement->addContent($item);
			}

			$this->addContent($msgElement);
		}
	}
}
