<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.11.2017
 * Time: 15:25
 */

namespace App\Common\Html;


/**
 * Class Checkbox
 *
 * @package App\Common\Html
 */
class Checkbox extends Component {

	/**
	 * Checkbox constructor.
	 *
	 * @param bool $is_checked
	 * @param string $label
	 * @param array $attributes
	 */
	public function __construct(bool $is_checked = false, string $label = '', array $attributes = []) {
		$attributes['label'] = $label;
		$attributes['type'] = 'checkbox';
		$attributes['checked'] = $is_checked;
		$attributes['value'] = 1;

		parent::__construct($attributes, 'input');
	}
}
