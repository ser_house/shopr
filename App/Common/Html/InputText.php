<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.11.2017
 * Time: 14:57
 */

namespace App\Common\Html;


/**
 * Class InputText
 *
 * @package App\Common\Html
 */
class InputText extends Component {

	/**
	 * InputText constructor.
	 *
	 * @param string $default_value
	 * @param string $label
	 * @param array $attributes
	 */
	public function __construct(string $default_value = '', string $label = '', array $attributes = []) {
		$attributes['label'] = $label;
		$attributes['value'] = $default_value;
		$attributes['type'] = 'text';

		parent::__construct($attributes, 'input');
	}
}
