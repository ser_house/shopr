<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.11.2017
 * Time: 08:52
 */

namespace App\Common\Html;

use App\Common\Assets\IAssets;
use App\Common\Assets\AssetsNull;

/**
 * Class Component
 *
 * @package App\Common\Html
 */
class Component implements IComponent {
	/** @var array */
	protected $attributes = [];

	// @todo: наличие и тега и файла шаблона нехорошо, ибо нарушает принцип единой ответственности.

	/** @var string */
	protected $tag = '';

	/** @var array */
	protected $content = [];

	/** @var string необязательный файл шаблона */
	protected $tpl_file = '';


	/**
	 * Component constructor.
	 *
	 * @param array $attributes
	 * @param string $tag
	 * @param string $content
	 */
	public function __construct(array $attributes = [], string $tag = '', $content = '') {
		$this->attributes = $attributes;
		$this->tag = $tag;

		if (empty($content)) {
			return;
		}

		if (is_array($content)) {
			foreach ($content as $item) {
				$this->addContent($item);
			}
		}
		else {
			$this->addContent($content, 'content');
		}
	}

	/**
	 * @param string|Component|Component[] $content
	 * @param string $key
	 *
	 * @return void
	 */
	public function addContent($content, string $key = ''): void {
		if (empty($key)) {
			$key = '_' . count($this->content);
		}

		$this->content[$key] = $content;
	}

	/**
	 * @return array
	 */
	public function getContent(): array {
		return $this->content;
	}

	/**
	 * @return array
	 */
	public function getAttributes(): array {
		return $this->attributes;
	}

	/**
	 *
	 * @param array $attributes
	 */
	public function setAttributes(array $attributes): void {
		$this->attributes = array_merge_recursive($this->attributes, $attributes);
	}

	/**
	 * Устанавливает произвольный атрибут с произвольным значением.
	 *
	 * @param string $attribute
	 * @param $value
	 */
	public function setAttribute(string $attribute, $value): void {
		$this->attributes[$attribute] = $value;
	}

	/**
	 * Добавляет $class_css в набор классов css.
	 * Метод не проверяет классы на дублирование.
	 *
	 * @param string $class_css
	 */
	public function addClassCss(string $class_css): void {
		$this->attributes['class'][] = $class_css;
	}

	/**
	 * @return string
	 */
	public function getTag(): string {
		return $this->tag;
	}

	/**
	 * Любой компонент может использовать файл шаблона,
	 * который может находиться в любом логически разумном месте
	 * (например, в директории отдельной подсистемы, модуля, сущности).
	 * Поэтому файл шаблона определяется полным путем.
	 *
	 * @return string
	 */
	public function getTplFile(): string {
		return $this->tpl_file;
	}

	/**
	 * Любой компонент может использовать файл шаблона,
	 * который может находиться в любом логически разумном месте
	 * (например, в директории отдельной подсистемы, модуля, сущности).
	 * Поэтому файл шаблона определяется полным путем.
	 *
	 * @param string $tpl_file полный путь до файла шаблона.
	 */
	public function setTplFile(string $tpl_file): void {
		$this->tpl_file = $tpl_file;
	}

	/**
	 * При рендеринге нет разницы между Html и отдельными элементами,
	 * поэтому у каждого элемента есть заглушка для согласованности интерфейса.
	 * @todo: конечно, не очень хорошо придумано.
	 *
	 * @return IAssets
	 */
	public function getAssetsCss(): IAssets {
		return new AssetsNull();
	}

	/**
	 * При рендеринге нет разницы между Html и отдельными элементами,
	 * поэтому у каждого элемента есть заглушка для согласованности интерфейса.
	 * @todo: конечно, не очень хорошо придумано.
	 *
	 * @return IAssets
	 */
	public function getAssetsJs(): IAssets {
		return new AssetsNull();
	}
}
