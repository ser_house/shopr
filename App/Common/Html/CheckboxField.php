<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.11.2017
 * Time: 15:27
 */

namespace App\Common\Html;


/**
 * Class CheckboxField
 *
 * @package App\Common\Html
 */
class CheckboxField extends Component {

	/**
	 * CheckboxField constructor.
	 *
	 * @param bool $is_checked
	 * @param string $label
	 * @param array $input_attributes
	 */
	public function __construct(bool $is_checked = false, string $label = '', array $input_attributes = []) {
		$attributes = [
			'class' => [
				'form-item',
				'form-checkbox',
			],
		];
		parent::__construct($attributes);

		$checkbox = new Checkbox($is_checked, $label, $input_attributes);

		$this->addContent($checkbox, 'checkbox');
	}
}
