<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.12.2017
 * Time: 17:05
 */

namespace App\Common\Html;


class NoData extends Component {

	/**
	 * NoData constructor.
	 *
	 * @param string $message
	 * @param array $attributes
	 */
	public function __construct(string $message, array $attributes = []) {
		parent::__construct($attributes, 'div', $message);
	}
}
