<?php
/**
 * Created by PhpStorm.
 * User: Me
 * Date: 19.11.2017
 * Time: 12:19
 */

namespace App\Common\Html;


/**
 * Class Page
 *
 * @package App\Common\Html
 */
class Page extends Component {

	/**
	 * Page constructor.
	 *
	 * @param string $title
	 * @param array $attributes
	 */
	public function __construct(string $title, array $attributes = []) {
		$attributes['id'] = 'page';
		$attributes['class'][] = 'page';

		parent::__construct($attributes);

		$this->tpl_file = TPL_DIR . '/page.tpl.php';

		$this->setTitle($title);
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->content['title'] = $title;
	}

	public function setMessages(Messages $messages): void {
		$this->content['messages'] = $messages;
	}

	public function setBreadcrumbs(Breadcrumbs $breadcrumbs): void {
		$this->content['breadcrumbs'] = $breadcrumbs;
	}

	/**
	 * @param string|Component $content
	 */
	public function setContent($content) {
		$this->addContent($content, 'content');
	}

	/**
	 * @inheritDoc
	 */
	public function addContent($content, string $key = ''): void {
		if (empty($key)) {
			$key = '_' . (isset($this->content['content']) ? count($this->content['content']) : 0);
		}
		$this->content['content'][$key] = $content;
	}
}
