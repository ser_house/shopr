<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.10.2017
 * Time: 09:23
 */

namespace App\Common\Html;


/**
 * Class Breadcrumbs
 *
 * @package App\Common\Html
 */
class Breadcrumbs extends Menu {
	/**
	 * Breadcrumbs constructor.
	 *
	 * @param array $items
	 * @param array $attributes
	 */
	public function __construct(array $items, array $attributes = []) {
		parent::__construct($items, $attributes);

		$this->attributes['class'] = array_diff($this->attributes['class'], ['menu']);
		$this->attributes['class'][] = 'breadcrumbs';
	}
}
