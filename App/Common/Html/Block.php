<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2017
 * Time: 10:23
 */

namespace App\Common\Html;


/**
 * Class Block
 *
 * @package App\Common\Html
 */
class Block extends Component {

	/**
	 * Block constructor.
	 *
	 * @param string $title
	 * @param string|Component $content
	 * @param array $attributes
	 */
	public function __construct(string $title = '', $content = '', array $attributes = []) {
		$attributes['class'][] = 'block';
		parent::__construct($attributes, 'div', $content);

		if (!empty($title)) {
			$this->setTitle($title);
		}
	}

	public function setTitle(string $title): void {
		$attr = [
			'class' => [
				'lined',
			],
		];
		$blockTitle = new Component($attr, 'h3', $title);
		$this->addContent($blockTitle, 'title');
	}
}
