<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.12.2017
 * Time: 11:58
 */

namespace App\Common\Html;


class Form extends Component {

	/**
	 * Form constructor.
	 *
	 * @param array $attributes
	 * @param string $method
	 * @param string $action
	 */
	public function __construct(array $attributes = [], string $method = 'post', string $action = '') {

		$attributes['method'] = $method;
		if (!empty($action)) {
			$attributes['action'] = $action;
		}
		parent::__construct($attributes, 'form');
	}

	public function setAction(string $action): void {
		$this->attributes['action'] = $action;
	}

	public function setMethod(string $method): void {
		$this->attributes['method'] = $method;
	}

	public function addTextField(string $name, string $label, string $default_value = '', array $attributes = []): Component {
		$required = !empty($attributes['required']);

		$field_attr = [
			'class' => ['form-text'],
		];
		if ($required) {
			$field_attr['class'][] = 'required';
		}
		$field = $this->addFieldWrapper($name, $field_attr);

		$attributes['name'] = $name;
		$input = new InputText($default_value, $label, $attributes);

		$field->addContent($input, $name);

		return $field;
	}

	public function addTextareaField(string $name, string $label, string $default_value = '', array $attributes = []): Component {
		$required = !empty($attributes['required']);

		$field_attr = [
			'class' => ['form-textarea'],
		];
		if ($required) {
			$field_attr['class'][] = 'required';
		}
		$field = $this->addFieldWrapper($name, $field_attr);

		$attributes['name'] = $name;
		$input = new Textarea($default_value, $label, $attributes);

		$field->addContent($input, $name);

		return $field;
	}

	public function addCheckboxField(string $name, string $label, bool $checked = false, array $attributes = []): Component {
		$field_attr = [
			'class' => ['form-checkbox'],
		];
		$field = $this->addFieldWrapper($name, $field_attr);

		$attributes['name'] = $name;
		$checkbox = new Checkbox($checked, $label, $attributes);

		$field->addContent($checkbox, $name);

		return $field;
	}

	public function addSelectField(string $name, string $label, array $options, $default_value, array $attributes = []): Component {
		$field_attr = [
			'class' => ['form-select'],
		];
		$field = $this->addFieldWrapper($name, $field_attr);

		$select = new Select($default_value, $options, $label, $attributes);

		$field->addContent($select, $name);

		return $field;
	}

	public function addHiddenInput(string $name, $default_value, array $attributes = []): Component {
		$attributes['name'] = $name;
		$input = new InputHidden($default_value, $attributes);
		$this->addContent($input, $name);

		return $input;
	}

	public function addActionsBlock(string $key = 'actions', array $attributes = []): Component {
		$attributes['class'][] = 'form-actions';
		$actions = new Component($attributes, 'div');
		$this->addContent($actions, $key);

		return $actions;
	}

	public function addFieldWrapper(string $key, array $attributes = []): Component {
		$attributes['class'][] = 'form-item';
		$field = new Component($attributes, 'div');
		$this->addContent($field, $key);
		return $field;
	}
}
