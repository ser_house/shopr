<?php
/**
 * Created by PhpStorm.
 * User: Me
 * Date: 20.11.2017
 * Time: 12:36
 */

namespace App\Common\Html;


/**
 * Class InputHidden
 *
 * @package App\Common\Html
 */
class InputHidden extends Component {
	/**
	 * InputHidden constructor.
	 *
	 * @param string $default_value
	 * @param array $attributes
	 */
	public function __construct(string $default_value = '', array $attributes = []) {
		$attributes['value'] = $default_value;
		$attributes['type'] = 'hidden';

		parent::__construct($attributes, 'input');
	}
}
