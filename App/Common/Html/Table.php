<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.10.2017
 * Time: 13:34
 */

namespace App\Common\Html;


/**
 * Class Table
 *
 * @package App\Common\Html
 */
class Table extends Component {
	/** @var Component[] */
	protected $columns = [];
	/** @var Component[] */
	protected $rows = [];


	/**
	 * Table constructor.
	 *
	 * @param Component[] $columns
	 * @param Component[] $rows
	 * @param array $attributes
	 */
	public function __construct(array $columns, array $rows, array $attributes = []) {
		parent::__construct($attributes);

		$this->tpl_file = TPL_DIR . '/table.tpl.php';

		$this->columns = $columns;
		$this->rows = $rows;
	}

	/**
	 * @return Component[]
	 */
	public function getColumns(): array {
		return $this->columns;
	}

	/**
	 * @return Component[]
	 */
	public function getRows(): array {
		return $this->rows;
	}
}
