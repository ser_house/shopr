<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.11.2017
 * Time: 20:33
 */

namespace App\Common\Html;


/**
 * Class Select
 *
 * @package App\Common\Html
 */
class Select extends Component {

	/**
	 * Select constructor.
	 *
	 * @param string $default_value
	 * @param array $options
	 * @param string $label
	 * @param array $attributes
	 */
	public function __construct(string $default_value = '', array $options = [], string $label = '', array $attributes = []) {
		$attributes['label'] = $label;
		parent::__construct($attributes, 'select');

		foreach ($options as $value => $option) {
			$attr = [
				'value' => $value,
			];
			if ($default_value == $value) {
				$attr['selected'] = true;
			}
			$optionElement = new Component($attr, 'option', $option);
			$this->addContent($optionElement);
		}
	}
}
