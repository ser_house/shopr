<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.11.2017
 * Time: 11:36
 */

namespace App\Common\Html;


/**
 * Class Button
 *
 * @package App\Common\Html
 */
class Button extends Component {

	/**
	 * Button constructor.
	 *
	 * @param string $label
	 * @param array $attributes
	 * @param string $type button|submit|reset
	 */
	public function __construct(string $label, array $attributes = [], string $type = 'button') {
		$attributes['type'] = !empty($type) ? $type : 'button';

		parent::__construct($attributes, 'button', $label);
	}
}
