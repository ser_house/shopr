<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 15:55
 */

namespace App\Common\Html;


/**
 * Class Link
 *
 * @package App\Common\Html
 */
class Link extends Component {

	/**
	 * Link constructor.
	 *
	 * @param string $text
	 * @param string $url
	 * @param array $attributes
	 */
	public function __construct(string $text, string $url, array $attributes = []) {

		$attributes['href'] = !empty($url) ? $url : '/';

		parent::__construct($attributes, 'a', $text);
	}
}
