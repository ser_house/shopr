<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.11.2017
 * Time: 12:00
 */

namespace App\Common\Html;


/**
 * Class InlineLabel
 *
 * @package App\Common\Html
 */
class InlineLabel extends Component {
	/**
	 * InlineLabel constructor.
	 *
	 * @param string $text
	 * @param string $value
	 * @param array $attributes
	 */
	public function __construct(string $text, string $value, array $attributes = []) {
		$attributes['class'][] = 'container-inline';

		parent::__construct($attributes, 'div');

		$label = new Label($text);
		$this->addContent($label, 'label');

		$value = new Component([], 'span', $value);
		$this->addContent($value, 'value');
	}
}
