<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.10.2017
 * Time: 19:04
 */

namespace App\Common\Html;


/**
 * Class Menu
 *
 * @package App\Common\Html
 */
class Menu extends Component {
	/**
	 * Menu constructor.
	 *
	 * @param array $items
	 * @param array $attributes
	 */
	public function __construct(array $items, array $attributes = []) {
		$attributes['class'][] = 'menu';

		parent::__construct($attributes, 'ul');

		foreach ($items as $item) {
			// Если ссылка не установлена, то надо вывести просто текст:
			// может быть это неактивный пункт меню или наоборот - активный (текущая страница).
			// Главная польза от такого - для хлебных крошек, где последний пункт хорошо бы вывести,
			// но ссылкой будет плохо. Но и для меню может быть полезно.
			if (isset($item['href'])) {
				$link = new Link($item['title'], $item['href']);
			}
			else {
				$link = $item['title'];
			}
			$linkElement = new Component([], 'li', $link);
			$this->addContent($linkElement);
		}
	}
}
