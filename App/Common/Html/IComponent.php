<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.12.2017
 * Time: 13:14
 */

namespace App\Common\Html;


/**
 * Interface IComponent
 *
 * @package App\Common\Html
 */
interface IComponent {
	/**
	 *
	 * @return IComponent[]
	 */
	public function getContent(): array;

	/**
	 *
	 * @return array
	 */
	public function getAttributes(): array;

	/**
	 *
	 * @return string
	 */
	public function getTag(): string;

	/**
	 *
	 * @return string
	 */
	public function getTplFile(): string;
}
