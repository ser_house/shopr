<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.12.2017
 * Time: 11:04
 */

namespace App\Common\Html;


class Textarea extends Component {
	/**
	 * Textarea constructor.
	 *
	 * @param string $default_value
	 * @param string $label
	 * @param array $attributes
	 */
	public function __construct(string $default_value = '', string $label = '', array $attributes = []) {
		$attributes['label'] = $label;

		parent::__construct($attributes, 'textarea');

		if (!empty($default_value)) {
			$this->addContent($default_value);
		}
	}
}
