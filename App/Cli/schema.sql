DROP TABLE IF EXISTS price;
DROP TABLE IF EXISTS last_price;
DROP TABLE IF EXISTS product_page;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS site;

CREATE TABLE product (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	created INT(11) UNSIGNED NOT NULL,
	title VARCHAR(255) NOT NULL,
	tracked TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Отслеживание включено.',
	PRIMARY KEY (id)
)
	COMMENT ='Товар.'
	COLLATE = utf8_general_ci
	ENGINE = InnoDB
	AUTO_INCREMENT = 1;

CREATE TABLE site (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	host VARCHAR(45) NOT NULL,
	title VARCHAR(45) NOT NULL,
	parser VARCHAR(45) NOT NULL,
	PRIMARY KEY (id)
)
	COMMENT ='Сайт.'
	COLLATE = utf8_general_ci
	ENGINE = InnoDB;

CREATE TABLE product_page (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT(11) UNSIGNED NOT NULL,
	site_id INT(11) UNSIGNED NOT NULL,
	url VARCHAR(255) NOT NULL,
	tracked TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Отслеживание включено.',
	PRIMARY KEY (id),
	UNIQUE KEY (product_id, site_id),
	CONSTRAINT pp_prdfk FOREIGN KEY (product_id) REFERENCES product (id)
		ON DELETE CASCADE,
	CONSTRAINT pp_sitefk FOREIGN KEY (site_id) REFERENCES site (id)
		ON DELETE CASCADE
)
	COMMENT ='Страница товара на конкретном сайте.'
	COLLATE = utf8_general_ci
	ENGINE = InnoDB
	AUTO_INCREMENT = 1;

CREATE TABLE price (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	product_page_id INT(11) UNSIGNED NOT NULL,
	datetime TIMESTAMP,
	price INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	INDEX price_pp_fk_idx (product_page_id),
	CONSTRAINT price_pp_fk FOREIGN KEY (product_page_id) REFERENCES product_page (id)
		ON DELETE CASCADE
		ON UPDATE RESTRICT
)
	COMMENT ='Цена товара в какой-то момент.'
	COLLATE = utf8_general_ci
	ENGINE = InnoDB
	AUTO_INCREMENT = 1;

CREATE TABLE last_price (
	product_page_id INT UNSIGNED NOT NULL COMMENT 'Ссылка на страницу товара.',
	price_id INT UNSIGNED NOT NULL COMMENT 'Ссылка на последнюю запись цены.',
	prev_price_id INT UNSIGNED NULL COMMENT 'Ссылка на предыдущую запись цены для этой страницы.',
	PRIMARY KEY (product_page_id, price_id),
	CONSTRAINT product_page_fk FOREIGN KEY (product_page_id) REFERENCES product_page (id)
		ON DELETE CASCADE
		ON UPDATE RESTRICT,
	CONSTRAINT price_id_fk FOREIGN KEY (price_id) REFERENCES price (id)
		ON DELETE CASCADE
		ON UPDATE RESTRICT,
	CONSTRAINT prev_price_id_fk FOREIGN KEY (prev_price_id) REFERENCES price (id)
		ON DELETE CASCADE
		ON UPDATE RESTRICT
)
	COMMENT ='Последняя цена на товар.'
	COLLATE = utf8_general_ci
	ENGINE = InnoDB
	AUTO_INCREMENT = 1;
