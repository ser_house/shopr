<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.12.2017
 * Time: 09:28
 */

namespace App\Cli;


use App\Common\Polling\Polling;

class TestPolling implements ICommand {

	/**
	 * @inheritDoc
	 */
	public function run() {
		$polling = new Polling();
		$generator = new Generator();

		for ($i = 0; $i < 5; $i++) {
			$data = $generator->generateRandomStringRu(15, true);
			$polling->send($data);
			sleep(2);
		}
	}
}
