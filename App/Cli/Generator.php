<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.12.2017
 * Time: 18:55
 */

namespace App\Cli;

use App\Common\Database\Db;
use App\Common\Database\DbInstance;
use App\Common\Formatter\Formatter;
use App\Sections\Product\Price;
use App\Sections\Product\PriceModel;
use App\Sections\Product\Product;
use App\Sections\Product\ProductModel;
use App\Sections\Product\ProductPage;
use App\Sections\Product\ProductPageModel;
use App\Sections\Site\Site;
use App\Sections\Site\SiteModel;

class Generator implements ICommand {
	public const MIN_STR_LEN = 15;
	public const MAX_STR_LEN = 30;

	private const MIN_SITES_COUNT = 5;
	private const MAX_SITES_COUNT = 12;

	private const MIN_PRODUCTS_COUNT = 5;
	private const MAX_PRODUCTS_COUNT = 20;

	private const MIN_PRICES_COUNT = 10;
	private const MAX_PRICES_COUNT = 30;

	private const MIN_PRICE = 50000;
	private const MAX_PRICE = 500000;

	private const MONTH_IN_SECONDS = 86400 * 30;

	private const STR_MODE_LOWER_CHARS = 0b0001;
	private const STR_MODE_UPPER_CHARS = 0b0010;
	private const STR_MODE_DIGITS = 0b0100;
	private const STR_MODE_ALL = self::STR_MODE_LOWER_CHARS | self::STR_MODE_UPPER_CHARS | self::STR_MODE_DIGITS;

	/**
	 * @inheritDoc
	 */
	public function run() {
		$db = DbInstance::getInstance();
		$db->beginTransaction();

		try {
			// Создать сайты, сохранить полученные id
			$sites_count = random_int(self::MIN_SITES_COUNT, self::MAX_SITES_COUNT);
			$site_ids = $this->runCreateSites($sites_count);
			$sites_count = count($site_ids);
			$this->print("Создано сайтов: $sites_count");

			$productPageModel = new ProductPageModel();

			// Создать товары, сохранить полученные id
			// Создать по 2-3 страницы для каждого товара, каждой странице присвоить случайный сайт из созданных
			// Пройтись по всем страницам и создать для каждой по несколько цен.
			$productModel = new ProductModel();
			$products_count = random_int(self::MIN_PRODUCTS_COUNT, self::MAX_PRODUCTS_COUNT);
			for ($i = 0; $i < $products_count; $i++) {
				$product = $this->generateProduct();
				$product->id = 0;
				$productModel->save($product, []);
				$this->print("Создан товар '$product->title'");

				$pages = $this->runCreateProductPages($product->id, $site_ids);
				$productPageModel->addPages($product->id, $pages);
				$pages_count = count($pages);
				$this->print("Создано страниц: $pages_count");

				$page_ids = $productPageModel->getProductPageIds($product->id);
				$this->runCreatePrices($page_ids);
			}

			$db->commit();
			$this->print("Создано товаров: $products_count");
		}
		catch (\Exception $e) {
			$db->rollBack();
			$this->print("Исключение: " . $e->getMessage());
		}
	}

	private function runCreateSites(int $count): array {
		$siteModel = new SiteModel();
		$site_ids = [];
		for ($i = 0; $i < $count; $i++) {
			$site = $this->generateSite();
			$site->id = 0;
			$siteModel->save($site);
			$site_ids[] = $site->id;
		}

		return $site_ids;
	}

	/**
	 * @param int $product_id
	 * @param array $site_ids
	 *
	 * @return ProductPage[]
	 * @throws \Exception
	 */
	private function runCreateProductPages(int $product_id, array $site_ids): array {
		// Для каждого товара на конкретном сайте может быть только одна страница.
		// Следовательно, для текущего товара страниц не может быть больше, чем сайтов.
		$max_pages_count = count($site_ids);
		$min_pages_count = floor($max_pages_count / 2);
		$count = random_int($min_pages_count, $max_pages_count);

		// Возьмем случайный набор сайтов
		$site_ids = array_rand(array_flip($site_ids), $count);
		/** @var ProductPage[] $pages */
		$pages = [];
		// и для каждого создадим одну страницу.
		foreach($site_ids as $site_id) {
			$page = $this->generateProductPage($product_id, $site_id);
			$page->id = 0;
			$pages[] = $page;
		}

		return $pages;
	}

	private function runCreatePrices(array $page_ids) {
		$total_prices_count = 0;
		$priceModel = new PriceModel();
		// Здесь соберем все цены для каждой из страниц, чтобы потом выбрать и сохранить последние.
		$prices_by_page_ids = [];
		foreach ($page_ids as $page_id) {
			$prices_count = random_int(self::MIN_PRICES_COUNT, self::MAX_PRICES_COUNT);
			for ($pri = 0; $pri < $prices_count; $pri++) {
				$price = $this->generatePagePrice($page_id);
				$priceModel->save($price);
				$prices_by_page_ids[$page_id][] = $price;
			}

			$total_prices_count += $prices_count;
		}

		// Отсортируем нагенерированные цены по возрастанию времени, для каждой страницы.
		foreach($prices_by_page_ids as $page_id => $prices) {
			usort($prices, function(Price $price1, Price $price2) {
				$timestamp1 = strtotime($price1->datetime);
				$timestamp2 = strtotime($price2->datetime);
				if ($timestamp1 == $timestamp2) {
					return 0;
				}
				return ($timestamp1 < $timestamp2) ? -1 : 1;
			});
			// Последняя цена в отсортированном массиве и есть последняя по времени, сохраняем.
			$lastPrice = array_pop($prices);
			$priceModel->addLast($lastPrice);
		}

		$this->print("Добавлено цен $total_prices_count");
	}

	private function printTestModes() {
		print 'STR_MODE_LOWER_CHARS' . PHP_EOL;
		for ($i = 0; $i < 10; $i++) {
			$str = $this->generateRandomString(10, self::STR_MODE_LOWER_CHARS);
			print $str . PHP_EOL;
		}

		print PHP_EOL;

		print 'STR_MODE_UPPER_CHARS' . PHP_EOL;
		for ($i = 0; $i < 10; $i++) {
			$str = $this->generateRandomString(10, self::STR_MODE_UPPER_CHARS);
			print $str . PHP_EOL;
		}

		print PHP_EOL;

		print 'STR_MODE_DIGITS' . PHP_EOL;
		for ($i = 0; $i < 10; $i++) {
			$str = $this->generateRandomString(10, self::STR_MODE_DIGITS);
			print $str . PHP_EOL;
		}

		print PHP_EOL;

		print 'STR_MODE_ALL' . PHP_EOL;
		for ($i = 0; $i < 10; $i++) {
			$str = $this->generateRandomString(10, self::STR_MODE_ALL);
			print $str . PHP_EOL;
		}
	}

	private function printTestProducts() {
		for ($i = 0; $i < 10; $i++) {
			$product = $this->generateProduct();
			$product_str = $this->getProductConsoleFormatted($product);
			print $product_str . PHP_EOL;
		}
	}

	private function printTestSites() {
		for ($i = 0; $i < 10; $i++) {
			$site = $this->generateSite();
			$site_str = $this->getSiteConsoleFormatted($site);
			print $site_str . PHP_EOL;
		}
	}

	private function printTestProductPages() {
		for ($i = 0; $i < 10; $i++) {
			$product_page = $this->generateProductPage();
			$page_str = $this->getProductPageConsoleFormatted($product_page);
			print $page_str . PHP_EOL;
		}
	}

	private function printTestPrices() {
		for ($i = 0; $i < 10; $i++) {
			$price = $this->generatePagePrice();
			$price_str = $this->getPriceConsoleFormatted($price);
			print $price_str . PHP_EOL;
		}
	}

	private function print(string $data) {
		print $data . PHP_EOL;
	}

	public function getProductConsoleFormatted(Product $product): string {
		$created_formatted = Formatter::formatDateFromTimestamp($product->created);
		$tracked_formatted = Formatter::formatTrackedValue($product->tracked, 'отслеживать', 'не отслеживать');
		return "$created_formatted '$product->title' $tracked_formatted";
	}

	public function getSiteConsoleFormatted(Site $site): string {
		return "[$site->id] '$site->host' '$site->title' $site->parser";
	}

	public function getProductPageConsoleFormatted(ProductPage $product_page): string {
		$str = "{$product_page->product_id} {$product_page->site_id}" . PHP_EOL;
		$str .= $product_page->url;

		return $str;
	}

	public function getPriceConsoleFormatted(\App\Sections\Product\Price $price): string {
		$date_formatted = Formatter::formatDateFromString($price->datetime);
		$price_formatted = Formatter::formatMoney($price->price);
		return "{$price->id} {$price->product_page_id} $date_formatted $price_formatted";
	}

	public function generateProduct(): Product {
		$data = [
			'id' => random_int(1000, 9999),
			'created' => self::generateRandomTimestamp(),
			'title' => $this->generateRandomStringRu(random_int(self::MIN_STR_LEN, self::MAX_STR_LEN), true),
			'tracked' => true,
		];

		return new Product($data);
	}

	public function generateSite(): Site {
		$host = $this->generateRandomString(random_int(5, 10), self::STR_MODE_LOWER_CHARS, '-');
		$host = trim($host, '-');
		$data = [
			'id' => random_int(1000, 9999),
			'host' => "www.$host.ru",
			'title' => '',
			'parser' => $this->generateRandomString(random_int(8, 12), self::STR_MODE_UPPER_CHARS | self::STR_MODE_LOWER_CHARS),
		];

		if (0 != random_int(1, 5) % 2) {
			$data['title'] = $this->generateRandomStringRu(9, true);
		}

		return new Site($data);
	}

	public function generateProductPage(int $product_id = 0, int $site_id = 0): ProductPage {
		if (empty($product_id)) {
			$product_id = random_int(1000, 9999);
		}

		if (empty($site_id)) {
			$site_id = random_int(1000, 9999);
		}

		$url = $this->generateRandomString(random_int(50, 200), self::STR_MODE_LOWER_CHARS, '/-');
		$pattern = [
			'/^[\W]+|[\W]+$/' => '',
			'/\/{2,}/' => '/',
			'/(\/-)|(-\/)/' => '-',
			'/-{2,}/' => '-',
		];
		$url = preg_replace(array_keys($pattern), array_values($pattern), $url);

		$data = [
			'id' => random_int(1000, 9999),
			'product_id' => $product_id,
			'site_id' => $site_id,
			'url' => 'https://' . $url,
			'tracked' => true,
		];

		return new ProductPage($data);
	}

	public function generatePagePrice(int $page_id = 0): \App\Sections\Product\Price {
		if (empty($page_id)) {
			$page_id = random_int(1000, 9999);
		}

		$data = [
			'id' => random_int(1000, 9999),
			'product_page_id' => $page_id,
			'datetime' => date('Y-m-d H:i:s', $this->generateRandomTimestamp()),
			'price' => $this->generatePriceAmount(),
		];

		return new \App\Sections\Product\Price($data);
	}

	public function generatePriceAmount(): int {
		return random_int(self::MIN_PRICE, self::MAX_PRICE) * 100;
	}

	public function generateRandomTimestamp(): int {
		$max_timestamp = time();
		$min_timestamp = $max_timestamp - self::MONTH_IN_SECONDS;

		return random_int($min_timestamp, $max_timestamp);
	}

	/**
	 * Простенькая генерация случайной строки
	 *
	 * @param int $length
	 *
	 * @param int $mode
	 * @param string $additional_characters
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function generateRandomString($length = 10, int $mode = self::STR_MODE_ALL, string $additional_characters = ''): string {

		$characters = '';

		if (0 == $mode || $mode > self::STR_MODE_ALL) {
			$mode = self::STR_MODE_ALL;
		}

		if ($mode & self::STR_MODE_DIGITS) {
			$characters .= '0123456789';
		}
		if ($mode & self::STR_MODE_LOWER_CHARS) {
			$characters .= 'abcdefghijklmnopqrstuvwxyz';
		}
		if ($mode & self::STR_MODE_UPPER_CHARS) {
			$characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}

		if (!empty($additional_characters)) {
			$characters .= $additional_characters;
		}

		$characters_length = strlen($characters);
		$random_str = '';
		for ($i = 0; $i < $length; $i++) {
			$random_str .= $characters[random_int(0, $characters_length - 1)];
		}

		return $random_str;
	}

	/**
	 * Простенькая генерация более-менее читаемой кириллической строки.
	 *
	 * @param int $length
	 *
	 * @param bool $white_spaces
	 *
	 * @param bool $case_title
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function generateRandomStringRu($length = 6, bool $white_spaces = false, bool $case_title = false): string {
		$string = '';
		// Закоментированные символы делают результат менее приглядным.
		$vowels = ['a', 'е',
//			'ё',
			'и', 'о', 'у', 'ы', 'э', 'ю', 'я'];
		$max_vowel_index = count($vowels) - 1;
		$consonants = [
			'б', 'в', 'г', 'д', 'ж', 'з',
//			'й',
			'к', 'л', 'м', 'н', 'п', 'р', 'с',
			'т',
//			'ф',
			'х', 'ц', 'ч',
//			'ш', 'щ',
		];
		$max_consonant_index = count($consonants) - 1;

		$max = $length / 2;
		for ($i = 1; $i <= $max; $i++) {
			$string .= $consonants[random_int(0, $max_consonant_index)];
			$string .= $vowels[random_int(0, $max_vowel_index)];
			if ($white_spaces) {
				$random_len = random_int(2, 4);
				if (0 == ($i % $random_len)) {
					$string .= ' ';
				}
			}
		}

		if ($case_title) {
			$string = mb_convert_case($string, MB_CASE_TITLE);
		}

		return trim($string);
	}
}
