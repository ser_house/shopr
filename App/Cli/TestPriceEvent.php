<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 11:18
 */

namespace App\Cli;

use App\Common\Event\Event;
use App\Common\Polling\Polling;

class TestPriceEvent implements ICommand {
	/**
	 * @inheritDoc
	 */
	public function run() {
		$polling = new Polling();

		$polling->send(Event::PRICE_CHECKED, 'main');
	}
}
