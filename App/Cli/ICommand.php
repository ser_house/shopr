<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.12.2017
 * Time: 18:56
 */

namespace App\Cli;


interface ICommand {

	/**
	 *
	 * @throws \Exception
	 */
	public function run();
}
