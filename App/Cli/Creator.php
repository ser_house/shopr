<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.11.2017
 * Time: 11:09
 */

namespace App\Cli;

use App\Common\Database\DbInstance;


/**
 * Class Creator
 *
 * @package App\Cli
 */
class Creator implements ICommand {

	/**
	 * Создает первоначальную конфигурацию приложения.
	 *
	 * php App/Cli/cli.php Creator run
	 *
	 * @throws \Exception
	 */
	public function run() {
		$schema_file = APP_DIR . '/Cli/schema.sql';

		if (!@file_exists($schema_file)) {
			throw new \Exception("Error: File <$schema_file> doesn't exists.");
		}

		$schema_sql = file_get_contents($schema_file);
		if (!$schema_sql) {
			throw new \Exception("Error: Can't get content from <$schema_file>.");
		}

		$db = DbInstance::getInstance();

		$sqls = explode(';' . PHP_EOL, $schema_sql);
		foreach($sqls as $sql) {
			$sql = trim($sql);
			if (empty($sql)) {
				continue;
			}
			print $sql . PHP_EOL . PHP_EOL;
			$stmt = $db->prepare($sql);
			$result = $stmt->execute();

			if (false === $result) {
				$err = $db->errorInfo();
				// Если драйвер выдал сообщение об ошибке, то его и выдадим.
				if (isset($err[2])) {
					$msg = $err[2];
				}
				else {
					$msg = "Error: Can't execute schema.sql.";
				}
				throw new \Exception($msg);
			}
		}
	}
}
