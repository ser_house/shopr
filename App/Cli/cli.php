<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.11.2017
 * Time: 11:19
 */

/**
 * Вызов из корня проекта (/var/www/project):
 * php App/Cli/cli.php Creator run
 * где Creator - название класса из Cli,
 * run - необязательное (если не указано, то будет вызыван ::run()) название метода этого класса.
 * Можно передавать и дополнительные аргументы после метода, через пробелы.
 */

include_once dirname(__DIR__) . '/bootstrap.inc';

if (!defined('HOST') || empty(HOST)) {
	$config = new \App\Common\Config\Config();
	$url = $config->get('app')['url'];
	define('HOST', $url);
}

$endl = PHP_EOL;

// position [0] is the script's file name
array_shift($argv);

$short_class_name = array_shift($argv);
$method = array_shift($argv);

if (empty($method)) {
	$method = 'run';
}

$class_file = "$short_class_name.php";

if (!@include_once($class_file)) {
	print "File <$class_file> doesn't exists.$endl";
	exit;
}

$class_name = "App\\Cli\\$short_class_name";

if (!class_exists($class_name)) {
	print "Class <$class_name> doesn't exists.$endl";
	exit;
}

$obj = new $class_name();

if (!method_exists($obj, $method)) {
	print "Method <$class_name::$method> doesn't exists.$endl";
	exit;
}

try {
	print "Executing...$endl";
	$obj->{$method}($argv);
	print "Ok.$endl";
}
catch (Exception $e) {
	print $e->getMessage() . $endl;
}
