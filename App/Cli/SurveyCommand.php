<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.11.2017
 * Time: 15:44
 */

namespace App\Cli;

use App\Common\App;
use App\Common\Common;
use App\Common\LoaderHtml\LoaderHtml;
use App\Common\Logger\Logger;
use App\Sections\Product\PriceModel;
use App\Sections\Product\ProductModel;
use App\Sections\Product\ProductPageModel;
use App\Sections\Site\SiteModel;

// php App/Cli/cli.php SurveyCommand


/**
 * Class Price
 *
 * @package App\Cli
 */
class SurveyCommand implements ICommand {

	/**
	 * @throws \Exception
	 */
	public function run() {
		$siteModel = new SiteModel();
		$priceModel = new PriceModel();

		$logger = new Logger('survey', true);

		$conf_parsers = App::getInstance()->getConfigValue('parsers');

		$productPageModel = new ProductPageModel();

		try {
			$page_ids = $productPageModel->getPageIdsToParse();
			if (empty($page_ids)) {
				return;
			}

			$pages = $productPageModel->getByIds($page_ids);
			$site_ids = Common::getFieldValuesList($pages, 'site_id');
			$sites = $siteModel->loadByIds($site_ids);

			foreach ($pages as $page_id => $page) {
				$host = $sites[$page->site_id]->host;
				if (!isset($conf_parsers[$host])) {
					$logger->write("Нет парсера для $host");
					continue;
				}

				$loader = new LoaderHtml($page->url);

				$fullParserClass = $conf_parsers[$host];
				$class = new \ReflectionClass($fullParserClass);
				$parserName = $class->getShortName();
				/** @var \App\Common\Parser\IParser $parser */
				$parser = new $fullParserClass($loader);

				$amount = $parser->getPrice() * 100;

				$logger->write($amount, "amount from $parserName");

				$last_prices = $priceModel->getLastByPageIds($page_ids);

				if (!empty($amount)) {
					$last_amount = $last_prices[$page_id]->price ?? 0;

					if ($amount != $last_amount) {
						// @todo: здесь возникает событие изменения цены.
						// @todo: здесь же надо сохранять вместе с новой предыдущую (текущую) цену.
						$price = new \App\Sections\Product\Price([
							'product_page_id' => $page_id,
							'price' => $amount,
						]);
						$priceModel->save($price);

						$last_price_id = $last_prices[$page_id]->id ?? 0;
						if (!empty($last_price_id)) {
							$priceModel->updateLast($last_price_id, $price->id);
							$logger->write('Последняя цена обновлена');
						}
						else {
							$priceModel->addLast($price);
							$logger->write('Добавлена новая последняя цена');
						}
					}
					else {
						$logger->write('Полученная цена равна последней сохраненной.');
					}
				}
			}
		}
		catch (\Exception $e) {
			$logger->write($e->getMessage(), 'Exception');
			throw $e;
		}
	}
}
