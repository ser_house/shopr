<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 15:03
 */

namespace App\Sections\Site\Controller;

use App\Common\Request;

/**
 * Class Factory
 *
 * @package App\Sections\Site\Controller
 */
class Factory {

	/**
	 * @param string $action
	 * @param Request $request
	 *
	 * @return \App\Common\Controller|null
	 */
	static public function getController(string $action, Request $request): ?\App\Common\Controller {
		if (empty($action)) {
			$action = 'index';
		}

		switch ($action) {
			case 'index':
				return new Index($request);

			case 'add':
				return new Add($request);

			case 'view':
				return new View($request);

			case 'edit':
				return new Edit($request);

			case 'delete':
				return new Delete($request);

			default:
				return null;
		}
	}
}
