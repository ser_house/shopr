<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:30
 */

namespace App\Sections\Site\Controller;

use \App\Common\Request;
use App\Common\Route;
use App\Sections\Site\Form\SiteFormPage;

/**
 * Class Edit
 *
 * @package App\Sections\Site\Controller
 */
class Edit extends Base {
	/** @var int */
	private $site_id = 0;

	/**
	 * Edit constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		$this->site_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @throws \Exception
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$app = \App\Common\App::getInstance();

		$site = $this->siteModel->loadById($this->site_id);

		if ($this->request->isPost()) {
			$data = $this->request->getPostData();
			$site->setData($data);
			$site->id = $this->site_id;

			$this->siteModel->save($site);
			$title = $site->title;

			$app->setMessage("Сайт <strong><em>$title</em></strong> сохранен.", 'status');
			Route::redirect($this->getUrl($site->id, 'view'));
		}

		return new SiteFormPage($this, $site, 'Редактирование сайта');
	}
}
