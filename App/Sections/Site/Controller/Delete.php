<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Site\Controller;

use \App\Common\Request;
use App\Common\Route;

/**
 * Class Delete
 *
 * @package App\Sections\Site\Controller
 */
class Delete extends Base {
	/** @var int */
	private $site_id = 0;

	/**
	 * Delete constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		$this->site_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke(): void {
		$redirect_uri = 'site';

		$app = \App\Common\App::getInstance();

		try {
			$site = $this->siteModel->loadById($this->site_id);
			$title = $site->title ?: $site->host;
			$this->siteModel->delete($this->site_id);
			$app->setMessage("Сайт <strong><em>$title</em></strong> удалён.", 'status');
		}
		catch (\Exception $e) {
			$app->setMessage($e->getMessage(), 'error');
		}
		finally {
			Route::redirect($redirect_uri);
		}
	}
}
