<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:39
 */

namespace App\Sections\Site\Controller;



use App\Sections\Site\SiteModel;
use App\Common\Request;


/**
 * Class Base
 *
 * @package App\Sections\Site\Controller
 */
class Base extends \App\Common\Controller {
	/**
	 * @var string
	 */
	protected $placeholder = '%site';

	/** @var null|SiteModel */
	protected $siteModel = null;

	/**
	 * Base constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		$this->siteModel = new SiteModel();

		parent::__construct($request);
	}

	/**
	 * @inheritDoc
	 */
	public function getUriItems(): array {
		return [
			'site' => [
				'title' => 'Сайты',
			],
			'add' => [
				'title' => 'Новый сайт',
			],
			$this->placeholder => [
				'title' => 'getTitle',
			],
			'edit' => [
				'title' => 'Редактирование сайта',
			],
			'delete' => [
				'title' => 'Удаление сайта',
			],
		];
	}

	/**
	 * Возвращает название сайта по его id. Полезно для, например, хлебных крошек.
	 *
	 * @param int $site_id
	 *
	 * @return string
	 */
	public function getTitle(int $site_id): string {
		try {
			$site = $this->siteModel->loadById($site_id);

			return $site->title ?: $site->host;
		}
		catch (\Exception $e) {
			\App\Common\App::getInstance()->setMessage($e->getMessage(), 'error');

			return '';
		}
	}

	/**
	 * Возвращает адрес определенной страницы сайта для указанного $op.
	 *
	 * @param int $id
	 * @param string $op
	 *
	 * @return string
	 */
	public function getUrl(int $id, string $op = 'view'): string {
		switch ($op) {
			case 'view':
				return "/site/$id";

			case 'edit':
				return "/site/$id/edit";

			case 'delete':
				return "/site/$id/delete";

			default:
				return "/site/$id";
		}
	}
}
