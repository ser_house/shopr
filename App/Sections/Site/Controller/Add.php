<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Site\Controller;

use App\Common\Route;
use App\Sections\Site\Form\SiteFormPage;

/**
 * Class Add
 *
 * @package App\Sections\Site\Controller
 */
class Add extends Base {

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$site = $this->siteModel->create();

		$app = \App\Common\App::getInstance();

		if (!empty($_POST)) {
			try {
				$data = $_POST;

				$site->setData($data);

				$this->siteModel->save($site);
				$title = $site->title;
				$app->setMessage("Сайт <strong><em>$title</em></strong> сохранен.", 'status');

				Route::redirect($this->getUrl($site->id, 'view'));
			}
			catch (\Exception $e) {
				$app->setMessage('Не удалось сохранить сайт: ' . $e->getMessage(), 'error');
			}
		}

		return new SiteFormPage($this, $site, 'Добавление сайта');
	}
}
