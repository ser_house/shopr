<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Site\Controller;


/**
 * Class Index
 *
 * @package App\Sections\Site\Controller
 */
class Index extends Base {

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$sites = $this->siteModel->getAll();

		$view = new \App\Sections\Site\View\Index($this, $sites);

		return $view;
	}
}
