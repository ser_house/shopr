<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Site\Controller;


/**
 * Class View
 *
 * @package App\Sections\Site\Controller
 */
class View extends Base {
	/** @var int */
	private $site_id = 0;

	/**
	 * View constructor.
	 *
	 * @param \App\Common\Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(\App\Common\Request $request) {
		parent::__construct($request);

		$this->site_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @throws \Exception
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$site = $this->siteModel->loadById($this->site_id);

		$view = new \App\Sections\Site\View\SiteView($this, $site);

		return $view;
	}
}
