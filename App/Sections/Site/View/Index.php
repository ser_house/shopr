<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.10.2017
 * Time: 18:45
 */

namespace App\Sections\Site\View;

use App\Common\Controller;
use App\Common\Html\Component;
use App\Common\Html\Link;
use App\Common\Html\Menu;
use App\Common\Html\NoData;
use App\Common\Html\Table;
use App\Common\View;

use App\Sections\Site\Site;


/**
 * Class Index
 *
 * @package App\Sections\Site\View
 */
class Index extends View {
	/**
	 * Index constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param array $sites
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, array $sites = []) {
		parent::__construct($controller);

		$page_title = 'Сайты';

		$page = $this->html->getPage();
		$page->addClassCss('sites');

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$local_actions = [
			[
				'href' => 'site/add',
				'title' => 'Добавить сайт',
			],
		];

		$attr = [
			'class' => [
				'local-actions',
				'inline-actions',
			],
		];

		$localActions = new Menu($local_actions, $attr);
		$page->addContent($localActions, 'local_actions');

		if (!empty($sites)) {
			$rows = [];
			foreach ($sites as $site) {
				$rows[] = $this->getTableRow($site);
			}

			$view = new Table(['id', 'Хост', 'Название', 'Действия'], $rows);
		}
		else {
			$view = new NoData('Нет сайтов');
		}

		$page->addContent($view, 'sites');
	}

	/**
	 * @param Site $site
	 *
	 * @return Component
	 */
	public function getTableRow(Site $site): Component {
		$row = new Component();

		$row->addContent($site->id, 'id');
		$row->addContent($site->host, 'host');

		$row->addContent(new Link($site->title, $this->controller->getUrl($site->id)), 'title');

		$actions_attr = [
			'class' => [
				'inline-actions',
			],
		];
		$actions = new Component($actions_attr, 'div');

		$edit = new Link('изменить', $this->controller->getUrl($site->id, 'edit'));
		$actions->addContent($edit, 'edit');

		$delete = new Link('удалить', $this->controller->getUrl($site->id, 'delete'));
		$actions->addContent($delete, 'delete');

		$row->addContent($actions, 'actions');

		return $row;
	}
}
