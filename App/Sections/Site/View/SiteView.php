<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 04:29
 */

namespace App\Sections\Site\View;

use App\Common\Controller;
use App\Common\Html\Component;
use App\Common\Html\InlineLabel;
use App\Common\Html\Link;
use App\Common\View;
use App\Sections\Site\Site;


/**
 * Class SiteView
 *
 * @package App\Sections\Site\View
 */
class SiteView extends View {

	/**
	 * Site constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param \App\Sections\Site\Site $site
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, Site $site) {
		parent::__construct($controller);

		$url = $site->host;
		$page_title = "Сайт $url";

		if (!empty($site->title)) {
			$page_title .= " ($site->title)";
		}

		$page = $this->html->getPage();
		$page->addClassCss('site');

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$actions_wrapper_attr = [
			'class' => [
				'local-actions',
				'inline-actions',
			],
		];
		$actions_wrapper = new Component($actions_wrapper_attr, 'div');

		$actions = [
			'edit' => 'изменить',
			'delete' => 'удалить',
		];
		foreach ($actions as $op => $label) {
			$actions_wrapper->addContent(new Link($label, $controller->getUrl($site->id, $op)), $op);
		}

		$page->addContent($actions_wrapper, 'local_actions');

		$urlComponent = new InlineLabel('Сайт', $site->host);
		$page->addContent($urlComponent, 'host');

		$titleComponent = new InlineLabel('Название', $site->title ?: 'не указано');
		$page->addContent($titleComponent, 'title');
	}
}
