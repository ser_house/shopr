<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 16:03
 */

namespace App\Sections\Site\Form;


use App\Common\Controller;
use App\Common\View;
use App\Sections\Site\Site;

/**
 * Class SiteFormPage
 *
 * @package App\Sections\Site\Form
 */
class SiteFormPage extends View {
	/** @var null|Site */
	private $site = null;

	/**
	 * ProductFormPage constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param null|Site $site
	 * @param string $page_title
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, Site $site, string $page_title) {
		parent::__construct($controller);

		$this->site = $site;

		$page = $this->html->getPage();

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$siteForm = new Form($this->site);
		$page->setContent($siteForm);

		$this->html->addCss('css/form.css');
	}
}
