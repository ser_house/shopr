<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2017
 * Time: 09:24
 */

namespace App\Sections\Site\Form;

use App\Common\Html\Component;
use App\Common\Html\Button;
use App\Sections\Site\Site;

/**
 * Class Form
 *
 * @package App\Sections\Site\Form
 */
class Form extends \App\Common\Html\Form {
	/** @var Site */
	private $site = null;

	/**
	 * Form constructor.
	 *
	 * @param Site $site
	 */
	public function __construct(Site $site) {
		$attributes = [
			'id' => 'site-form',
			'class' => [
				'site-form',
			],
		];
		parent::__construct($attributes);

		$this->site = $site;

		$this->build();
	}

	private function build() {
		$site_id = $this->site->id;

		$is_new = empty($site_id);

		$this->addHiddenInput('id', $site_id);

		if ($is_new) {
			$host_attr = [
				'placeholder' => 'Адрес сайта',
			];
			$this->addTextField('host', 'Сайт', $this->site->host, $host_attr);
		}
		else {
			$this->addContent(new Component([], 'span', $this->site->host), 'host');
		}

		$titleField = $this->addTextField('title', 'Название', $this->site->title);
		$desc = 'Название сайта используется для его более удобной идентификации.';
		$titleField->addContent(new Component(['class' => ['description']], 'div', $desc));

		$actions = $this->addActionsBlock();
		$actions->addContent(new Button('Сохранить', [], 'submit'), 'savePages');
	}
}
