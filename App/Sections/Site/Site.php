<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2017
 * Time: 09:25
 */

namespace App\Sections\Site;

use App\Common\Entity\Entity;


/**
 * Class Site
 *
 * @package App\Sections\Site
 */
class Site extends Entity {
	/**
	 * @var string
	 */
	public $host = '';
	/**
	 * @var string
	 */
	public $title = '';
	/**
	 * @var string
	 */
	public $parser = '';
}
