<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.11.2017
 * Time: 09:25
 */

namespace App\Sections\Site;

use App\Common\Common;
use App\Common\Database\DbInstance;
use App\Common\Cache;

/**
 * Class SiteModel
 *
 * @package App\Sections\Site
 */
class SiteModel {
	/**
	 * @param array $values
	 *
	 * @return Site
	 */
	public function create(array $values = []): Site {
		return new Site($values);
	}

	/**
	 * @param string $url
	 *
	 * @return Site
	 */
	public function createFromUrl(string $url): Site {
		$values = [
			'host' => Common::getHost($url),
		];

		return new Site($values);
	}

	/**
	 * @param Site $site
	 *
	 * @throws \Exception
	 */
	public function save(Site $site) {
		$db = DbInstance::getInstance();

		$id = trim($site->id);
		$title = trim($site->title);
		$host = trim($site->host);

		if (empty($host)) {
			throw new \InvalidArgumentException("Не указан ключ 'host': " . print_r($site, true));
		}

		$is_new = empty($id);

		$db->beginTransaction();

		try {
			if ($is_new) {
				$sql = 'INSERT INTO site (host, title) VALUES (:host, :title)';
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':host', $host, \PDO::PARAM_STR);
				$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
				$stmt->execute();
				$id = $db->lastInsertId();
				$site->id = $id;
			}
			else {
				$sql = 'UPDATE site SET host = :host, title = :title WHERE id = :id';
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':host', $host, \PDO::PARAM_STR);
				$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
				$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
				$stmt->execute();
			}

			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * @param int $site_id
	 *
	 * @throws \Exception
	 */
	public function delete(int $site_id) {
		$db = DbInstance::getInstance();

		$db->beginTransaction();

		try {
			$sql = "DELETE FROM site WHERE id = :id";
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':id', $site_id, \PDO::PARAM_INT);
			$success = $stmt->execute();

			if (!$success) {
				throw new \Exception("Не удалось удалить сайт с id ($site_id).");
			}
			$count = $stmt->rowCount();
			if (empty($count)) {
				throw new \Exception("Сайт с id ($site_id) не существует.");
			}
			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * @param int $site_id
	 *
	 * @return Site
	 * @throws \Exception
	 */
	public function loadById(int $site_id): Site {
		$site = &Cache::static (__CLASS__ . ':' . __METHOD__ . $site_id);

		if (!isset($data)) {
			$db = DbInstance::getInstance();

			$stmt = $db->prepare('SELECT * FROM site WHERE id = :id');
			$stmt->bindParam(':id', $site_id, \PDO::PARAM_INT);
			$stmt->execute();

			$data = $stmt->fetch(\PDO::FETCH_ASSOC);
			if (empty($data)) {
				throw new \Exception("Не удалось загрузить сайт [$site_id]");
			}

			$site = $this->create($data);
		}

		return $site;
	}

	/**
	 * Возвращает индексированный и отсортированный по id массив сайтов.
	 *
	 * @return Site[]
	 */
	public function getAll(): array {
		$db = DbInstance::getInstance();
		$stmt = $db->prepare('SELECT * FROM site ORDER BY id');
		$stmt->execute();

		$sites = $stmt->fetchAll(\PDO::FETCH_CLASS, Site::class);
		return Common::mapToIndexed($sites);
	}

	/**
	 * Возвращает индексированный и отсортированный по id массив сайтов.
	 *
	 * @param array $site_ids
	 *
	 * @return Site[]
	 */
	public function loadByIds(array $site_ids): array {
		$db = DbInstance::getInstance();

		$params = $db->prepareIN($site_ids);
		$params_str = implode(',', array_keys($params));

		$sql = "SELECT * FROM site WHERE id IN ($params_str) ORDER BY id";
		$stmt = $db->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam($key, $value, \PDO::PARAM_INT);
		}

		$stmt->execute();

		$sites = $stmt->fetchAll(\PDO::FETCH_CLASS, Site::class);
		return Common::mapToIndexed($sites);
	}
}
