<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.12.2017
 * Time: 09:31
 */

namespace App\Sections\Admin\View;


use App\Cli\Generator;
use App\Common\Controller;
use App\Common\Html\Button;
use App\Common\Html\Component;
use App\Common\Html\Form;
use App\Common\View;


/**
 * Class TestPolling.
 *
 * Страница с формой для визуального тестирования пушей.
 * Пуши для неё отправляются:
 * 1. из консоли командой php App/Cli/cli.php TestPolling.
 * 2. с самой формы кнопкой (открываем страницу в двух браузерах
 *    и тыкаем в одном из них в кнопку - текст из поля ввода появится в обоих браузерах).
 *
 * @package App\Sections\Admin\View
 */
class TestPolling extends View {

	public function __construct(Controller $controller) {
		parent::__construct($controller);

		$this->html->addCss('css/form.css');
		$this->html->addJs('js/pushstream.js');
		$this->html->addJs('js/jquery-3.2.1.min.js');
		$this->html->addJs('admin/js/test_long_polling.js');

		$uri_items = $controller->getUriItems();
		$page_title = $uri_items['test-polling']['title'];
		$page = $this->html->getPage();

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$generator = new Generator();
		$msg = $generator->generateRandomStringRu();

		$form = new Form();

		$form->addTextField('msg', 'Сообщение', $msg, ['id' => 'poll-msg', 'required' => true]);

		$actions = $form->addActionsBlock();
		$actions->addContent(new Button('Send', ['id' => 'send']));

		$result = new Component(['id' => 'result'], 'div');
		$form->addContent($result);

		$page->addContent($form);
	}
}
