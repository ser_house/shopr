<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.12.2017
 * Time: 10:57
 */

namespace App\Sections\Admin\View;

use App\Cli\Generator;
use App\Common\Controller;
use App\Common\Formatter\Formatter;
use App\Common\Html\Button;
use App\Common\Html\Component;
use App\Common\Html\Form;
use App\Common\Html\Table;
use App\Common\View;


class TestRender extends View {

	/**
	 * TestRender constructor.
	 *
	 * @param \App\Common\Controller $controller
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller) {
		parent::__construct($controller);

		$this->html->addCss('css/form.css');

		$page_title = 'Проверка рендера';

		$page = $this->html->getPage();

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$generator = new Generator();

		$form = $this->buildForm($generator);
		$page->addContent($form, 'form');

		$table = $this->buildTable($generator);
		$page->addContent($table, 'table');
	}

	private function buildForm(Generator $generator): Form {
		$form = new Form();

		$form->addTextField('title', 'Название', 'название', ['required' => true]);

		$textarea_attr = [
			'rows' => 5,
			'cols' => 100,
			'required' => true,
		];
		$text = $generator->generateRandomStringRu(300, true);
		$form->addTextareaField('body', 'Текст', $text, $textarea_attr);

		$form->addCheckboxField('tracked', 'отслеживать');

		$options = [
			'' => '- None -',
			'a' => 'One',
			'b' => 'Two',
			'c' => 'Three',
			'd' => 'Four',
		];
		$form->addSelectField('select', 'Поле выбора', $options, 'a');

		$actions = $form->addActionsBlock();

		$save = new Button('Сохранить', [], 'submit');
		$actions->addContent($save, 'savePages');

		return $form;
	}

	private function buildTable(Generator $generator): Table {
		$table_items = [];
		for ($i = 1; $i < 5; $i++) {
			$table_items[] = [
				'n/n' => $i,
				'string' => $generator->generateRandomStringRu(10, true),
				'price' => $generator->generatePriceAmount(),
			];
		}

		$price_cell_attr = [
			'class' => ['price'],
		];

		$rows = [];
		foreach ($table_items as $i => $table_item) {
			$row = new Component();

			if (0 == $i % 2) {
				$row->setAttribute('style', 'background-color:#eee');
			}

			$row->addContent($table_item['n/n']);
			$row->addContent($table_item['string']);

			$priceCell = new Component($price_cell_attr, '', Formatter::formatMoney($table_item['price']));
			$row->addContent($priceCell);

			$rows[] = $row;
		}

		return new Table(['n/n', 'Строка', 'Сумма'], $rows);
	}
}
