<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.12.2017
 * Time: 13:32
 */

namespace App\Sections\Admin\View;


use App\Common\Controller;
use App\Common\Html\Menu;
use App\Common\View;

class Index extends View {

	/**
	 * Index constructor.
	 *
	 * @param \App\Common\Controller $controller
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller) {
		parent::__construct($controller);

		$uri_items = $this->controller->getUriItems();

		$page_title = $uri_items['admin']['title'];

		$page = $this->html->getPage();

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$local_actions = [
			[
				'href' => '/admin/test-render',
				'title' => $uri_items['test-render']['title'],
			],
			[
				'href' => '/admin/test-polling',
				'title' => $uri_items['test-polling']['title'],
			],
		];
		$attr = [
			'class' => [
				'local-actions',
				'inline-actions',
			],
		];

		$localActions = new Menu($local_actions, $attr);
		$page->addContent($localActions, 'local_actions');
	}
}
