<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.12.2017
 * Time: 13:19
 */

namespace App\Sections\Admin\Controller;


class Factory {
	/**
	 * @param string $action
	 * @param \App\Common\Request $request
	 *
	 * @return \App\Common\Controller
	 */
	static public function getController(string $action, \App\Common\Request $request): \App\Common\Controller {
		if (empty($action)) {
			$action = 'index';
		}

		switch ($action) {
			case 'test-render':
				return new TestRender($request);

			case 'test-polling':
				return new TestPolling($request);

			default:
				return new Index($request);
		}
	}
}
