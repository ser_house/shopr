<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.12.2017
 * Time: 12:08
 */

namespace App\Sections\Admin\Controller;


use App\Common\Controller;

class Base extends Controller {
	/**
	 * @inheritDoc
	 */
	public function getUriItems(): array {
		return [
			'admin' => [
				'title' => 'Админ',
			],
			'test-render' => [
				'title' => 'Тест рендера',
			],
			'test-polling' => [
				'title' => 'Тест Long-Polling'
			],
		];
	}
}
