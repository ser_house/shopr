<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.12.2017
 * Time: 09:17
 */

namespace App\Sections\Admin\Controller;



class TestPolling extends Base {
	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$view = new \App\Sections\Admin\View\TestPolling($this);

		return $view;
	}
}
