<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.01.2018
 * Time: 15:28
 */

namespace App\Sections\Product;

use App\Common\Database\DbInstance;


class PriceModel {
	/**
	 * Создаёт и возвращает объект цены.
	 *
	 * @param array $values
	 *
	 * @return Price
	 */
	public function create(array $values = []): Price {
		// @todo: надо обдумать возможность создавать объекты без использования модели.
		if (is_string($values['datetime'])) {
			$values['datetime'] = date('Y-m-d H:i:s', strtotime($values['datetime']));
		}
		else {
			$values['datetime'] = date('Y-m-d H:i:s', $values['datetime']);
		}
		return new Price($values);
	}

	/**
	 * Возвращает последние цены для тех из указанных страниц,
	 * для которых такие цены есть.
	 *
	 * @param array $page_ids
	 *
	 * @return Price[] индексированный идентификаторами страниц из входного набора
	 * (для которых есть последние цены) набор цен:
	 * <pre>
	 * $result = [
	 *   $page_id_1 => Price(),
	 *   $page_id_2 => Price(),
	 * ];
	 * </pre>
	 * или пустой массив.
	 */
	public function getLastByPageIds(array $page_ids): array {
		$db = DbInstance::getInstance();

		$params = $db->prepareIN($page_ids);
		$params_str = implode(',', array_keys($params));

		// Выберем последние сохраненные цены для каждой страницы.
		$sql = "SELECT
			price.*
		FROM price
			INNER JOIN last_price AS last ON price.id = last.price_id
		WHERE last.product_page_id IN($params_str)
		ORDER BY price.datetime DESC";

		$stmt = $db->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam($key, $value, \PDO::PARAM_INT);
		}
		$stmt->execute();

		$prices = $stmt->fetchAll(\PDO::FETCH_CLASS, Price::class);
		if (empty($prices)) {
			return [];
		}

		// Каждой странице свой объект последней цены.
		$prices_indexed_by_page = [];

		foreach ($prices as $price) {
			$page_id = $price->product_page_id;
			$prices_indexed_by_page[$page_id] = $price;
		}

		return $prices_indexed_by_page;
	}

	/**
	 * Возвращает цены для каждого переданного id страницы.
	 *
	 * @param array $page_ids
	 *
	 * @return array ассоциативный массив, индексированный идентификаторами страниц
	 * 	(только тех, для которых найдена хотя бы одна цена).
	 * 	Каждой странице задается набор цен, отсортированных по времени по убыванию.
	 * 	Набор цен - тоже ассоциативный массив, индексированный идентификаторами цен.
	 * 	Результат выглядит так:
	 *  <pre>
	 *  $result = [
	 *    $page_id_1 => [
	 *      $price_id_1 => Price(),
	 *      $price_id_2 => Price(),
	 *    ],
	 *    $page_id_2 => [
	 *      $price_id_3 => Price(),
	 *      $price_id_4 => Price(),
	 *    ],
	 *  ];
	 * 	</pre>
	 */
	public function getByPageIds(array $page_ids): array {
		$db = DbInstance::getInstance();

		// Выберем цены для каждой страницы.
		$params = $db->prepareIN($page_ids);
		$params_str = implode(',', array_keys($params));
		$sql = "SELECT * FROM price WHERE product_page_id IN($params_str) ORDER BY datetime DESC";

		$stmt = $db->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam($key, $value, \PDO::PARAM_INT);
		}
		$stmt->execute();

		$prices = $stmt->fetchAll(\PDO::FETCH_CLASS, Price::class);
		if (empty($prices)) {
			return [];
		}

		$prices_indexed_by_page = [];

		foreach ($prices as $price) {
			$page_id = $price->product_page_id;
			$prices_indexed_by_page[$page_id][$price->id] = $price;
		}

		return $prices_indexed_by_page;
	}

	/**
	 * Безусловно сохраняет цену для определенной страницы товара.
	 *
	 * @param Price $price
	 */
	public function save(Price $price): void {
		$db = DbInstance::getInstance();

		// Нужна возможность записать определенное вызывающим кодом время,
		// поэтому набор полей динамический.
		$fields = [
			'product_page_id' => ':page_id',
			'price' => ':price',
		];
		if (!empty($price->datetime)) {
			$fields['datetime'] = ':datetime';
		}
		$fields_str = implode(',', array_keys($fields));
		$values_str = implode(',', $fields);
		$sql = "INSERT INTO price ($fields_str) VALUES ($values_str)";
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':page_id', $price->product_page_id, \PDO::PARAM_INT);
		if (!empty($price->datetime)) {
			$stmt->bindParam(':datetime', $price->datetime, \PDO::PARAM_STR);
		}
		$stmt->bindParam(':price', $price->price, \PDO::PARAM_INT);
		$stmt->execute();

		$price->id = $db->lastInsertId();
	}


	/**
	 * Отмечает новую сохраненную цену товара как последнюю актуальную.
	 * Это новая цена по определению (в противном случае мы будем не добавлять, а обновлять),
	 * то у неё нет предыдущей последней.
	 *
	 * @param Price $price
	 *
	 * @return int
	 */
	public function addLast(Price $price): int {
		$db = DbInstance::getInstance();

		$sql = 'INSERT INTO last_price (product_page_id, price_id) VALUES (:page_id, :price_id)';
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':page_id', $price->product_page_id, \PDO::PARAM_INT);
		$stmt->bindParam(':price_id', $price->id, \PDO::PARAM_INT);
		$stmt->execute();

		return $db->lastInsertId();
	}

	/**
	 * Обновляет запись о последней сохраненной цене так, что та хранит id другой цены.
	 *
	 * @param int $last_price_id
	 * @param int $price_id
	 */
	public function updateLast(int $last_price_id, int $price_id) {
		$db = DbInstance::getInstance();

		$sql = 'UPDATE last_price SET prev_price_id = price_id, price_id = :price_id WHERE id = :last_price_id';
		$stmt = $db->prepare($sql);
		$stmt->bindParam(':price_id', $price_id, \PDO::PARAM_INT);
		$stmt->bindParam(':last_price_id', $last_price_id, \PDO::PARAM_INT);
		$stmt->execute();
	}
}
