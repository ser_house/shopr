<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:39
 */

namespace App\Sections\Product\Controller;


use App\Sections\Product\ProductModel;
use App\Sections\Product\ProductPageModel;
use \App\Common\Request;

/**
 * Class Base
 *
 * @package App\Sections\Product\Controller
 */
class Base extends \App\Common\Controller {
	/**
	 * @var string
	 */
	protected $placeholder = '%product';

	/** @var ProductModel */
	protected $productModel = null;
	/** @var ProductPageModel */
	protected $productPageModel = null;

	/**
	 * Base constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		$this->productModel = new ProductModel();
		$this->productPageModel = new ProductPageModel();

		parent::__construct($request);
	}

	/**
	 * @inheritDoc
	 */
	public function getUriItems(): array {
		return [
			'product' => [
				'title' => 'Товары',
			],
			'add' => [
				'title' => 'Новый товар',
			],
			$this->placeholder => [
				'title' => 'getTitle',
			],
			'edit' => [
				'title' => 'Редактирование товара',
			],
			'delete' => [
				'title' => 'Удаление товара',
			],
			'prices' => [
				'title' => 'Цены',
			],
		];
	}

	/**
	 * Возвращает название товара по его id. Полезно для, например, хлебных крошек.
	 *
	 * @param int $product_id
	 *
	 * @return string
	 */
	public function getTitle(int $product_id): string {
		try {
			$product = $this->productModel->getProductById($product_id);

			return $product->title;
		}
		catch (\Exception $e) {
			\App\Common\App::getInstance()->setMessage($e->getMessage(), 'error');

			return '';
		}
	}

	/**
	 * Возвращает адрес определенной страницы товара для указанного $op.
	 *
	 * @param int $id
	 * @param string $op
	 *
	 * @return string
	 */
	public function getUrl(int $id, string $op = 'view'): string {
		switch ($op) {
			case 'view':
				return "/product/$id";

			default:
				return "/product/$id/$op";
		}
	}
}
