<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.11.2017
 * Time: 23:05
 */

namespace App\Sections\Product\Controller;

use App\Common\Common;
use App\Sections\Product\PriceModel;
use App\Sections\Site\SiteModel;


/**
 * Class Prices
 *
 * @package App\Sections\Product\Controller
 */
class Prices extends Base {
	/** @var int */
	private $product_id = 0;

	/**
	 * View constructor.
	 *
	 * @param \App\Common\Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(\App\Common\Request $request) {
		parent::__construct($request);

		$this->product_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$pages = $this->productPageModel->getByProductId($this->product_id);
		$priceModel = new PriceModel();
		$page_ids = Common::getFieldValuesList($pages, 'id');
		$prices = $priceModel->getByPageIds($page_ids);

		$siteModel = new SiteModel();
		$site_ids = Common::getFieldValuesList($pages, 'site_id');
		$sites = $siteModel->loadByIds($site_ids);

		$view = new \App\Sections\Product\View\Prices($this, $pages, $prices, $sites);

		return $view;
	}
}
