<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:30
 */

namespace App\Sections\Product\Controller;

use App\Common\Request;
use App\Common\Route;
use App\Sections\Product\Form\ProductFormPage;

/**
 * Class Edit
 *
 * @package App\Sections\Product\Controller
 */
class Edit extends Base {
	/** @var int */
	private $product_id = 0;

	/**
	 * Edit constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		$this->product_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @throws \Exception
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$app = \App\Common\App::getInstance();

		$product = $this->productModel->getProductById($this->product_id);

		try {
			if ($this->request->isPost()) {
				$data = $this->request->getPostData();
				$data['id'] = $this->product_id;
				$data['created'] = $product->created;

				$data = $this->productModel->validateInputEdit($data);
				$pages = $data['pages'];
				unset($data['pages']);
				$product->setData($data);

				$new_hosts = $this->productModel->save($product, $pages);

				$app->setMessage("Товар <strong><em>$product->title</em></strong> сохранен.", 'status');

				if (!empty($new_hosts)) {
					\App\Common\App::getInstance()->setMessage("Появились новые <a href='/site'>сайты</a> без названий.", 'warning');
				}
				Route::redirect($this->getUrl($product->id, 'view'));
			}
			else {
				$product->pages = $this->productPageModel->getByProductId($this->product_id);
			}
		}
		catch (\Exception | \InvalidArgumentException $e) {
			$app->setMessage($e->getMessage(), 'error');
		}

		$view = new ProductFormPage($this, $product, 'Редактирование товара');

		return $view;
	}
}
