<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Product\Controller;

/**
 * Class Index
 *
 * @package App\Sections\Product\Controller
 */
class Index extends Base {

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$products = $this->productModel->getProducts();

		$view = new \App\Sections\Product\View\Index($this, $products);

		return $view;
	}
}
