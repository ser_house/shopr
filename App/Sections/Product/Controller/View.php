<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Product\Controller;


/**
 * Class View
 *
 * @package App\Sections\Product\Controller
 */
class View extends Base {
	/** @var int */
	private $product_id = 0;

	/**
	 * View constructor.
	 *
	 * @param \App\Common\Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(\App\Common\Request $request) {
		parent::__construct($request);

		$this->product_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @throws \Exception
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$product = $this->productModel->getProductById($this->product_id);

		$product->pages = $this->productPageModel->getByProductId($this->product_id);

		$view = new \App\Sections\Product\View\ProductView($this, $this->productModel, $product);

		return $view;
	}
}
