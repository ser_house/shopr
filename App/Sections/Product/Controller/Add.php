<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Product\Controller;

use App\Sections\Product\Form\ProductFormPage;

/**
 * Class Add
 *
 * @package App\Sections\Product\Controller
 */
class Add extends Base {

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$product = $this->productModel->create();

		$app = \App\Common\App::getInstance();

		if ($this->request->isPost()) {
			try {
				$data = $this->request->getPostData();
				$data = $this->productModel->validateInputNew($data);
				$pages = $data['pages'];
				unset($data['pages']);
				$product->setData($data);

				$this->productModel->save($product, $pages);
				$title = $product->title;
				$app->setMessage("Товар <strong><em>$title</em></strong> сохранен.", 'status');

				\App\Common\Route::redirect($this->getUrl($product->id, 'view'));
			}
			catch (\Exception | \InvalidArgumentException $e) {
				$app->setMessage('Не удалось сохранить товар:' . PHP_EOL . $e->getMessage(), 'error');
			}
		}

		$view = new ProductFormPage($this, $product, 'Добавление товара');

		return $view;
	}
}
