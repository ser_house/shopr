<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 14:31
 */

namespace App\Sections\Product\Controller;

use \App\Common\Request;
use App\Common\Route;

/**
 * Class Delete
 *
 * @package App\Sections\Product\Controller
 */
class Delete extends Base {
	/** @var int */
	private $product_id = 0;

	/**
	 * Delete constructor.
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function __construct(Request $request) {
		parent::__construct($request);

		$this->product_id = $this->request->getEntityId();
	}

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke(): void {
		// @todo: ну, вообще-то здесь надо через страницу подтверждения проходить.
		$redirect_uri = 'product';

		$app = \App\Common\App::getInstance();

		try {
			$product = $this->productModel->getProductById($this->product_id);
			$title = $product->title;
			$this->productModel->delete($this->product_id);
			$app->setMessage("Товар <strong><em>$title</em></strong> удалён.", 'status');
		}
		catch (\Exception $e) {
			$app->setMessage($e->getMessage(), 'error');
		}
		finally {
			Route::redirect($redirect_uri);
		}
	}
}
