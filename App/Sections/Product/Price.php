<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 12:55
 */

namespace App\Sections\Product;


use App\Common\Entity\Entity;

class Price extends Entity {
	/** @var int */
	public $product_page_id = 0;

	/** @var string */
	public $datetime = '';

	/** @var int */
	public $price = 0;
}
