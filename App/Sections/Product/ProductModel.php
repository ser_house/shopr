<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.10.2017
 * Time: 18:15
 */

namespace App\Sections\Product;

use App\Common\Common;
use App\Common\Database\DbInstance;
use App\Common\Cache;
use App\Sections\Site\SiteModel;


/**
 * Class ProductModel
 *
 * @package App\Sections\Product
 */
class ProductModel {

	/**
	 * Создаёт и возвращает объект товара.
	 *
	 * @param array $values
	 *
	 * @return Product
	 */
	public function create(array $values = []): Product {
		return new Product($values);
	}

	/**
	 * @param Product $product
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function save(Product $product, array $pages): array {
		$db = DbInstance::getInstance();

		$id = trim($product->id);
		$title = trim($product->title);
		$is_tracked = $product->tracked;

		$is_new = empty($id);

		$db->beginTransaction();

		try {
			if ($is_new) {
				$created = $product->created;
				if (empty($created)) {
					$created = time();
				}

				$sql = 'INSERT INTO product (title, created, tracked) VALUES (:title, :created, :tracked)';
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
				$stmt->bindParam(':created', $created, \PDO::PARAM_INT);
				$stmt->bindParam(':tracked', $is_tracked, \PDO::PARAM_INT);
				$stmt->execute();
				$id = $db->lastInsertId();
				$product->id = $id;
			}
			else {
				$sql = 'UPDATE product 
					SET title = :title, tracked = :tracked 
					WHERE id = :id';
				$stmt = $db->prepare($sql);
				$stmt->bindParam(':title', $title, \PDO::PARAM_STR);
				$stmt->bindParam(':tracked', $is_tracked, \PDO::PARAM_INT);
				$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
				$stmt->execute();
			}

			$new_hosts = [];
			if (!empty($pages)) {
				$productPageModel = new ProductPageModel();
				$new_hosts = $productPageModel->savePages($id, $pages);
			}

			$db->commit();

			return $new_hosts;
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * @param int $product_id
	 *
	 * @throws \Exception
	 */
	public function delete(int $product_id) {
		$db = DbInstance::getInstance();

		$db->beginTransaction();

		try {
			$sql = "DELETE FROM product WHERE id = :id";
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':id', $product_id, \PDO::PARAM_INT);
			$success = $stmt->execute();

			if (!$success) {
				throw new \Exception("Не удалось удалить товар с id ($product_id).");
			}
			$count = $stmt->rowCount();
			if (empty($count)) {
				throw new \Exception("Товар с id ($product_id) не существует.");
			}
			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * @param int $product_id
	 *
	 * @return Product
	 * @throws \Exception
	 */
	public function getProductById(int $product_id): Product {
		$product = &Cache::static (__CLASS__ . ':' . __METHOD__ . $product_id);

		if (!isset($product)) {
			$products = $this->getProductsByIds([$product_id]);
			if (empty($products)) {
				throw new \Exception("Товар не найден (id: $product_id).");
			}
			$product = reset($products);
		}

		return $product;
	}

	/**
	 * @param int[] $product_ids
	 *
	 * @return Product[]
	 */
	public function getProductsByIds(array $product_ids): array {
		$cache_hash = implode(',', array_map(function($value) {
			return trim($value);
		}, $product_ids));

		$products = &Cache::static(__CLASS__ . __METHOD__ . $cache_hash);

		if (!isset($products)) {
			$db = DbInstance::getInstance();
			$params = $db->prepareIN($product_ids);

			$params_str = implode(',', array_keys($params));

			$stmt = $db->prepare("SELECT * FROM product WHERE id IN ($params_str) ORDER BY id");
			foreach ($params as $key => &$value) {
				$stmt->bindParam($key, $value, \PDO::PARAM_INT);
			}
			$stmt->execute();

			$items = $stmt->fetchAll(\PDO::FETCH_CLASS, Product::class);
			$products = Common::mapToIndexed($items);
		}

		return $products;
	}

	/**
	 * @return Product[]
	 */
	public function getProducts(): array {
		$db = DbInstance::getInstance();
		$stmt = $db->prepare('SELECT * FROM product ORDER BY id');
		$stmt->execute();

		$products = $stmt->fetchAll(\PDO::FETCH_CLASS, Product::class);
		return Common::mapToIndexed($products);
	}

	public function getUpdatedProductIds(int $last_hours): array {
		$db = DbInstance::getInstance();
		// @todo: условие по дате цены как-нибудь вынести вне, в интерфейс. Чтобы удобно было менять.
		// Хотя можно пока так оставить, а там может и вовсе фильтрация будет.

		// id товаров, цена которых изменилась.
		$sql = "SELECT DISTINCT
			pp.product_id
		FROM price
			INNER JOIN product_page AS pp ON pp.id = price.product_page_id
			INNER JOIN product ON product.id = pp.product_id
		WHERE product.tracked = 1
			AND pp.tracked = 1
			AND price.datetime > DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL :hours HOUR)";

		$stmt = $db->prepare($sql);
		$stmt->bindParam(':hours', $last_hours, \PDO::PARAM_INT);
		$stmt->execute();

		$product_ids = $stmt->fetchAll(\PDO::FETCH_COLUMN);

		return $product_ids ?: [];
	}

	/**
	 * Загружает данные по товарам, цена которых недавно обновилась хотя бы на одной отслеживаемой странице.
	 *
	 * @param int $last_hours
	 *
	 * @return Product[] с добавленным свойством last_prices - список цен,
	 *  каждая цена описывается ассоциативным массивом с ключами:
	 * <pre>
	 *  - 'host' (string)
	 *  - 'product_id' (int)
	 *  - 'datetime' (string)
	 *  - 'price' (int)
	 *  - 'prev_price' (int|null) предыдущая цена на этой странице
	 * </pre>
	 */
	public function getUpdatedProductsData(int $last_hours = 24): array {
		$product_ids = $this->getUpdatedProductIds($last_hours);
		if (empty($product_ids)) {
			return [];
		}

		$products = $this->getProductsByIds($product_ids);

		$db = DbInstance::getInstance();

		$params = $db->prepareIN($product_ids);
		$params_str = implode(',', array_keys($params));

		// И данные таких товаров для вывода.
		// @todo: предыдущая цена берется пока тупо подзапросом.
		$sql = "SELECT
			pp.product_id,
			last.product_page_id,
			site.id AS site_id,
			site.host AS site_host,
			site.title AS site_title,
			price.id AS price_id,
			price.datetime AS price_datetime,
			price.price,
			prev_price.price AS prev_price
		FROM last_price AS last
			INNER JOIN price ON price.id = last.price_id
			LEFT JOIN price AS prev_price ON prev_price.id = last.prev_price_id
			INNER JOIN product_page AS pp ON pp.id = last.product_page_id
			INNER JOIN site ON site.id = pp.site_id
		WHERE pp.product_id IN ($params_str)
		ORDER BY pp.product_id ASC, price.datetime DESC";

		$stmt = $db->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam($key, $value, \PDO::PARAM_INT);
		}

		$stmt->execute();

		$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		$updated = [];

		$sites_cached = [];
		$siteModel = new SiteModel();
		$priceModel = new PriceModel();

		foreach ($items as $item) {
			$product_id = $item['product_id'];
			$page_id = $item['product_page_id'];

			if (!isset($updated[$product_id])) {
				$updated[$product_id] = [
					'product' => $products[$product_id],
					'pages' => [],
				];
			}

			$site_id = $item['site_id'];
			if (!isset($sites_cached[$site_id])) {
				$site_values = [
					'id' => $site_id,
					'host' => $item['site_host'],
					'title' => $item['site_title'],
				];
				$sites_cached[$site_id] = $siteModel->create($site_values);
			}

			if (!isset($updated[$product_id]['pages'][$page_id])) {
				$price_data = [
					'id' => $item['price_id'],
					'product_page_id' => $page_id,
					'datetime' => $item['price_datetime'],
					'price' => $item['price'],
				];
				$price = $priceModel->create($price_data);
				$price->site = $sites_cached[$site_id];
				$price->prev_price = $item['prev_price'];

				$updated[$product_id]['pages'][$page_id] = $price;
			}
		}

		return $updated;
	}

	public function validateInputNew(array $input): array {
		$errors = [];

		// Для нового товара обязательным является только название.
		if (empty($input['title'])) {
			$errors[] = 'Не указано название товара.';
		}

		// Поле tracked может отсутствовать, это будет false.
		// Но если оно присутствует, то его значение должно быть true (1) или false (0).
		if (isset($input['tracked']) && !filter_var($input['tracked'], FILTER_VALIDATE_BOOLEAN)) {
			$errors[] = 'Неверное свойство отслеживания товара.';
		}

		$input['tracked'] = !empty($input['tracked']);

		if (!empty($errors)) {
			throw new \InvalidArgumentException(implode(PHP_EOL, $errors));
		}

		return $input;
	}

	public function validateInputEdit(array $input): array {
		// Чтобы не выбрасывать ошибки по одной (если их несколько),
		// соберем и выкинем все сразу.
		$errors = [];

		// Для отредактированного товара обязательными являются поля названия, id, created.
		// Поля id и created должны присутствовать и быть целыми числами, отличными от 0.
		if (empty($input['id']) || !filter_var($input['id'], FILTER_VALIDATE_INT)) {
			$errors[] = 'Неверный идентификатор товара.';
		}
		if (empty($input['created']) || !filter_var($input['created'], FILTER_VALIDATE_INT)) {
			$errors[] = 'Неверное время создания товара.';
		}
		if (empty($input['title'])) {
			$errors[] = 'Не указано название товара.';
		}

		// Поле tracked может отсутствовать, это будет false.
		// Но если оно присутствует, то его значение должно быть true (1) или false (0).
		if (isset($input['tracked']) && !filter_var($input['tracked'], FILTER_VALIDATE_BOOLEAN)) {
			$errors[] = 'Неверное свойство отслеживания товара.';
		}

		$input['tracked'] = !empty($input['tracked']);

		if (!empty($errors)) {
			throw new \InvalidArgumentException(implode(PHP_EOL, $errors));
		}

		return $input;
	}
}
