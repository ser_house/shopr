<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 15:06
 */

namespace App\Sections\Product;

use App\Common\Entity\Entity;


/**
 * Class Product
 *
 * @package App\Sections\Product
 */
class Product extends Entity {
	/**
	 * @var string
	 */
	public $title = '';
	/**
	 * @var int
	 */
	public $created = 0;
	/**
	 * @var bool
	 */
	public $tracked = true;

	/** @var ProductPage[] список страниц */
	public $pages = [];
}
