<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.12.2017
 * Time: 11:10
 */

namespace App\Sections\Product;

use App\Common\Common;
use App\Common\Cache;
use App\Common\Database\DbInstance;
use App\Sections\Site\SiteModel;

class ProductPageModel {
	/**
	 * Создаёт и возвращает объект страницы товара.
	 *
	 * @param array $data
	 *
	 * @return \App\Sections\Product\ProductPage
	 */
	public function create(array $data): ProductPage {
		$data['tracked'] = $data['tracked'] ?? false;
		return new ProductPage($data);
	}

	/**
	 * Метод получает список страниц товара, разбирает их на новые, изменяемые и удаляемые,
	 * и приводит базу данных в соответствии с новым набором страниц.
	 *
	 * @param int $product_id
	 * @param array[] $pages страницы ассоциативными массивам
	 *
	 * @return array ассоциативный массив новых сайтов в виде page_url => page_host
	 * @throws \Exception
	 */
	public function savePages(int $product_id, array $pages): array {
		$siteModel = new SiteModel();

		// Возьмем все имеющиеся уже сайты
		$sites = $siteModel->getAll();
		// и сделаем их них список для быстрого поиска.
		$exists_sites_by_hosts = Common::mapToIndexed($sites, 'host');

		// Соберем из новых страниц новые сайты.
		$new_hosts = [];

		foreach ($pages as &$page) {
			if (is_array($page)) {
				$page = $this->create($page);
			}
			$this->checkPageHost($page, $exists_sites_by_hosts, $new_hosts);
		}
		unset($page);

		// Может быть:
		//  добавление новой страницы
		//  удаление текущей произвольной
		//  изменение текущей произвольной

		// Подготовим индексированный по их id массив текущих страниц
		$current_pages = $this->getByProductId($product_id);

		/** @var ProductPage[] $current_pages_by_id */
		$current_pages_by_id = Common::mapToIndexed($current_pages);

		// и индексированный по их id массив новых страниц.
		// Новые страницы - это в том числе и текущие, которые пришли сюда неизмененными.
		// У них есть id (собственно, среди новых только у них он и есть).
		$pages_by_id = Common::mapToIndexed($pages);

		/** @var ProductPage[] $pages_to_change страницы для изменения */
		$pages_to_change = [];

		/** @var ProductPage[] $pages_to_add страницы для добавления */
		$pages_to_add = [];

		/** @var ProductPage[] $pages */
		foreach ($pages as $page) {
			$id = $page->id;

			if (!empty($id)) {
				$current_page = $current_pages_by_id[$id];
				// У страницы могут измениться только адрес и состояние отслеживание
				// (если мы, например, решили заменить одну страницу на другую).
				// Если изменился флаг отслеживания, то это простое изменение.
				// А вот если изменился адрес, то он может вести уже на другой сайт,
				// поэтому у страницы, возможно, потребуется изменить и ссылку на сайт.
				if ($current_page->url != $page->url || $current_page->tracked != $page->tracked) {
					$pages_to_change[] = $page;
				}
			}
			else {
				$pages_to_add[] = $page;
			}
		}

		$current_page_ids = array_keys($current_pages_by_id);
		/** @var array $page_to_remove_ids страницы для удаления */
		$page_to_remove_ids = [];
		foreach ($current_page_ids as $id) {
			if (!isset($pages_by_id[$id])) {
				$page_to_remove_ids[] = $id;
			}
		}

		$db = DbInstance::getInstance();
		$db->beginTransaction();

		try {
			// Если добавляются новые страницы.
			if (!empty($pages_to_add)) {
				$this->setSiteIdToPages($pages_to_add, $new_hosts, $siteModel, $exists_sites_by_hosts);
				$this->addPages($product_id, $pages_to_add);
			}

			// Если в результате изменения страниц(-ы) возникли новые сайты
			if (!empty($pages_to_change)) {
				$this->setSiteIdToPages($pages_to_change, $new_hosts, $siteModel, $exists_sites_by_hosts);
				$this->updatePages($pages_to_change);
			}

			// Если есть страницы на удаление.
			if (!empty($page_to_remove_ids)) {
				$this->deletePages($page_to_remove_ids);
			}

			$db->commit();

			return $new_hosts;
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * Вспомогательный метод.
	 *
	 * Проверяет страницу на то, что у неё неизвестный системе сайт.
	 * Если это так, то сайт добавляется в массив новых.
	 *
	 * @param ProductPage $page страница
	 * @param array $exists_sites_by_hosts заранее подготовленный ассоциативный (по host) массив существующих в системе
	 *   сайтов
	 * @param array $new_hosts ассоциативный (по url) массив новых сайтов, в который будет добавлен сайт страницы (если
	 *   его еще нет в системе)
	 */
	private function checkPageHost(ProductPage &$page, array $exists_sites_by_hosts, array &$new_hosts): void {
		$page->url = Common::trimUrl($page->url);

		// Свяжем со страницей - пригодится в методе setSiteIdToPages().
		$page->host = Common::getHost($page->url);

		if (!isset($exists_sites_by_hosts[$page->host])) {
			$new_hosts[$page->url] = $page->host;
		}
	}

	/**
	 * Вспомогательный метод.
	 *
	 * Для каждой страницы проверяет её сайт и так или иначе
	 * (при необходимости создаёт и сохраняет новый сайт)
	 * устанавливает корректный ключ 'site_id'.
	 *
	 * @param ProductPage[] $pages страницы
	 * @param array $new_hosts заранее подготовленный ассоциативный (по url) массив новых сайтов
	 * @param \App\Sections\Site\SiteModel $siteModel
	 * @param array $exists_sites_by_hosts заранее подготовленный ассоциативный (по host) массив существующих в системе
	 *   сайтов
	 *
	 * @throws \Exception
	 */
	private function setSiteIdToPages(array &$pages, array $new_hosts, SiteModel $siteModel, array $exists_sites_by_hosts): void {
		foreach ($pages as &$page) {
			if (isset($new_hosts[$page->url])) {
				// Если есть новый сайт, то создадим, сохраним и свяжем его со страницей.
				$site = $siteModel->create(['host' => $new_hosts[$page->url]]);
				$siteModel->save($site);
			}
			else {
				$site = $exists_sites_by_hosts[$page->host];
			}
			$page->site_id = $site->id;
		}
		unset($page);
	}

	/**
	 * Добавляет все переданные страницы в базу за раз.
	 *
	 * @param int $product_id
	 * @param ProductPage[] $pages
	 *
	 * @throws \Exception
	 */
	public function addPages(int $product_id, array $pages) {
		$values = [];

		$pages_count = count($pages);
		for ($i = 0; $i < $pages_count; $i++) {
			$values[] = "(:product_id_$i, :site_id_$i, :url_$i, :tracked_$i)";
		}

		$values_str = implode(',', $values);
		$sql = "INSERT INTO product_page (product_id, site_id, url, tracked) VALUES $values_str";

		$db = DbInstance::getInstance();
		$db->beginTransaction();

		try {
			$stmt = $db->prepare($sql);

			for ($i = 0; $i < $pages_count; $i++) {
				$stmt->bindParam(":product_id_$i", $product_id, \PDO::PARAM_INT);
				$stmt->bindParam(":site_id_$i", $pages[$i]->site_id, \PDO::PARAM_INT);
				$stmt->bindParam(":url_$i", $pages[$i]->url, \PDO::PARAM_STR);
				$stmt->bindParam(":tracked_$i", $pages[$i]->tracked, \PDO::PARAM_INT);
			}

			$stmt->execute();

			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * Обновляет страницы в базе.
	 * Если не удалось обновить хотя бы одну страницу,
	 * то метод откатывает изменения в базе и выбрасывает стандартное исключение.
	 *
	 * @param ProductPage[] $pages страницы.
	 *
	 * @throws \Exception
	 */
	public function updatePages(array $pages) {
		$sql = "UPDATE product_page SET site_id = :site_id, url = :url, tracked = :tracked WHERE id = :id";

		$db = DbInstance::getInstance();
		$db->beginTransaction();

		try {
			$stmt = $db->prepare($sql);

			foreach ($pages as $page) {
				$stmt->bindParam(':site_id', $page->site_id, \PDO::PARAM_INT);
				$stmt->bindParam(':url', $page->url, \PDO::PARAM_STR);
				$stmt->bindParam(':tracked', $page->tracked, \PDO::PARAM_INT);
				$stmt->bindParam(':id', $page->id, \PDO::PARAM_INT);
				$stmt->execute();
			}

			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * Удаляет страницы с указанными идентификаторами.
	 * Если не удалось удалить хотя бы одну страницу,
	 * то метод откатывает изменения в базе и выбрасывает стандартное исключение.
	 *
	 * @param int[] $page_ids
	 *
	 * @throws \Exception
	 */
	public function deletePages(array $page_ids) {
		$db = DbInstance::getInstance();

		$params = $db->prepareIN($page_ids);
		$params_str = implode(',', array_keys($params));

		$sql = "DELETE FROM product_page WHERE id IN ($params_str)";

		$db->beginTransaction();

		try {
			$stmt = $db->prepare($sql);
			foreach ($params as $key => &$value) {
				$stmt->bindParam($key, $value, \PDO::PARAM_INT);
			}
			$stmt->execute();

			$db->commit();
		}
		catch (\Exception $e) {
			$db->rollBack();
			throw $e;
		}
	}

	/**
	 * Возвращает объекты страниц товара. Результат статически кэшируется.
	 *
	 * @param int $product_id
	 *
	 * @return ProductPage[] ассоциативный массив страниц, индексированный по их id
	 */
	public function getByProductId(int $product_id): array {
		$product_pages = &Cache::static (__CLASS__ . ':' . __METHOD__ . $product_id);

		if (!isset($product_pages)) {
			$db = DbInstance::getInstance();

			$sql = "SELECT * FROM product_page WHERE product_id = :product_id ORDER BY id";
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':product_id', $product_id, \PDO::PARAM_INT);
			$stmt->execute();

			$pages = $stmt->fetchAll(\PDO::FETCH_CLASS, ProductPage::class);
			$product_pages = Common::mapToIndexed($pages);
		}

		return $product_pages;
	}

	/**
	 * Возвращает объекты страниц для переданных идентификаторов.
	 *
	 * @param array $page_ids
	 *
	 * @return ProductPage[]
	 */
	public function getByIds(array $page_ids): array {
		$db = DbInstance::getInstance();

		$params = $db->prepareIN($page_ids);

		$params_str = implode(',', array_keys($params));

		$sql = "SELECT * FROM product_page WHERE id IN ($params_str)";

		$stmt = $db->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam($key, $value, \PDO::PARAM_INT);
		}
		$stmt->execute();

		$pages = $stmt->fetchAll(\PDO::FETCH_CLASS, ProductPage::class);

		return Common::mapToIndexed($pages);
	}

	/**
	 * Возвращает id страниц товара. Результат статически кэшируется.
	 *
	 * @param int $product_id
	 *
	 * @return array список id страниц.
	 */
	public function getProductPageIds(int $product_id): array {
		$page_ids = &Cache::static (__CLASS__ . ':' . __METHOD__ . $product_id);

		if (!isset($page_ids)) {
			$db = DbInstance::getInstance();

			$stmt = $db->prepare('SELECT id FROM product_page WHERE product_id = :product_id');
			$stmt->bindParam(':product_id', $product_id, \PDO::PARAM_INT);
			$stmt->execute();

			$page_ids = $stmt->fetchAll(\PDO::FETCH_COLUMN);
		}

		return $page_ids;
	}

	/**
	 * Возвращает id страниц для парсинга. Результат статически кэшируется.
	 *
	 * @return array список id страниц.
	 */
	public function getPageIdsToParse(): array {
		$page_ids = &Cache::static (__CLASS__ . ':' . __METHOD__ );

		if (!isset($page_ids)) {
			$db = DbInstance::getInstance();

			$sql = "SELECT 
				pp.id
			FROM product_page AS pp
				INNER JOIN product ON product.id = pp.product_id
			WHERE product.tracked = 1 
				AND pp.tracked = 1";

			$stmt = $db->prepare($sql);
			$stmt->execute();

			$page_ids = $stmt->fetchAll(\PDO::FETCH_COLUMN);
		}

		return $page_ids;
	}
}
