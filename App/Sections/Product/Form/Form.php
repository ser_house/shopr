<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.11.2017
 * Time: 14:00
 */

namespace App\Sections\Product\Form;


use App\Common\Html\Component;
use App\Common\Html\Button;
use App\Common\Html\Checkbox;
use App\Common\Html\InputHidden;
use App\Common\Html\InputText;
use App\Sections\Product\Product;
use App\Sections\Product\ProductPage;
use App\Sections\Product\ProductPageModel;


/**
 * Class Form
 *
 * @package App\Sections\Product\Form
 */
class Form extends \App\Common\Html\Form {
	/** @var Product */
	private $product = null;

	/**
	 * Form constructor.
	 *
	 * @param Product $product
	 */
	public function __construct(Product $product) {
		$attributes = [
			'id' => 'product-form',
			'class' => [
				'product-form',
			],
		];
		parent::__construct($attributes);

		$this->product = $product;

		$this->setTplFile(APP_DIR . '/Sections/Product/templates/form.tpl.php');

		$this->build();
	}

	private function build() {
		$title_attr = [
			'id' => 'product-title',
			'class' => [
				'product-title',
			],
			'required' => true,
		];
		$this->addTextField('title', 'Название', $this->product->title, $title_attr);

		$this->addCheckboxField('tracked', 'отслеживать', $this->product->tracked);

		// Полагаемся на Vue, который создаст сколько нужно полей
		// и заполнит их данными из атрибута формы 'pages-data'.
		$empty_page = new ProductPage();

		if (empty($this->product->pages)) {
			$this->product->pages[] = $empty_page;
		}

		// Это для инициализации vue.
		// Страницы могут прийти как индексированный их id массив, нам же нужен просто списко объектов.
		$this->setAttribute('pages-data', json_encode(array_values($this->product->pages)));
		// А это для связи vue с элементами формы, по атрибутам (чтобы они, собственно, были на странице).
		$this->content['pages'][] = $this->buildPageElement($empty_page);

		// А вот так было бы лучше - первоначально отрендерить на сервере и потом прицепить vue,
		// чтобы он реагировал на изменения. Увы, vue не для того (но есть, конечно, SSR для него).
//		$this->content['pages'] = [];
//		foreach ($this->product->pages as $page) {
//			$this->content['pages'][] = $this->buildPageElement($page);
//		}

		$actions = $this->addActionsBlock('add_page');

		$add_attr = [
			'class' => [
				'add-page',
			],
			'@click' => 'addPage',
		];
		$add = new Button('Добавить', $add_attr);

		$actions->addContent($add, 'add');
	}

	/**
	 * @param ProductPage $page
	 *
	 * @return Component
	 */
	private function buildPageElement(ProductPage $page): Component {
		$wrapper_attr = [
			'v-for' => '(page, index) in pages',
			':key' => 'index',
			'class' => [
				'form-item',
				'container-inline',
				'product-page',
			],
		];
		$wrapper = new Component($wrapper_attr, 'div');

		$id_attr = [
			'class' => ['page-id'],
			'v-bind:name' => 'getInputName(index, "id")',
			'v-model' => 'page.id',
		];
		$id = new InputHidden($page->id, $id_attr);
		$wrapper->addContent($id, 'id');

		$site_id_attr = [
			'class' => ['page-site-id'],
			'v-bind:name' => 'getInputName(index, "site_id")',
			'v-model' => 'page.site_id',
		];
		$site_id = new InputHidden($page->site_id, $site_id_attr);
		$wrapper->addContent($site_id, 'site_id');

		$url_attr = [
			'class' => ['page-url'],
			'placeholder' => 'Укажите полный адрес страницы',
			'v-model' => 'page.url',
			'v-bind:name' => 'getInputName(index, "url")',
			'required' => true,
		];
		$pageUrl = new InputText($page->url, '', $url_attr);

		$wrapper->addContent($pageUrl, 'url');

		$tracked_attr = [
			'class' => ['page-tracked'],
			'v-bind:name' => 'getInputName(index, "tracked")',
			'v-model' => 'page.tracked',
			'checked' => $page->tracked,
		];
		$pageTrack = new Checkbox($page->tracked, 'отслеживать', $tracked_attr);

		$wrapper->addContent($pageTrack, 'tracked');

		$button_attr = [
			'class' => [
				'remove',
			],
			'v-show' => 'index > 0',
			'@click.prevent' => 'removePage(index)',
		];
		$remove = new Button('Удалить', $button_attr);
		$wrapper->addContent($remove);

		return $wrapper;
	}
}
