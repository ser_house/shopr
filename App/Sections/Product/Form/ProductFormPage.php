<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 16:03
 */

namespace App\Sections\Product\Form;


use App\Common\Controller;
use App\Common\View;
use App\Sections\Product\Product;

/**
 * Class ProductFormPage
 *
 * @package App\Sections\Product\Form
 */
class ProductFormPage extends View {
	/** @var Product */
	private $product = null;

	/**
	 * ProductFormPage constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param Product $product
	 * @param string $page_title
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, Product $product, string $page_title) {
		parent::__construct($controller);

		$this->product = $product;

		$page = $this->html->getPage();
		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$productForm = new Form($this->product);
		$page->setContent($productForm);

		$this->html->addCss('css/form.css');

		$this->html->addJs('js/axios/axios.min.js');
		$this->html->addJs('js/vue/vue.min.js');
		$this->html->addJs('product/form/form.js');
	}
}
