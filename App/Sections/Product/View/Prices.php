<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.11.2017
 * Time: 23:15
 */

namespace App\Sections\Product\View;


use App\Common\Controller;
use App\Common\Html\Component;
use App\Common\Html\Link;
use App\Common\Html\NoData;
use App\Common\View;

use App\Sections\Product;


/**
 * Class Prices
 *
 * @package App\Sections\Product\View
 */
class Prices extends View {

	/**
	 * Prices constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param \App\Sections\Product\ProductPage[] $pages
	 * @param array $prices
	 * @param \App\Sections\Site\Site[] $sites
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, array $pages, array $prices, array $sites) {
		parent::__construct($controller);

		$page_title = 'Цены';

		$page = $this->html->getPage();
		$page->addClassCss('product-prices');

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		if (!empty($pages)) {

			$attr = [
				'class' => [
					'page-prices',
				],
			];

			foreach ($pages as $page_id => $pricePage) {

				$pageView = new Component($attr, 'div');

				if (!isset($sites[$pricePage->site_id])) {
					continue;
				}

				$site = $sites[$pricePage->site_id];
				$site_title = $site->title ?: $site->host;
				$site_link = new Link($site_title, $pricePage->url, ['title' => 'Страница товара в магазине.']);

				$caption = new Component(['class' => ['container-inline']], 'div');
				$caption->addContent(new Component([], 'label', 'Сайт'));
				$caption->addContent($site_link);

				$pageView->addContent($caption);

				if (isset($prices[$page_id])) {
					$columns_mode = Product\Html\Prices::COLUMN_DATETIME | Product\Html\Prices::COLUMN_PRICE;
					$table = new Product\Html\Prices($prices[$page_id], $columns_mode);
					$pageView->addContent($table);
				}
				else {
					$pageView->addContent(new NoData('Нет цен.'));
				}

				$page->addContent($pageView);
			}
		}
		else {
			$page->addContent(new NoData('Нет цен.'));
		}
	}
}
