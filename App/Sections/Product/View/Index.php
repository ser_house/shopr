<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.10.2017
 * Time: 18:45
 */

namespace App\Sections\Product\View;


use App\Common\Controller;
use App\Common\Formatter\Formatter;
use App\Common\Html\Component;
use App\Common\Html\Link;
use App\Common\Html\Menu;
use App\Common\Html\Table;
use App\Common\View;
use App\Sections\Product\Product;



/**
 * Class Index
 *
 * @package App\Sections\Product\View
 */
class Index extends View {
	/**
	 * Index constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param array $products
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, array $products = []) {
		parent::__construct($controller);

		$page_title = 'Товары';

		$page = $this->html->getPage();
		$page->addClassCss('products');

		$page->setTitle($page_title);
		$this->html->setTitle($page_title);

		$local_actions = [
			[
				'href' => 'product/add',
				'title' => 'Добавить товар',
			],
		];
		$attr = [
			'class' => [
				'local-actions',
				'inline-actions',
			],
		];

		$localActions = new Menu($local_actions, $attr);
		$page->addContent($localActions, 'local_actions');

		if (!empty($products)) {
			$rows = [];
			foreach ($products as $product) {
				$rows[] = $this->getTableRow($product);
			}

			$view = new Table(['id', 'Создан', 'Название', 'Отсл.', 'Действия'], $rows);
		}
		else {
			$view = new Component([], 'div', 'Нет товаров');
		}

		$page->addContent($view, 'products');
	}

	/**
	 * @param Product $product
	 *
	 * @return \App\Common\Html\Component
	 */
	public function getTableRow(Product $product): Component {
		$row = new Component();

		$row->addContent($product->id, 'id');
		$row->addContent(Formatter::formatDateFromTimestamp($product->created), 'created');
		$row->addContent(new Link($product->title, $this->controller->getUrl($product->id)), 'title');
		$row->addContent(Formatter::formatTrackedHtml($product->tracked), 'tracked');

		$actions_attr = [
			'class' => [
				'inline-actions',
			],
		];
		$actions = new Component($actions_attr, 'div');

		$link = new Link('изменить', $this->controller->getUrl($product->id, 'edit'));
		$actions->addContent($link, 'edit');
		$link = new Link('удалить', $this->controller->getUrl($product->id, 'delete'));
		$actions->addContent($link, 'delete');
		$link = new Link('цены', $this->controller->getUrl($product->id, 'prices'));
		$actions->addContent($link, 'prices');

		$row->addContent($actions, 'actions');

		return $row;
	}

}
