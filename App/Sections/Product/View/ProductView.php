<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.10.2017
 * Time: 04:29
 */

namespace App\Sections\Product\View;

use App\Common\Common;
use App\Common\Controller;
use App\Common\Formatter\Formatter;
use App\Common\Html\Component;
use App\Common\Html\NoData;
use App\Common\Html\Table;
use App\Common\View;
use App\Sections\Product\ProductModel;
use App\Sections\Product\Product;
use App\Common\Html\Link;
use App\Sections\Site\SiteModel;


/**
 * Class ProductView
 *
 * @package App\Sections\Product\View
 */
class ProductView extends View {
	/**
	 * Product constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param ProductModel $model
	 * @param Product $product
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, ProductModel $model, Product $product) {
		parent::__construct($controller);

		$page = $this->html->getPage();
		$page->addClassCss('product');

		$page->setTitle($product->title);
		$this->html->setTitle($product->title);

		$content = new Component();

		$content->setTplFile(APP_DIR . '/Sections/Product/templates/product.tpl.php');

		$actions = [
			'edit' => 'изменить',
			'delete' => 'удалить',
			'prices' => 'цены',
		];
		// product.tpl.php ждёт список, и правильно ждёт.
		$localActions = [];
		foreach ($actions as $op => $label) {
			$uri = $this->controller->getUrl($product->id, $op);
			$localActions[$op] = new Link($label, $uri);
		}
		$content->addContent($localActions, 'local_actions');

		$content->addContent(Formatter::formatDateFromTimestamp($product->created), 'created');
		$content->addContent(Formatter::formatTrackedHtml($product->tracked), 'tracked');


		if (!empty($product->pages)) {
			$site_ids = Common::getFieldValuesList($product->pages, 'site_id');

			$siteModel = new SiteModel();
			$sites = $siteModel->loadByIds($site_ids);

			$rows = [];
			foreach ($product->pages as $productPage) {
				$row = new Component();

				$site = $sites[$productPage->site_id];

				$link_attr = [
					'style' => "background-image: url(https://www.google.com/s2/favicons?domain={$site->host});",
					'class' => [
						'product-page-link',
					],
				];

				$row->addContent(new Link($site->host, $productPage->url, $link_attr), 'host');
				$row->addContent(Formatter::formatTrackedHtml($productPage->tracked, 'да', 'нет'), 'tracked');

				$rows[] = $row;
			}

			$pages = new Table(['Сайт', 'Отсл.'], $rows);
		}
		else {
			$pages = new NoData('Нет страниц.');
		}
		$content->addContent($pages, 'pages');

		$page->addContent($content);
	}

}
