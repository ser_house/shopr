<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 12:57
 */

namespace App\Sections\Product;

use App\Common\Entity\Entity;

class ProductPage extends Entity {
	/** @var int */
	public $product_id = 0;

	/** @var int */
	public $site_id = 0;

	/** @var string */
	public $url = '';

	/**
	 * @var bool
	 */
	public $tracked = true;
}
