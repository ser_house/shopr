<form<?= $attributes ?>>
	<?= $title ?>
	<?= $tracked ?>
	<fieldset class="product-pages">
		<legend>Страницы для парсинга</legend>
		<?= $pages ?>
		<?= $add_page ?>
	</fieldset>
	<div class="form-actions">
		<button type="submit">Сохранить</button>
	</div>
</form>
