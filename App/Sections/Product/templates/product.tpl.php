<div class="product-view">
	<div class="local-actions inline-actions">
		<?= $local_actions ?>
	</div>
	<div class="container-inline product-created">
		<label>Создан</label><?= $created ?>
	</div>
	<div class="container-inline product-tracked">
		<label>Отслеживание включено</label><?= $tracked ?>
	</div>
	<div class="product-pages">
		<label>Страницы с ценами:</label>
		<?= $pages ?>
	</div>
</div>
