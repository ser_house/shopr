<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.12.2017
 * Time: 16:33
 */

namespace App\Sections\Product\Html;


use App\Common\App;
use App\Common\Filter\Filter;
use App\Common\Formatter\Formatter;
use App\Common\Html\Component;
use App\Common\Html\Table;
use App\Common\Logger\Logger;
use App\Sections\Product\Price;

class Prices extends Table {
	public const COLUMN_HOST = 0b0001;
	public const COLUMN_DATETIME = 0b0010;
	public const COLUMN_PRICE = 0b0100;
	public const COLUMN_ALL = self::COLUMN_HOST | self::COLUMN_DATETIME | self::COLUMN_PRICE;

	/**
	 * Prices constructor.
	 *
	 * @param Price[] $prices
	 * @param int $columns_mode
	 */
	public function __construct(array $prices, int $columns_mode = self::COLUMN_ALL) {
		$price_cell_attr = [
			'class' => ['price'],
		];

		$columns = $this->buildColumns($columns_mode, $price_cell_attr);

		$db_datetime_format = App::getInstance()->getConfigValue('formats')['datetime']['db'];
		$rows = [];
		$firstPrice = reset($prices);
		$last_datetime = \DateTime::createFromFormat($db_datetime_format, $firstPrice->datetime);
		$min_price = 0;

		foreach ($prices as &$price) {
			$price_datetime = \DateTime::createFromFormat($db_datetime_format, $price->datetime);
			if ($price_datetime > $last_datetime) {
				$last_datetime = $price_datetime;
			}
			if (empty($min_price)) {
				$min_price = $price->price;
			}
			elseif ($price->price < $min_price) {
				$min_price = $price->price;
			}
		}
		unset($price);

		foreach ($prices as $price) {
			$row = new Component();

			if ($columns_mode & self::COLUMN_HOST) {
				$site = $price->site;
				$label = $site->title ?: $site->host;
				$row->addContent(Filter::sanitize($label));
			}

			if ($columns_mode & self::COLUMN_DATETIME) {
				$row->addContent(Formatter::formatDateFromString($price->datetime));
			}

			if ($columns_mode & self::COLUMN_PRICE) {
				$cell = new Component($price_cell_attr, '', Formatter::formatMoney($price->price));
				$row->addContent($cell);
			}

			$row_attributes = [];

			$title = [];

			if ($price->datetime == $last_datetime->format($db_datetime_format)) {
				$row_attributes['class'][] = 'last-time';
				$title[] = 'Цена обновлена.';
			}

			if ($price->price == $min_price) {
				$row_attributes['class'][] = 'min-price';
				$title[] = 'Минимальная цена.';
			}

			if (isset($price->prev_price)) {
				$title[] = 'Предыдущая: ' . Formatter::formatMoney($price->prev_price);
			}

			if (!empty($title)) {
				$row_attributes['title'] = implode(PHP_EOL, $title);
			}

			if (!empty($price->prev_price)) {
				if ($price->price > $price->prev_price) {
					$row_attributes['class'][] = 'more-price';
				}
				elseif ($price->price < $price->prev_price) {
					$row_attributes['class'][] = 'less-price';
				}
			}

			$row->setAttributes($row_attributes);

			$rows[] = $row;
		}

		parent::__construct($columns, $rows);
	}

	private function buildColumns(int $columns_mode, array $price_cell_attr): array {
		$columns = [];

		if (0 == $columns_mode || $columns_mode > self::COLUMN_ALL) {
			$columns_mode = self::COLUMN_ALL;
		}

		if ($columns_mode & self::COLUMN_HOST) {
			$columns[] = 'Сайт';
		}

		if ($columns_mode & self::COLUMN_DATETIME) {
			$columns[] = 'Время';
		}

		if ($columns_mode & self::COLUMN_PRICE) {
			$columns[] = new Component($price_cell_attr, '', 'Цена');
		}

		return $columns;
	}
}
