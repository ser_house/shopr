<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2017
 * Time: 17:38
 */

namespace App\Sections\Main\View;

use App\Common\Controller;
use App\Common\Html\Block;
use App\Common\Html\Component;
use App\Common\Html\Link;
use App\Common\View;
use App\Sections\Product\Html\Prices;
use App\Sections\Product\Product;


class Index extends View {
	/**
	 * Index constructor.
	 *
	 * @param \App\Common\Controller $controller
	 * @param array $blocks_data
	 *
	 * @throws \Exception
	 */
	public function __construct(Controller $controller, array $blocks_data) {
		parent::__construct($controller);

		$page = $this->html->getPage();
		$page->addClassCss('main');

		if (!empty($blocks_data['price_logs'])) {
			$priceLogsBlock = $this->priceLogsBlock($blocks_data['price_logs'], 'Журнал');
			$page->addContent($priceLogsBlock, 'price_logs');
		}

		if (!empty($blocks_data['updated_products'])) {
			$latestUpdatesBlock = $this->latestUpdatesBlock($blocks_data['updated_products'], 'Последние обновления');
			$page->addContent($latestUpdatesBlock, 'latest_updates');
		}

		if (!empty($blocks_data['all_products'])) {
			$allProductsBlock = $this->allProductsBlock($blocks_data['all_products'], 'Все товары');
			$page->addContent($allProductsBlock, 'all_products');

			$this->html->addJs('js/jquery-3.2.1.min.js');
			$this->html->addJs('js/pushstream.js');
			$this->html->addJs('main/main.js');
		}
	}

	/**
	 * Блок логов последних запросов цен.
	 *
	 * @param string[] $records
	 * @param string $title
	 *
	 * @return \App\Common\Html\Block
	 */
	protected function priceLogsBlock(array $records, string $title): Block {
		$block = new Block($title, '', ['class' => ['log-block']]);

		$view_attr = [
			'class' => [
				'price-logs',
			],
		];
		$view = new Component($view_attr, 'div');
		foreach ($records as $record) {
			$item = new Component([], 'div', $record);
			$view->addContent($item);
		}

		$block->addContent($view);

		return $block;
	}

	/**
	 * Блок свежих обновлений цен.
	 *
	 * @param array $data
	 * @param string $title
	 *
	 * @return \App\Common\Html\Block
	 */
	protected function latestUpdatesBlock(array $data, string $title): Block {
		$block = new Block($title, '', ['class' => ['product-prices-block']]);

		$view_attr = [
			'class' => [
				'product-view',
			],
		];

		foreach ($data as $product_id => $item) {
			$productView = new Component($view_attr, 'div');

			$caption = new Component([], 'label');
			// @todo: не так надо, не тут.
			$uri = "/product/{$item['product']->id}/prices";
			$caption->addContent(new Link($item['product']->title, $uri, ['title' => 'Страница цен товара.']));
			$productView->addContent($caption);

			$table = new Prices($item['pages']);
			$productView->addContent($table);

			$block->addContent($productView);
		}

		return $block;
	}

	/**
	 * Блок всех товаров.
	 *
	 * @param Product[] $products
	 * @param string $title
	 *
	 * @return \App\Common\Html\Block
	 */
	protected function allProductsBlock(array $products, string $title): Block {
		$block = new Block($title, '', ['class' => ['product-prices-block']]);

		$prd_item_attr = [
			'class' => [
				'product-item',
				'collapsible',
				'collapsed',
			],
		];

		$prd_title_attr = [
			'class' => [
				'title-wrapper',
			],
		];
		foreach ($products as $product) {
			$productItem = new Component($prd_item_attr, 'div');

			$productTitle = new Component($prd_title_attr, 'div');

			$link_attr = [
				'class' => [
					'ajax-load',
					'toggle-link',
				],
				'data-product_id' => $product->id,
			];
			$title = new Link($product->title, 'javascript:void(0);', $link_attr);
			$productTitle->addContent($title);

			$productItem->addContent($productTitle);

			$block->addContent($productItem);
		}

		return $block;
	}
}
