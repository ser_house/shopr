<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.12.2017
 * Time: 22:32
 */

namespace App\Sections\Main\Controller;

use App\Common\Html\Component;
use App\Common\View;


/**
 * Class NotFound
 *
 * @package App\Sections\Main\Controller
 */
class NotFound extends \App\Common\Controller {
	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		$title = "Страница не найдена";

		$view = new View($this);
		$html = $view->getHtml();
		$page = $html->getPage();

		$page->setTitle($title);
		$html->setTitle($title);

		$div = new Component([], 'div', '404 Not Found:');
		$page->addContent($div);

		if (!empty($_SESSION['404_source'])) {
			$uri = $_SESSION['404_source'];
			unset($_SESSION['404_source']);
			$uriDiv = new Component([], 'div', urldecode($uri));
			$page->addContent($uriDiv);
		}

		return $view;
	}
}
