<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.11.2017
 * Time: 15:48
 */

namespace App\Sections\Main\Controller;


use App\Common\Common;
use App\Common\Html\NoData;
use App\Common\Logger\Logger;
use App\Sections\Product\PriceModel;
use App\Sections\Product\ProductModel;
use App\Sections\Product\ProductPageModel;
use App\Sections\Site\SiteModel;


/**
 * Class Index
 *
 * @package App\Sections\Main\Controller
 */
class Index extends \App\Common\Controller {

	/**
	 * The __invoke method is called when a script tries to call an object as a function.
	 *
	 * @return mixed
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
	 */
	public function __invoke() {
		if ($this->request->isAjax()) {
			return $this->ajaxProcess();
		}

		$blocks_data = [];

		$productModel = new ProductModel();

		$updated_products = $productModel->getUpdatedProductsData();

		if (!empty($updated_products)) {
			$blocks_data['updated_products'] = $updated_products;
		}

		$all_products = $productModel->getProducts();
		if (!empty($all_products)) {
			$blocks_data['all_products'] = $all_products;
		}

		$logger = new Logger();
		$price_logs = $logger->read('survey');
		if (!empty($price_logs)) {
			$blocks_data['price_logs'] = $price_logs;
		}

		return new \App\Sections\Main\View\Index($this, $blocks_data);
	}

	private function ajaxProcess() {
		$data = $this->request->getPostData();

		$product_id = $data['productId'];

		$productPageModel = new ProductPageModel();
		$pages = $productPageModel->getByProductId($product_id);

		$page_ids = Common::getFieldValuesList($pages, 'id');
		$priceModel = new PriceModel();
		$prices = $priceModel->getLastByPageIds($page_ids);

		if (empty($prices)) {
			return [
				'status' => 'success',
				'html' => new NoData('Нет цен.'),
			];
		}
		else {
			// Каждому объекту цены задаем объект сайта для вывода.
			$siteModel = new SiteModel();
			$site_ids = Common::getFieldValuesList($pages, 'site_id');
			$sites = $siteModel->loadByIds($site_ids);
			foreach($prices as &$price) {
				$pricePage = $pages[$price->product_page_id];
				$price->site = $sites[$pricePage->site_id];
			}
			unset($price);
			return [
				'status' => 'success',
				'html' => new \App\Sections\Product\Html\Prices($prices),
			];
		}
	}
}
