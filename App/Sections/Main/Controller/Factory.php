<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.12.2017
 * Time: 22:30
 */

namespace App\Sections\Main\Controller;


/**
 * Class Factory
 *
 * @package App\Sections\Main\Controller
 */
class Factory {
	/**
	 * @param string $action
	 * @param \App\Common\Request $request
	 *
	 * @return \App\Common\Controller
	 */
	static public function getController(string $action, \App\Common\Request $request): \App\Common\Controller {
		if (empty($action)) {
			$action = 'index';
		}

		switch ($action) {
			case 'index':
				return new Index($request);

			default:
				return new NotFound($request);
		}
	}
}
