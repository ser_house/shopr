<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.10.2017
 * Time: 17:35
 */


return [
	'db' => [
		'host' => 'localhost',
		'dbname' => 'dbname',
		'user' => 'user',
		'pass' => 'pass',
		'charset' => 'utf8',
	],
	'app' => [
		'title' => 'Цены товаров',
		'main_menu' => [
			'/product' => 'Товары',
			'/site' => 'Сайты',
		],
	],
	'parsers' => [
		'www.onlinetrade.ru' => \App\Common\Parser\OnlineTrade::class,
		'www.citilink.ru' => \App\Common\Parser\Citilink::class,
		'www.fotosklad.ru' => \App\Common\Parser\Fotosklad::class,
		'www.dns-shop.ru' => \App\Common\Parser\DNS::class,
		'www.mvideo.ru' => \App\Common\Parser\Mvideo::class,
		'www.eldorado.ru' => \App\Common\Parser\Eldorado::class,
	],
	'formats' => [
		'datetime' => [
			'db' => 'Y-m-d H:i:s',
			'view' => 'd.m.Y H:i:s',
		],
	],
];

