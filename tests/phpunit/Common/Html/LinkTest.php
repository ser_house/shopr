<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 09:26
 */

namespace App\Common\Html;

use App\Common\Render\Render;
use PHPUnit\Framework\TestCase;


/**
 * Class LinkTest
 *
 * @package App\Common\Html
 */
class LinkTest extends TestCase {

	public function testLink() {
		$expected = new \DOMDocument;
		$expected->loadXML('<a href="/uri">text</a>');

		$link = new Link('text', '/uri');
		$render = new Render();
		$html = $render->render($link);

		$actual = new \DOMDocument;
		$actual->loadXML($html);

		$this->assertEqualXMLStructure($expected->firstChild, $actual->firstChild, true);

		$expected->loadXML('<a href="/uri" title="title">text</a>');

		$link->setAttribute('title', 'title');
		$html = $render->render($link);

		$actual->loadXML($html);
		$this->assertEqualXMLStructure($expected->firstChild, $actual->firstChild, true);
	}
}
