<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.12.2017
 * Time: 11:59
 */

namespace App\Common\Html;

use App\Common\Render\Render;
use DiDom\Document;
use PHPUnit\Framework\TestCase;

/**
 * Class FormTest
 *
 * @package App\Common\Html
 */
class FormTest extends TestCase {

	public function testFormCreate() {
		$attributes = [
			'id' => 'form',
			'class' => [
				'form',
			],
		];
		$form = new Form($attributes);
		$render = new Render();
		$html = $render->render($form);

		$dom = new Document($html);
		$formEl = $dom->first('form');

		$this->assertEquals('form', $formEl->attr('id'));
		$this->assertEquals('form', $formEl->attr('class'));
		$this->assertEquals('post', $formEl->attr('method'));
		$this->assertEmpty($formEl->attr('action'));


		$form->setAction('get-form-data');
		$form->setMethod('get');

		$html = $render->render($form);

		$dom->loadHtml($html);

		$formEl = $dom->first('form');

		$this->assertEquals('form', $formEl->attr('class'));
		$this->assertEquals('get', $formEl->attr('method'));
		$this->assertEquals('get-form-data', $formEl->attr('action'));
	}

	public function testFormWithTextfield() {
		$attributes = [
			'id' => 'form',
			'class' => [
				'form',
			],
		];
		$form = new Form($attributes);
		$title_attr = [
			'id' => 'entity-title',
			'class' => [
				'entity-title',
			],
			'required' => true,
		];
		$form->addTextField('title', 'Название', 'название', $title_attr);

		$render = new Render();
		$html = $render->render($form);

		$dom = new Document($html);
		$form = $dom->first('form');
		$this->assertNotEmpty($form);

		$field = $form->first('div');
		$this->assertNotEmpty($field);
		$classes = $field->attr('class');
		$this->assertNotEmpty($classes);
		$this->assertInternalType('string', $classes);
		$classes = explode(' ', $classes);
		$this->assertCount(3, $classes);
		$this->assertContains('form-item', $classes);
		$this->assertContains('form-text', $classes);
		$this->assertContains('required', $classes);

		$label = $field->first('label');
		$this->assertNotEmpty($label);
		$this->assertEquals('entity-title', $label->attr('for'));
		$this->assertEquals('Название', $label->text());

		$input = $label->nextSibling('input');
		$this->assertNotEmpty($input);
		$this->assertEquals('entity-title', $input->attr('id'));
		$this->assertEquals('entity-title', $input->attr('class'));
		$this->assertEquals('title', $input->attr('name'));
		$this->assertEquals('text', $input->attr('type'));
		$this->assertEquals('название', $input->attr('value'));
		$this->assertNotEmpty($input->hasAttribute('required'));
	}

	public function testFormWithCheckbox() {
		$form = new Form();
		$form->addCheckboxField('tracked', 'отслеживать', true);

		$render = new Render();
		$html = $render->render($form);

		$dom = new Document($html);

		$field = $dom->first('div');
		$classes = $field->attr('class');
		$this->assertNotEmpty($classes);
		$this->assertInternalType('string', $classes);
		$classes = explode(' ', $classes);
		$this->assertCount(2, $classes);
		$this->assertContains('form-item', $classes);
		$this->assertContains('form-checkbox', $classes);

		$label = $field->first('label');
		$this->assertNotEmpty($label);

		$this->assertEmpty($label->attr('for'));

		$classes = $label->attr('class');
		$this->assertNotEmpty($classes);
		$this->assertInternalType('string', $classes);
		$classes = explode(' ', $classes);
		$this->assertCount(1, $classes);
		$this->assertContains('checkbox-label', $classes);

		$this->assertEquals('отслеживать', $label->text());

		$input = $label->first('input');
		$this->assertNotEmpty($input);
		$this->assertEquals('tracked', $input->attr('name'));
		$this->assertEquals('checkbox', $input->attr('type'));
		$this->assertEquals(1, $input->attr('value'));
		$this->assertNotEmpty($input->hasAttribute('checked'));
	}

	public function testFormWithTexarea() {
		$attributes = [
			'id' => 'form',
			'class' => [
				'form',
			],
		];
		$form = new Form($attributes);
		$title_attr = [
			'id' => 'entity-text',
			'class' => [
				'entity-text',
			],
			'required' => true,
		];
		$form->addTextareaField('text', 'Текст', 'текст', $title_attr);

		$render = new Render();
		$html = $render->render($form);

		$dom = new Document($html);
		$form = $dom->first('form');
		$this->assertNotEmpty($form);

		$field = $form->first('div');
		$this->assertNotEmpty($field);
		$classes = $field->attr('class');
		$this->assertNotEmpty($classes);
		$this->assertInternalType('string', $classes);
		$classes = explode(' ', $classes);
		$this->assertCount(3, $classes);
		$this->assertContains('form-item', $classes);
		$this->assertContains('form-textarea', $classes);
		$this->assertContains('required', $classes);

		$label = $field->first('label');
		$this->assertNotEmpty($label);
		$this->assertEquals('entity-text', $label->attr('for'));
		$this->assertEquals('Текст', $label->text());

		$textarea = $label->nextSibling('textarea');
		$this->assertNotEmpty($textarea);
		$this->assertEquals('entity-text', $textarea->attr('id'));
		$this->assertEquals('entity-text', $textarea->attr('class'));
		$this->assertEquals('text', $textarea->attr('name'));
		$this->assertEquals('текст', $textarea->text());
		$this->assertNotEmpty($textarea->hasAttribute('required'));
	}
}
