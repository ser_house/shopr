<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2017
 * Time: 01:14
 */

namespace App\Common\Config;

use PHPUnit\Framework\TestCase;


/**
 * Class ConfigTest
 *
 * @package App\Common\Config
 */
class ConfigTest extends TestCase {

	public function testGet() {
		$config = new Config();
		$this->assertEquals('Цены товаров', $config->get('app')['title']);
	}

	public function testMergingTest() {
		$config = new Config();
		$this->assertEquals('localhost', $config->get('db')['host']);
		$this->assertEquals('shopr_test', $config->get('db')['dbname']);
		$this->assertEquals('test', $config->get('db')['pass']);
		$this->assertEquals('test', $config->get('db')['user']);
		$this->assertEquals('utf8', $config->get('db')['charset']);
	}
}
