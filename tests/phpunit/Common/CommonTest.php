<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 19:34
 */

namespace App\Common\Entity;

use App\Common\Common;
use PHPUnit\Framework\TestCase;

class CommonTest extends TestCase {

	public function testGetFieldValues() {

		$entities = [];
		$items = [];

		for($i = 1; $i < 10; $i++) {
			$entities[] = new Entity(['id' => $i]);
			$items[] = ['id' => $i];
		}

		$ids = Common::getFieldValuesList($entities, 'id');

		$this->assertInternalType('array', $ids);
		$this->assertEquals(count($entities), count($ids));


		$ids = Common::getFieldValuesList($items, 'id');

		$this->assertInternalType('array', $ids);
		$this->assertEquals(count($items), count($ids));
	}
}
