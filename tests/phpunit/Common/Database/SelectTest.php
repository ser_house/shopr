<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.12.2017
 * Time: 11:36
 */

namespace App\Common\Database;

use PHPUnit\Framework\TestCase;

class SelectTest extends TestCase {

	public function testGetSql() {
		$std_sql = "SELECT 
			last.* 
		FROM last_price AS last
		INNER JOIN price ON last.price_id = price.id
		WHERE price.price <= 50000000 
		ORDER BY last.id DESC
		LIMIT 2";

		$select = new Select('last_price AS last');
		$select->addFields('last.*');
		$select->addJoin('INNER JOIN price ON last.price_id = price.id');
		$select->addWhere('price.price <= 50000000');
		$select->addOrderBy('last.id DESC');
		$select->setLimit(2);

		$select_sql = $select->getSql();

		$db = DbInstance::getInstance();

		$stmt = $db->prepare($std_sql);
		$stmt->execute();
		$std_result = $stmt->fetchColumn();

		$stmt = $db->prepare($select_sql);
		$stmt->execute();
		$select_result = $stmt->fetchColumn();

		$this->assertEquals($std_result, $select_result);
	}
}
