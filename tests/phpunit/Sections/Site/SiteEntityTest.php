<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 12:43
 */

namespace App\Sections\Product;

use App\Sections\Site\Site;
use PHPUnit\Framework\TestCase;

class SiteEntityTest extends TestCase {

	public function testSetData() {
		$data = [
			'id' => 1,
			'host' => 'host',
			'title' => 'title',
		];

		$site = new Site($data);

		$this->assertEquals(1, $site->id);
		$this->assertEquals('title', $site->title);
		$this->assertEquals('host', $site->host);
	}
}
