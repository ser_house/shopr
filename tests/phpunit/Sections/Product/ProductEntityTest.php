<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.12.2017
 * Time: 12:43
 */

namespace App\Sections\Product;

use PHPUnit\Framework\TestCase;

class ProductEntityTest extends TestCase {

	public function testSetData() {
		$created_at = time();

		$data = [
			'id' => 1,
			'title' => 'title',
			'created' => $created_at,
			'tracked' => false,
		];

		$product = new Product($data);

		$this->assertEquals(1, $product->id);
		$this->assertEquals('title', $product->title);
		$this->assertEquals($created_at, $product->created);
		$this->assertFalse($product->tracked);
	}
}
