<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.12.2017
 * Time: 22:26
 */

namespace App\Sections\Product;

use PHPUnit\Framework\TestCase;

class ProductModelTest extends TestCase {
	/** @var ProductModel */
	protected $productModel;

	protected function setUp() {
		$this->productModel = new ProductModel();
	}

	public function testLoad() {
		$products = $this->productModel->getProducts();

		$this->assertInternalType('array', $products);
		foreach($products as $product) {
			$this->assertTrue($product instanceof Product);
		}

		$products = $this->productModel->getProductsByIds([100, 200]);
		print_r($products);
		print PHP_EOL;
	}

	public function testValidateInputNew() {
		$this->expectException(\InvalidArgumentException::class);

		$data = [
			'title' => '',
		];

		$data = $this->productModel->validateInputNew($data);

		$this->assertNotEmpty($data['tracked']);
	}

	public function testValidateInputEdit() {
		$this->expectException(\InvalidArgumentException::class);

		$data = [
			'id' => '',
			'created' => '',
			'title' => 'Title',
			'tracked' => 'tracked',
		];

		$this->productModel->validateInputNew($data);
	}
}
