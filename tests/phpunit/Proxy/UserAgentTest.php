<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.11.2017
 * Time: 11:04
 */

namespace App\Common\Proxy;

use PHPUnit\Framework\TestCase;


/**
 * Class UserAgentTest
 *
 * @package App\Sections\Proxy
 */
class UserAgentTest extends TestCase {

	public function testLoop() {
		$agent = UserAgent::getUserAgentString();
		$this->assertNotEmpty($agent);
	}
}
