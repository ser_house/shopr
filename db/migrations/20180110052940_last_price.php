<?php


use Phinx\Migration\AbstractMigration;

class LastPrice extends AbstractMigration {
	/**
	 * {@inheritdoc}
	 */
	public function up() {
		$stmt = $this->query('SELECT product_page_id, price_id FROM last_price');
		$rows = $stmt->fetchAll();
		$items = [];
		foreach($rows as $row) {
			$item = [
				'product_page_id' => $row['product_page_id'],
				'price_id' => $row['price_id'],
			];
			$sql = "SELECT 
				id 
			FROM price 
			WHERE product_page_id = {$row['product_page_id']} 
			ORDER BY datetime DESC LIMIT 1, 1";
			$prev_price_row = $this->fetchRow($sql);
			$item['prev_price_id'] = $prev_price_row['id'] ?: NULL;

			$items[] = $item;
		}

		$table = $this->table('last_price');
		$table->drop();

		$table = $this->table('last_price', ['id' => FALSE, 'primary_key' => ['product_page_id', 'price_id']]);
		$table->addColumn('product_page_id', 'integer', ['signed' => FALSE, 'comment' => 'Ссылка на страницу товара.'])
			->addColumn('price_id', 'integer', ['signed' => FALSE, 'comment' => 'Ссылка на последнюю запись цены.'])
			->addColumn('prev_price_id', 'integer', ['signed' => FALSE, 'null' => TRUE, 'comment' => 'Ссылка на предыдущую запись цены для этой страницы.'])
			->addForeignKey('product_page_id', 'product_page', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
			->addForeignKey('price_id', 'price', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
			->addForeignKey('prev_price_id', 'price', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
			->save();

		$this->insert('last_price', $items);
	}

	/**
	 * {@inheritdoc}
	 */
	public function down() {

	}
}
