<?php

error_reporting(E_ALL);

ini_set('xdebug.show_mem_delta', 1);
//ini_set('session.auto_start', 1);
session_start();

define('HOST', 'http://' . $_SERVER['HTTP_HOST']);

require_once '../App/bootstrap.inc';

$app = \App\Common\App::getInstance();
$app->process();
