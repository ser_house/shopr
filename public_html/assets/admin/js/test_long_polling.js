var channelId = "channel";

var pushstream = new PushStream({
	modes: 'websocket|longpolling'
});
pushstream.onmessage = function(data) {
	$('#result').append('<div class="result">' + data + '</div>');
};
pushstream.addChannel(channelId);
pushstream.connect();


function publish_message(msg) {
	$.ajax({
		url: '/pub?id=' + channelId,
		type: "POST",
		contentType: "application/json",
		data: msg
	});
}


$(document).ready(function () {
	$('#send').click(function(event) {
		var msg = $.trim($('#poll-msg').val());
		if (msg.length === 0) {
			alert("You must enter text to publish");
		}
		else {
			publish_message(msg);
		}
	});
});
