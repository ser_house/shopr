(function () {
	// noinspection Annotator
	// noinspection JSUnusedGlobalSymbols
	var app = new Vue({
		el: '#product-form',
		data: function () {
			var pages = document.getElementById('product-form').getAttribute('pages-data');
			return {
				pages: JSON.parse(pages)
			};
		},
		computed: {
			quantity: function () {
				return this.pages.length;
			}
		},
		methods: {
			addPage: function (event) {
				event.preventDefault();
				this.pages.push({
					id: '',
					site_id: '',
					url: '',
					tracked: true
				});
			},
			removePage: function (index) {
				this.pages.splice(index, 1);
			},
			getInputName: function (index, dataName) {
				return "pages[" + index + "][" + dataName + "]";
			}
		}
	});

})();
