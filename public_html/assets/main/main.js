var channelId = 'channel';

const PRICE_UNCHANGED = 0;
const PRICE_CHANGED = 1;

var pushstream = new PushStream({
	modes: 'websocket|longpolling'
});
pushstream.onmessage = function(data) {
	location.reload(true);
};
pushstream.addChannel(channelId);
pushstream.connect();

$(document).ready(function () {
  $(this).on('click', '.ajax-load', function(event) {
    event.preventDefault();

    var $this = $(this);

    if (!$this.data('has_data')) {
      var productId = $this.data('product_id');

      $this.after('<span class="ajax-loading-small"></span>');
      $.post(window.location, {
        op: 'product_prices_load',
        productId: productId
      }, function (response) {
        $this.next('.ajax-loading-small').remove();

        var $html = null;
        if ('success' === response.status) {
          $this.data('has_data', true);
          $html = $('<div class="content">' + response.html + '</div>');
        }
        else {
          $html = $('<div class="error">' + response.html + '</div>');
        }
				$this.closest('.collapsible').toggleClass('collapsed');
        $html.insertAfter($this.parent()).slideDown();
      }, 'json');
    }
    else {
			$this.closest('.collapsible').toggleClass('collapsed');
      $this.parent().next('.content').slideToggle();
    }

    return false;
  });
});
